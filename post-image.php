<?php
/**
 * The template for displaying posts in the Image post format
 */
?>
<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	
	<?php BP_entry_thumbnail( array( 'caption_img' => TRUE ) ); ?>

	<header class="entry-header">
		<?php BP_entry_title(); ?>
		<?php BP_entry_meta(); ?>
	</header><!--END .entry-header -->
	
	<?php BP_entry_content( array( 'content' => BP_content_strip_shortcode( $the_content, array( 'caption' ) ) ) ); ?>

	<?php if ( is_single() ) : ?>
	<footer class="entry-footer">
		<?php BP_entry_tags(); ?>
		<?php BP_entry_social_media_share(); ?>
	</footer><!--END .entry-footer -->
	<?php endif; ?>

</article><!--END #post-<?php the_ID(); ?> -->