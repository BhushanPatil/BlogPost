<?php
/**
 * Template Name: Login
 */
?>

<?php get_header(); ?>

	<div class="grid-container">
    
        <?php $sidebar_position = BP_get_sidebar_position(); ?>
		<?php if ( $sidebar_position == 'left-sidebar' ) get_sidebar(); ?>
		
        <?php if ( $sidebar_position == 'no-sidebar' ) : ?><div id="primary-content" class="col-12">
		<?php else : ?><div id="primary-content" class="col-8"><?php endif; ?>

			<div id="post-entries">

				<article id="login-page" <?php post_class(); ?>>

					<?php BP_entry_thumbnail(); ?>

					<header class="entry-header">
						<?php BP_entry_title(); ?>
						<?php BP_entry_meta( array('meta'=>'%edit%') ); ?>
					</header><!--END .entry-header -->

					<div class="login-box">
						<?php
						if ( !is_user_logged_in() ) {
							$args = array(
								'echo' 				=> true,
								'redirect' 			=> site_url( $_SERVER['REQUEST_URI'] ),
								'form_id' 			=> 'loginform',
								'label_username' 	=> __( 'Username', THEME_TEXTDOMAIN ),
								'label_password' 	=> __( 'Password', THEME_TEXTDOMAIN ),
								'label_remember' 	=> __( 'Remember Me', THEME_TEXTDOMAIN ),
								'label_log_in' 		=> __( 'Log In', THEME_TEXTDOMAIN ),
								'id_username' 		=> 'user_login',
								'id_password' 		=> 'user_pass',
								'id_remember' 		=> 'rememberme',
								'id_submit' 		=> 'wp-submit',
								'remember' 			=> true,
								'value_username'	=> NULL,
								'value_remember'	=> false
							);
							wp_login_form($args);
						}
						else {
							$current_user = wp_get_current_user(); ?>
							<p class="alredy-loggedin">
								<i class="icon-unlock"></i><?php _e( 'You are now logged in as', THEME_TEXTDOMAIN ); ?> <strong><?php echo $current_user->user_login; ?></strong>. <a href="<?php echo wp_logout_url( get_permalink() ); ?>" title="Logout"><?php _e( 'Logout?', THEME_TEXTDOMAIN ); ?></a>
							</p>
						<?php } ?>
					</div>

					<?php BP_entry_content(); ?>
					
				</article><!--END #login-page -->

			</div><!--END #post-entries -->
			
		</div><!--END #primary-content -->

		<?php if ( $sidebar_position == 'right-sidebar' ) get_sidebar(); ?>
	
	</div><!--END .grid-container -->

<?php get_footer(); ?>