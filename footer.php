<?php
/**
 * The template for displaying the footer.
 * Contains footer content and the closing of the
 * #main and #page div elements.
 */
?>
	</main><!--END #site-content -->

	<!--#################### Site Footer ####################-->
	<footer id="site-footer">

		<?php if ( is_active_sidebar( 'sidebar-2' ) ) : ?>

			<div id="footer-sidebar">

				<div class="grid-container">

					<?php dynamic_sidebar( 'sidebar-2' ); ?>

				</div><!--END .grid-container -->

			</div><!--END #footer-sidebar -->

		<?php endif; ?>

		<div id="sub-footer-bottom">

			<div class="grid-container">

				<div class="col-12">

					<nav id="footer-nav">
						<?php wp_nav_menu( array(
							'theme_location' 	=> 'footer',
							'container' 		=> false,
							'depth' 			=> 1,
							'fallback_cb' 		=> 'BP_page_menu'
						) ); ?>
						
					</nav><!--END #footer-nav -->

					<p id="copyright">
						<?php echo get_theme_mod( 'copyright_text', '&copy; '. date("Y") .' <a href="'. home_url( '/') .'">'. get_bloginfo('name') .'</a>. Proudly Powered by <a href="http://wordpress.org">WordPress</a>.' ); ?>&nbsp;
					</p><!--END .copyright -->

				</div><!--END .col-12 -->

			</div><!--END .grid-container -->

		</div><!--END #footer-site-info -->

		<div id="site-links">
			<a id="back-to-top-link" href="#"><i class="button transparent fa fa-arrow-up"></i></a>
		</div>

	</footer><!--END #site-footer -->

	<?php wp_footer(); ?>
	
</body>
</html>