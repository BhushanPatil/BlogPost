<?php
/**
 * Blogpost theme functions and definitions.
 *
 * Sets up the theme and provides some helper functions, which are used in the
 * theme as custom template tags. Others are attached to action and filter
 * hooks in WordPress to change core functionality.
 *
 * When using a child theme (see http://codex.wordpress.org/Theme_Development
 * and http://codex.wordpress.org/Child_Themes), you can override certain
 * functions (those wrapped in a function_exists() call) by defining them first
 * in your child theme's functions.php file. The child theme's functions.php
 * file is included before the parent theme's file, so the child theme
 * functions would be used.
 *
 * Functions that are not pluggable (not wrapped in function_exists()) are
 * instead attached to a filter or action hook.
 *
 * For more information on hooks, actions, and filters,
 * see http://codex.wordpress.org/Plugin_API
 */


/* ============================================================================ *
 * Theme Info Constant Vars 													*
 * ============================================================================ */
// define( 'THEME_NAME', 'Blogpost' );
// define( 'THEME_VERSION', '1.0' );

if ( is_child_theme() ) {
	$temp_obj = wp_get_theme();
	$theme_obj = wp_get_theme( $temp_obj->get('Template') );
}
else $theme_obj = wp_get_theme();

define( 'THEME_NAME', $theme_obj->get('Name') );
define( 'THEME_VERSION', $theme_obj->get('Version') );
define( 'THEME_TEXTDOMAIN', $theme_obj->get('TextDomain') );
define( 'THEME_DIR', get_template_directory() );
define( 'THEME_URI', get_template_directory_uri() );


/* ============================================================================ *
 * Theme Functions 																*
 * ============================================================================ */
require_once( THEME_DIR .'/functions/theme-setup.php' );
require_once( THEME_DIR .'/functions/entry.php' );
require_once( THEME_DIR .'/functions/navigation.php' );
require_once( THEME_DIR .'/functions/load-more.php' );
require_once( THEME_DIR .'/functions/attachments.php' );
require_once( THEME_DIR .'/functions/meta.php' );
require_once( THEME_DIR .'/functions/shortcodes.php' );
require_once( THEME_DIR .'/functions/user.php' );
require_once( THEME_DIR .'/functions/thumbnail.php' );
require_once( THEME_DIR .'/functions/fonts.php' );
require_once( THEME_DIR .'/functions/helper-functions.php' );
require_once( THEME_DIR .'/functions/wp-custom-functions.php' );
require_once( THEME_DIR .'/functions/theme-settings.php' );


/* ============================================================================ *
 * Admin Functions 																*
 * ============================================================================ */
require_once( THEME_DIR .'/functions/theme-customizer.php' );
require_once( THEME_DIR .'/functions/metaboxes.php' );


/* ============================================================================ *
 * Widgets 																		*
 * ============================================================================ */
require_once( THEME_DIR .'/widgets/widget-recent-posts.php' );
require_once( THEME_DIR .'/widgets/widget-post-slider.php' );
require_once( THEME_DIR .'/widgets/widget-recent-comments.php' );
require_once( THEME_DIR .'/widgets/widget-text.php' );
require_once( THEME_DIR .'/widgets/widget-video.php' );
require_once( THEME_DIR .'/widgets/widget-flickr.php' );
require_once( THEME_DIR .'/widgets/widget-dribbble.php' );
require_once( THEME_DIR .'/widgets/widget-facebook-like-box.php' );
require_once( THEME_DIR .'/widgets/widget-twitter.php' );
require_once( THEME_DIR .'/widgets/widget-author-bio.php' );


/* ============================================================================ *
 * Shortcodes 																	*
 * ============================================================================ */
require_once( THEME_DIR .'/shortcodes/shortcode-slider.php' );


/* ============================================================================ *
 * X Panel - Admin Options Panel Framework for Wordpress Themes					*
 * ============================================================================ */
require_once ( THEME_DIR .'/x/index.php' );


?>