<?php
/**
 * The main template file.
 *
 * This is the most generic template file in a WordPress theme and one of the
 * two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * For example, it puts together the home page when no home.php file exists.
 *
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 */
?>

<?php get_header(); ?>

	<div class="grid-container">
    
        <?php $sidebar_position = BP_get_sidebar_position(); ?>
		<?php if ( $sidebar_position == 'left-sidebar' ) get_sidebar(); ?>
		
        <?php if ( $sidebar_position == 'no-sidebar' ) : ?><div id="primary-content" class="col-12">
		<?php else : ?><div id="primary-content" class="col-8"><?php endif; ?>

			<?php get_template_part( 'loop' ); ?>
			
		</div><!--END #primary-content -->

		<?php if ( $sidebar_position == 'right-sidebar' ) get_sidebar(); ?>
		
	</div><!--END .grid-container -->

<?php get_footer(); ?>