<?php
/**
 * The template for displaying posts in the Video post format
 */
?>
<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

	<?php BP_entry_thumbnail(); ?>

	<?php $the_content = get_the_content();
	$shortcodes = array( 'video', 'embed', 'youtube' ); // Schorcode to search in post content.
	$the_shortcode = BP_get_shortcode( $the_content, $shortcodes );
	if ( !post_password_required() && !empty( $the_shortcode ) ) : ?>
	<div class="entry-video">
		<div class="fluid-video"><?php echo do_shortcode( $the_shortcode ); ?></div>
	</div><!--END .entry-video -->
	<?php endif; ?>

	<header class="entry-header">
		<?php BP_entry_title(); ?>
		<?php BP_entry_meta(); ?>
	</header><!--END .entry-header -->
	
	<?php BP_entry_content( array( 'content' => BP_content_strip_shortcode( $the_content, $shortcodes ) ) ); ?>

	<?php if ( is_single() ) : ?>
	<footer class="entry-footer">
		<?php BP_entry_tags(); ?>
		<?php BP_entry_social_media_share(); ?>
	</footer><!--END .entry-footer -->
	<?php endif; ?>

</article><!--END #post-<?php the_ID(); ?> -->