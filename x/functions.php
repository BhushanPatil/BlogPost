<?php if ( !defined('X_DIR') ) exit;

/* Admin Init */
function x_init() {
	if ( empty( x_get_options('x_init_time') ) ) {
		x_save_options( x_get_defaults() );
		x_save_options( time(), 'x_init_time' );
	}
}

/* Create Options Menu Page */
function x_menu_page() {
	$x_page = add_object_page( THEME_NAME, THEME_NAME, 'edit_theme_options', 'themeoptions', 'x_ui', 'dashicons-admin-generic' );
	add_action( 'admin_print_scripts-'.$x_page, 'x_scripts' );
	add_action( 'admin_print_styles-'.$x_page, 'x_styles' );
}

/* Add CSS files */
function x_styles() {
	wp_enqueue_style( 'wp-color-picker' ); // Add the color picker css file
	wp_enqueue_style( 'x-style', X_URI .'/css/style.css' );
}

/* Add JavaScript files */
function x_scripts() {

	wp_enqueue_media(); 						// Load Wordpress media uploader
	wp_enqueue_script( 'wp-color-picker' ); 	// Load Wordpress color picker

	wp_enqueue_script( 'jquery-ui-core' ); 		// Load jQuery UI Core
	wp_enqueue_script( 'jquery-ui-sortable' ); 	// Load jQuery UI Sortable
	wp_enqueue_script( 'jquery-ui-slider' ); 	// Load jQuery UI Slider

	wp_enqueue_script( 'ace-editor', X_URI .'/js/ace/ace.js', null, null );
	wp_enqueue_script( 'ace-mode-css', X_URI .'/js/ace/mode-css.js', array( 'ace-editor' ), null );
	wp_enqueue_script( 'ace-mode-html', X_URI .'/js/ace/mode-html.js', array( 'ace-editor' ), null );
	wp_enqueue_script( 'ace-mode-javascript', X_URI .'/js/ace/mode-javascript.js', array( 'ace-editor' ), null );

	wp_enqueue_script( 'cookie', X_URI .'/js/jquery.cookie.js', array('jquery'), null );

	wp_enqueue_script( 'x-panel', X_URI .'/js/x-panel.js', array( 'jquery', 'cookie', 'ace-editor', 'jquery-ui-slider', 'jquery-ui-sortable','jquery-ui-core',  'wp-color-picker' ), null );
}

/* Custom WP theme activation message */
function x_message() { 
	// Tweaked the message on theme activate ?>
    <script type="text/javascript">
    jQuery(function(){
    	var message = '<p>New theme activated. This theme comes with an <a href="<?php echo admin_url('admin.php?page=themeoptions'); ?>">options panel</a> to configure settings. This theme also supports widgets, please visit the <a href="<?php echo admin_url('widgets.php'); ?>">widgets settings page</a> to configure them.</p>';
    	jQuery('.themes-php #message2').html(message);
    });
    </script><?php
}



/* Get options from the database and process them with the load filter hook */
function x_get_options( $name = null, $default = false ) {
	if ( $name == null ) return get_theme_mods(); 	// Return all values
	else return get_theme_mod( $name, $default ); 	// Return one specific value
}

/* Save options to the database after processing them */
function x_save_options( $data, $name = null ) {

	if ( empty($data) ) return;

	if ( $name != null )
		set_theme_mod( $name, $data );
	else {
		$saved_data = x_get_options();
		foreach ( $data as $k => $v ) {
			if ( $saved_data[$k] != $v || !isset( $saved_data[$k] ) ) { // Only write to the DB when we need to
				set_theme_mod( $k, $v );
			}
	  	}
	}
}

/* Returns array of option default values */
function x_get_defaults() {

	global $x_options;

	$defaults = array();

	foreach ( $x_options as $option ) {

		$id = $option['id'];
		$type = $option['type'];

		if ( $type == 'multi-check' || $type == 'multi-image' || $type == 'multi-switch' ) {
			
			if ( is_array($option['default']) ) {

				foreach ( $option['default'] as $k => $v ) $defaults[$id][$v] = 1;
			}
			else $defaults[$id][$option['default']] = 1;
		}
		elseif ( isset($id) ) $defaults[$id] = $option['default'];
	}

	return $defaults;
}

/* AJAX Save Options */
function x_data_post() {

	if ( !wp_verify_nonce( $_POST['nonce'], 'x_ajax_nonce' ) ) die('-1');

	$type = $_POST['type'];

	if ( $type == 'save' ) {
		$data = array();
		wp_parse_str( stripslashes($_POST['data']), $data );
		x_save_options( $data );
		die('1');
	}
	elseif ( $type == 'reset' ) {
		x_save_options( x_get_defaults() );
		die('1');
    }
    elseif ( $type == 'backup' ) {
    	$backup_data = x_get_options(); 	// Get all options to save backup
    	$backup_data['x_last_backup_time'] = time();
    	unset($backup_data['x_backups']);
    	x_save_options( $backup_data, 'x_backups' );
    	die('1'); 
    }
    elseif ( $type == 'restore' ) {
    	$backup_data = x_get_options( 'x_backups' );
    	x_save_options( $backup_data );
    	die('1');
    }
    elseif ( $type == 'import' ) {
    	$import_data = unserialize(base64_decode($_POST['data'])); // 100% safe - ignore theme check nag
    	x_save_options( $import_data );
    	die('1');
    }
	elseif ( $type == 'upload' ) {
		$clickedID = $_POST['data']; // Acts as the name
		$filename = $_FILES[$clickedID];
		$filename['name'] = preg_replace( '/[^a-zA-Z0-9._\-]/', '', $filename['name'] );
		$override['test_form'] = false;
		$override['action'] = 'wp_handle_upload';
		$uploaded_file = wp_handle_upload( $filename, $override );

		$upload_tracking[] = $clickedID;

		// Update $options array with image URL
		$upload_image = x_get_options(); // Preserve current data, so get options array from db

		$upload_image[$clickedID] = $uploaded_file['url'];

		x_save_options( $upload_image );

		if ( !empty($uploaded_file['error']) ) echo 'Upload Error: '. $uploaded_file['error'];
		else echo $uploaded_file['url']; // Is the Response
	}
	elseif ( $type == 'image_reset' ) {
		$id = $_POST['data']; 					// Acts as the name
		$delete_image = x_get_options(); 		// Preserve rest of data, so get options array from db
		$delete_image[$id] = ''; 				// Update array key with empty value	 
		x_save_options( $delete_image ) ;
	}
	elseif ( $type == 'update_gallery' ) {
		if ( !empty( $_POST['ids'] ) )  {
			$r = '<ul class="x-gallery-list">';
			foreach( $_POST['ids'] as $id ) {
				$thumbnail = wp_get_attachment_image_src( $id, 'thumbnail' );
				$r .= '<li><img src="'. $thumbnail[0] .'" alt="'. $id .'" /></li>';
			}
			$r .= '</ul>';
			exit( $r );
		}
	}
    else die('-1');
}



/* Updates and Returns an array of web fonts */
function x_web_fonts( $api_key = 'AIzaSyBmzfJsfXkXP9PUvwfq53jA1l1YJNxBT4g', $interval = 604800 ) {

	/* Get the theme name */
	$theme_name = strtolower( str_replace(' ','_',THEME_NAME) );

	/* Cached fields name */
	$db_cache 				= 'x-font-cache-'. $theme_name;
	$db_cache_last_updated 	= 'x-font-cache-last-update'. $theme_name;
	$db_cache_theme_name 	= 'x-font-theme-name'. $theme_name;

	/* Get cached data values, if exists */
	$current_fonts 	= get_option( $db_cache ); 				// Get current fonts
	$last 			= get_option( $db_cache_last_updated ); // Get the date for last update
	$theme 			= get_option( $db_cache_theme_name ); 	// Get the theme name

	/* Get current timestamp */
	$now = time();

	/* Check the API key is set */
	if ( !$api_key ) return false;

	/* Check the last update date  */
	if ( !$last || ( ( $now - $last ) > $interval ) || !$theme || $current_fonts == "" || !$current_fonts ) {

		/* Get the Fonts from remote URL */
		$fontsSeraliazed = 'https://www.googleapis.com/webfonts/v1/webfonts?key='. $api_key;
		$response = wp_remote_get ( $fontsSeraliazed, array ( 'sslverify' => false ) );

		/* We have no errors, proceed */
		if ( 200 == wp_remote_retrieve_response_code( $response ) ) {

			/* Parse the result from Font API */
			$response_data = json_decode( $response['body'], true );

			$font_array = array();

			/* Generate the array to store the fonts */
			foreach ( $response_data['items'] as $index => $value ) {
				$family 								= $value['family'];
				$font_array[$family]['variants'] 		= $value['variants'];
				$font_array[$family]['subsets'] 		= $value['subsets'];
				$font_array[$family]['web-font'] 		= true;
			}

			if ( is_array($font_array) ) {
				/* We got good results, so update the existing fields */
				update_option( $db_cache, $font_array );
				update_option( $db_cache_last_updated, $now );
				update_option( $db_cache_theme_name, $theme_name );
			}
			else {
				/* There are no fields, so add them to the database */
				add_option( $db_cache, $font_array, '', 'no' );
				add_option( $db_cache_last_updated, $now, '', 'no' );
				add_option( $db_cache_theme_name, $theme_name, '', 'no' );
			}

			/* Get the font array from options DB */
			$db_font_array = get_option( $db_cache );
		}
		elseif ( is_array($current_fonts) && count($current_fonts) ) {
			/* We are using the already stored fonts */
			$db_font_array = $current_fonts;
		}
	}
	else {
		/* get the font array from options DB */
		if ( is_array($current_fonts) && count($current_fonts) ) {
			$db_font_array = $current_fonts;
		}
	}
	return $db_font_array;
}

/* Returns an array of standard fonts */
function x_standard_fonts() {
	return array(
		'Arial' => array(
			'variants' 		=> array( 'normal', 'bold', 'italic', 'bolditalic' ),
			'type'			=> 'sans-serif',
			'standard-font' => true
		),
		'Arial Black' => array(
			'variants' 		=> array( 'normal', 'italic' ),
			'type'			=> 'sans-serif',
			'standard-font' => true
		),
		'Comic Sans MS' => array(
			'variants' 		=> array( 'normal', 'bold', 'italic', 'bolditalic' ),
			'type'			=> 'sans-serif',
			'standard-font' => true
		),
		'Impact' => array(
			'variants' 		=> array( 'normal', 'bold', 'italic', 'bolditalic' ),
			'type'			=> 'sans-serif',
			'standard-font' => true
		),
		'Lucida Sans Unicode' => array(
			'variants' 		=> array( 'normal', 'bold', 'italic', 'bolditalic' ),
			'type'			=> 'sans-serif',
			'standard-font' => true
		),
		'Tahoma' => array(
			'variants' 		=> array( 'normal', 'bold', 'italic', 'bolditalic' ),
			'type'			=> 'sans-serif',
			'standard-font' => true
		),
		'Trebuchet MS' => array(
			'variants' 		=> array( 'normal', 'bold', 'italic', 'bolditalic' ),
			'type'			=> 'sans-serif',
			'standard-font' => true
		),
		'Verdana' => array(
			'variants' 		=> array( 'normal', 'bold', 'italic', 'bolditalic' ),
			'type'			=> 'sans-serif',
			'standard-font' => true
		),

		'Georgia' => array(
			'variants' 		=> array( 'normal', 'bold', 'italic', 'bolditalic' ),
			'type'			=> 'serif',
			'standard-font' => true
		),
		'Palatino Linotype' => array(
			'variants' 		=> array( 'normal', 'bold', 'italic', 'bolditalic' ),
			'type'			=> 'serif',
			'standard-font' => true
		),
		'Times New Roman' => array(
			'variants' 		=> array( 'normal', 'bold', 'italic', 'bolditalic' ),
			'type'			=> 'serif',
			'standard-font' => true
		),

		'Courier New' 		=> array(
			'variants' 		=> array( 'normal', 'bold', 'italic', 'bolditalic' ),
			'type'			=> 'monospace',
			'standard-font' => true
		),
		'Lucida Console' 	=> array(
			'variants' 		=> array( 'normal', 'bold', 'italic', 'bolditalic' ),
			'type'			=> 'monospace',
			'standard-font' => true
		)
	);
}

/* Returns an array of fonts */
function x_fonts( $font = null ) {

	$font_array = array_merge( x_standard_fonts(), x_web_fonts() );

	if ( $font == null )
		return $font_array;
	else
		return $font_array[$font];
}


/* X Options Page UI */
function x_ui() { include_once( X_DIR.'/ui.php' ); }

/* Process options array and build HTML navigation */
function x_ui_nav( $echo = true ) {

	global $x_options;

	$html = '<ul>';

	$group = isset($_COOKIE['x-group']) ? $_COOKIE['x-group'] : '';
	$counter = 0;
	foreach ( $x_options as $option ) {

		if ( $option['type'] == 'heading' ) {

			$name = $option['name'];
			$id = str_replace( ' ', '', strtolower( $name ) );
			$icon = isset( $option['icon'] ) ? $option['icon'] : 'dashicons-admin-generic';

			$group_get = isset($_GET['group']) ? '#x-group-'. $_GET['group'] : '';
			$group_cookie = isset($_COOKIE['x-group']) ? $_COOKIE['x-group'] : '';
			$group = empty( $group_get ) ? $group_cookie : $group_get;
			if ( $group == '#x-group-'. $id || ( empty($group) && $counter == 0 ) ) $class = ' class="selected"';
			else $class = '';

			$html .= '<li>';
			$html .= '<a href="#x-group-'. $id .'" title="'. $name .'"'. $class .'>';
			$html .= '<span class="dashicons '. $icon .'"></span>';
			$html .= '<span class="x-group-name">'. $name .'</span>';
			$html .= '</a>';
			$html .= '</li>';
			$counter++;
		}
	}
	$html .= '</ul>';

	if ( $echo ) echo $html;
	else return $html;
}

/* Process options array and build HTML for options */
function x_ui_options( $echo = true ) {

	global $x_options;
	$saved_data = x_get_options();
	$r = '';
	$counter = 0;

	$sub_controls = array();
	foreach ( $x_options as $key => $option ) {
		if ( isset($option['sub_control']) && !empty($option['sub_control']) ) {
			$parent_id = $option['sub_control'];
			$sub_controls[$parent_id][$key] = $option['id'];
		}
	}

	foreach ( $x_options as $option ) {

		if ( isset($option['sub_control']) && !empty($option['sub_control']) ) continue;

		$id = $option['id'];
		$name = isset($option['name']) ? $option['name'] : '';
		$desc = isset($option['desc']) ? $option['desc'] : '';
		$type = isset($option['type']) ? $option['type'] : '';
		$value = isset($saved_data[$id]) ? $saved_data[$id] : ( isset($option['default']) ? $option['default'] : '' );
		$counter++;

		/* Name and Description of each option */
		if ( $type != 'heading' ) {

			if ( isset($option['class']) ) $class = ' '. $option['class'];
			else $class = '';

			$r .= '<div class="x-option x-option-'. $type . $class .'">';

			if ( !empty($name) || !empty($desc) ) {
				$r .= '<div class="x-option-explain">';
				if ( !empty($name) ) $r .= '<div class="x-option-name">'. $name .'</div>';
				if ( !empty($desc) ) $r .= '<div class="x-desc">'. $desc .'</div>';
				$r .= '</div>';
			}

			$r .= '<div class="x-controls">';
		}

		if ( $type == 'heading' ) {

			if ( $counter > 1 ) {
				$r .= '</div><!--.x-options-->';
				$r .= '</div><!--.x-group-->';
			}
			
			$group_id = 'x-group-'. str_replace( ' ', '', strtolower($name) );

			$group_get = isset($_GET['group']) ? '#x-group-'. $_GET['group'] : '';
			$group_cookie = isset($_COOKIE['x-group']) ? $_COOKIE['x-group'] : '';
			$group = empty( $group_get ) ? $group_cookie : $group_get;

			if ( $group == '#'.$group_id || ( empty($group) && $counter == 1 ) )
				$style = ' style="display: block;"';
			else
				$style = '';

			$r .= '<div id="'. $group_id .'" class="x-group"'. $style .'>';

			if ( $option['heading'] !== false ) {

				$heading = isset( $option['heading'] ) ? $option['heading'] : $name;

				$r .= '<div class="x-group-heading">';
				if ( !empty($heading) ) $r .= '<h2>'. $heading .'</h2>';
				if ( !empty($desc) ) $r .= '<div class="x-group-desc">'. $desc .'</div>';
				$r .= '</div><!--.x-group-heading-->';
			}

			$r .= '<div id="'. $group_id .'-options" class="x-options">';
		}

		if ( $type != 'heading' ) {

			/* HTML for control */
			if ( isset($sub_controls[$id]) ) {

				$r .= x_ui_control( $option, $value, 'x-fold' );

				foreach ( $sub_controls[$id] as $key => $option ) {

					$sub_control_value = isset($saved_data[$key]) ? $saved_data[$key] : ( isset($x_options[$key]['default']) ? $x_options[$key]['default'] : '' );

					$r .= x_ui_control( $x_options[$key], $sub_control_value );
				}
			}
			else $r .= x_ui_control( $option, $value );

			/* HTML closing tags for each option */
			$r .= '</div><!--.x-controls-->';
			$r .= '</div><!-- #x-option-'. $id .'-->';
		}
	}

	/* HTML closing tags for each options group */
	$r .= '</div><!--.x-options-->';
	$r .= '</div><!--.x-group-->';

	if ( $echo ) echo $r;
	else return $r;
}

/* Builds and Returns controls HTML */
function x_ui_control( $control, $field_value, $class = '' ) {

	$field_name = $control['id'];
	$html_id = x_ui_html_id( $field_name );
	$type = $control['type'];
	$r = '';

	$class = !empty($class) ? ' '.$class : '';

	$sub_control = isset($control['sub_control']) ? x_ui_html_id($control['sub_control']) : '';
	$fold = isset($control['fold']) ? $control['fold'] : '';
	if ( !empty($sub_control) && !empty($fold) ) {

		$class .= ' x-fold-'.$fold.'-'.$sub_control;

		$radio = isset($control['radio']) ? $control['radio'] : '';
		if ( $fold == 'radio' && !empty($radio)  ) {
			$class .= ' x-'.$sub_control.'-'.$radio.'-radio';
		}
	}

	$r .= '<div id="'. $html_id .'" class="x-control x-control-'. $type . $class .'" data-field-name="'. $field_name .'">';

	if ( !empty($sub_control) ) {
		$name = isset($control['name']) ? $control['name'] : '' ;
		$desc = isset($control['desc']) ? $control['desc'] : '' ;
		if ( !empty($name) || !empty($desc) ) {
			$r .= '<div class="x-control-explain">';
			if ( !empty($name) ) $r .= '<div class="x-control-name">'. $name .'</div>';
			if ( !empty($desc) ) $r .= '<div class="x-desc">'. $desc .'</div>';
			$r .= '</div>';
		}
	}

	if ( $type == 'text' ) {
		$placeholder = isset($control['placeholder']) ? ' placeholder="'. $control['placeholder'] .'"' : '';
		$r .= '<input type="text" name="'. $field_name .'" value="'. $field_value .'"'. $placeholder .' />';
	}
	elseif ( $type == 'textarea' ) {
		$placeholder = isset($control['placeholder']) ? ' placeholder="'. $control['placeholder'] .'"' : '';
		$r .= '<textarea name="'. $field_name .'"'. $placeholder .'>'. $field_value .'</textarea>';
	}
	elseif ( $type == 'select' ) {
		$r .= '<select name="'. $field_name .'">';
		foreach ( $control['choices'] as $k => $v )
			$r .= '<option value="'. $k .'"'. selected( $field_value, $k, false ) .' />'. $v .'</option>';
		$r .= '</select>';
	}
	elseif ( $type == 'select-editable' ) {
		$r .= x_select_editable( array(
			'id' 			=> $field_name,
			'html_id' 		=> $html_id,
			'choices' 		=> $control['choices'],
			'value' 		=> $field_value,
			'placeholder' 	=> $control['placeholder']
		) );
	}
	elseif ( $type == 'radio' ) {
		foreach ( $control['choices'] as $key => $val ) {
			$r .= x_radio( array(
				'id' 			=> $field_name,
				'html_id' 		=> $html_id.'-'.$key.'-radio',
				'value' 		=> $key,
				'label' 		=> $val,
				'checked' 		=> $field_value == $key ? true : false,
				'class' 		=> isset($control['class']) ? $control['class'] : ''
			) );
		}
	}
	elseif ( $type == 'checkbox' ) {
		$r .= x_checkbox( array(
			'id' 			=> $field_name,
			'html_id' 		=> $html_id.'-checkbox',
			'value' 		=> $field_value,
			'label' 		=> isset($control['label']) ? $control['label'] : '',
			'class' 		=> isset($control['class']) ? $control['class'] : ''
		) );
	}
	elseif ( $type == 'multi-check' ) {
		if ( isset($control['inline']) && $control['inline'] ) $inline = ' inline'; else  $inline = '';
		foreach ( $control['choices'] as $k => $v ) {
			$r .= x_checkbox( array(
				'id' 			=> $field_name.'['.$k.']',
				'html_id' 		=> $html_id.'-'.$k.'-checkbox',
				'value' 		=> $field_value[$k],
				'label' 		=> !empty($v) ? $v : '',
				'class' 		=> $inline
			) );
		}
	}
	elseif ( $type == 'switch' ) {
		$switch_id = $html_id.'-switch';
		$r .= '<div class="x-switch">';
		$r .= '<input type="hidden" name="'. $field_name .'" value="0" />';
		$r .= '<input id="'. $switch_id .'" type="checkbox" name="'. $field_name .'" value="1" '. checked( $field_value, 1, false ) .' />';
		$r .= '<label for="'. $switch_id .'">';
		$r .= '<span class="switch-icon"></span>';
		if ( !empty($control['label']) ) $r .= '<span class="switch-label-text">'. $control['label'] .'</span>';
		$r .= '</label>';
		$r .= '</div>';
	}
	elseif ( $type == 'multi-switch' ) {
		if ( isset($control['inline']) && $control['inline'] ) $inline = ' inline'; else  $inline = '';
		foreach ( $control['choices'] as $k => $v ) {
			$switch_id = $html_id.'-'.$k.'-switch';
			$r .= '<div class="x-switch'. $inline .'">';
			$r .= '<input type="hidden" name="'. $field_name .'['.$k.']" value="0" />';
			$r .= '<input id="'. $switch_id .'" type="checkbox" name="'. $field_name .'['.$k.']" value="1" '. checked( $field_value[$k], 1, false ) .' />';
			$r .= '<label for="'. $switch_id .'">';
			$r .= '<span class="switch-icon"></span>';
			if ( !empty($v) ) $r .= '<span class="switch-label-text">'. $v .'</span>';
			$r .= '</label>';
			$r .= '</div>';
		}
	}
	elseif ( $type == 'measurement' ) {
		if ( is_array($control['units']) ) $unit_array = $control['units'];
		else $unit_array = array( '', 'px', '%', 'em', 'rem', 'pt', 'pc', 'ex', 'mm', 'cm', 'in' );
		$r .= '<input type="text" name="'. $field_name .'[value]" value="'. $field_value['value'] .'" />';
		$r .= '<select name="'. $field_name .'[unit]">';
		foreach ( $unit_array as $key )
			$r .= '<option value="'. $key .'"'. selected( $field_value['unit'], $key, false ) .'>'. $key .'</option>';
		$r .= '</select>';
	}
	elseif ( $type == 'color' ) {
		$default = isset($control['default']) ? ' data-default-color="'. $control['default'] .'" ' : '';
		$r .= '<input class="x-color" name="'. $field_name .'" type="text" value="'. $field_value .'"'. $default .' />';
	}
	elseif ( $type == 'image' ) {
		if ( !isset($control['inline']) || $control['inline'] ) $inline = ' inline'; else  $inline = '';
		foreach ( $control['choices'] as $k => $v ) {
			$image_id = $html_id.'-'.$k.'-image';
			$r .= '<div class="x-image-option'. $inline .'">';
			$r .= '<input id="'. $image_id .'" type="radio" name="'. $field_name .'" value="'. $k .'" '. checked( $field_value, $k, false ) .' />';
			$r .= '<label for="'. $image_id .'"><img src="'. $v .'" alt="'. $k .'" /></label>';
			$r .= '</div>';
		}
	}
	elseif ( $type == 'multi-image' ) {
		if ( !isset($control['inline']) || $control['inline'] ) $inline = ' inline'; else  $inline = '';
		foreach ( $control['choices'] as $k => $v ) {
			$image_id = $html_id.'-'.$k.'-image';
			$r .= '<div class="x-image-option'. $inline .'">';
			$r .= '<input type="hidden" name="'. $field_name .'['. $k .']'.'" value="0" />';
			$r .= '<input id="'. $image_id .'" type="checkbox" name="'. $field_name .'['. $k .']'.'" value="1" '. checked( $field_value[$k], 1, false ) .' /><label for="'. $image_id .'"><img src="'. $v .'" alt="'. $k .'" /></label>';
			$r .= '</div>';
		}
	}
	elseif ( $type == 'range' ) {
		$data_atts = '';
		$range_vars = array( 'animate', 'max', 'min', 'orientation', 'range', 'step' );
		foreach ( $range_vars as $key ) {
			if ( isset($control[$key]) && !empty($control[$key]) )
				$data_atts .= ' data-'. $key .'="'. $control[$key] .'"';
		}
		if ( !empty($field_value) ) $data_atts .= 'data-value="'. $field_value .'"';
		else $field_value = 0;
		$r .= '<div class="x-range">';
		$r .= '<input type="text" name="'. $field_name .'" value="'. $field_value .'" />';
		$r .= '<div class="x-range-slider" '. $data_atts .'></div>';
		$r .= '</div>';
	}
	elseif ( $type == 'slides' ) {
		$r .= '<input type="hidden" name="'. $field_name .'" value="" />';
		$r .= '<ul class="x-slides">';
		if ( is_array($field_value) )
			foreach ( $field_value as $key => $val ) {
				$r .= x_slide( array(
					'id' 		=> $field_name,
					'html_id' 	=> $html_id.'-'.$key,
					'slide' 	=> $field_value[$key],
					'order' 	=> $key
				) );
			}
		$r .= '</ul>';
		$r .= '<button class="button x-slide-add" type="button">Add New</button>';
	}
	elseif ( $type == 'media' ) {
		$r .= x_media( array(
			'id' 			=> $field_name,
			'html_id' 		=> $html_id,
			'value' 		=> $field_value
		) );
	}
	elseif ( $type == 'sorter' ) {

		$default 		= ( isset( $control['default'] ) && !empty( $control['default'] ) ) ? $control['default'] : array();
		$groups 		= ( isset( $control['groups'] ) && !empty( $control['groups'] ) ) ? $control['groups'] : array();
		$items 			= ( isset( $control['items'] ) && !empty( $control['items'] ) ) ? $control['items'] : array();
		$items_saved 	= array();
		$sortlists 		= $field_value;

		foreach ( $default as $key => $val ) {
			if ( !array_key_exists( $key, $groups ) ) $groups[$key] = ucfirst($key);
			$items = array_merge( $items, $val );
		}

		foreach ( $sortlists as $key ) $items_saved = array_merge( $items_saved, $key );

		// Add unsaved items to first group
		foreach ( $items as $key => $val ) {
			if ( !array_key_exists( $key, $items_saved ) ) $sortlists[key($groups)][$key] = $val;
		}

		// Unset saved items that does not exists in items array
		foreach ( $sortlists as $key => $val ) {
			foreach ( $val as $k => $v ) {
				if ( !array_key_exists( $k, $items ) ) unset($val[$k]);
			}
			$sortlists[$key] = $val;
		}

		// Assuming all sync, now get the correct naming for each block
		foreach ( $sortlists as $key => $val ) {
			foreach( $val as $k => $v ) $val[$k] = $items[$k];
			$sortlists[$key] = $val;
		}

		foreach ( $groups as $key => $val ) {
			$group_id = $html_id.'-'.$key;
			$r .= '<ul id="'. $group_id .'" class="x-sorter-group '. $html_id.'-group" data-group="'. $key .'">';
			$r .= '<li class="x-sorter-group-heading"><h3>'. $val .'</h3></li>';
			foreach ( (array)$sortlists[$key] as $k => $v ) {
				$r .= '<li class="x-sorter-item">';
				$r .= '<input type="hidden" name="'. $field_name .'['.$key.']['.$k.']" value="'. $v .'" data-item="'. $k .'">';
				$r .= ''. $v .'';
				$r .= '</li>';
			}
			$r .= '</ul>';
		}
	}
	elseif ( $type == 'typography' ) {

		$typography_vars = array( 'family', 'fallback', 'variant', 'size', 'height' );
		foreach ( $typography_vars as $key ) if ( !isset($field_value[$key]) ) $field_value[$key] = '';
		extract($field_value);

		$font_array = x_fonts();
		$fallback_array = array(
			'sans-serif' 	=> 'sans-serif',
			'serif' 		=> 'serif',
			'cursive' 		=> 'cursive',
			'fantasy' 		=> 'fantasy',
			'monospace'		=> 'monospace'
		);
		$variant_array = array(
			'lighter' 		=> 'Lighter',
			'lighteritalic' => 'Lighter Italic',
			'normal' 		=> 'Normal',
			'bold' 			=> 'Bold',
			'bolditalic' 	=> 'Bold Italic',
			'bolder' 		=> 'Bolder',
			'bolderitalic' 	=> 'Bolder Italic',
			'regular' 		=> 'Regular',
			'italic' 		=> 'Italic',
			'100' 			=> 'Thin 100',
			'100italic' 	=> 'Thin 100 Italic',
			'200' 			=> 'Extra Light 200',
			'200italic' 	=> 'Extra Light 200 Italic',
			'300' 			=> 'Light 300',
			'300italic' 	=> 'Light 300 Italic',
			'400' 			=> 'Normal 400',
			'400italic' 	=> 'Normal 400 Italic',
			'500' 			=> 'Medium 500',
			'500italic' 	=> 'Medium 500 Italic',
			'600' 			=> 'Semi-Bold 600',
			'600italic' 	=> 'Semi-Bold 600 Italic',
			'700' 			=> 'Bold 700',
			'700italic' 	=> 'Bold 700 Italic',
			'800' 			=> 'Extra Bold 800',
			'800italic' 	=> 'Extra Bold 800 Italic',
			'900' 			=> 'Ultra Bold 900',
			'900italic' 	=> 'Ultra Bold 900 Italic'
		);
		$size_array = array(
			'6px' 	=> '6px',
			'7px' 	=> '7px',
			'8px' 	=> '8px',
			'9px' 	=> '9px',
			'10px' 	=> '10px',
			'11px' 	=> '11px',
			'12px' 	=> '12px',
			'14px' 	=> '14px',
			'16px' 	=> '16px',
			'18px' 	=> '18px',
			'21px' 	=> '21px',
			'24px' 	=> '24px',
			'36px' 	=> '36px',
			'48px' 	=> '48px',
			'60px' 	=> '60px',
			'72px' 	=> '72px'
		);
		$web_font_variant_array = array(
			'100' 			=> 'Thin 100',
			'100italic' 	=> 'Thin 100 Italic',
			'200' 			=> 'Extra Light 200',
			'200italic' 	=> 'Extra Light 200 Italic',
			'300' 			=> 'Light 300',
			'300italic' 	=> 'Light 300 Italic',
			'400' 			=> 'Normal 400',
			'400italic' 	=> 'Normal 400 Italic',
			'500' 			=> 'Medium 500',
			'500italic' 	=> 'Medium 500 Italic',
			'600' 			=> 'Semi-Bold 600',
			'600italic' 	=> 'Semi-Bold 600 Italic',
			'700' 			=> 'Bold 700',
			'700italic' 	=> 'Bold 700 Italic',
			'800' 			=> 'Extra Bold 800',
			'800italic' 	=> 'Extra Bold 800 Italic',
			'900' 			=> 'Ultra Bold 900',
			'900italic' 	=> 'Ultra Bold 900 Italic'
		);
		$web_font_subset_array = array(
			'latin' 		=> 'Latin',
			'latin-ext' 	=> 'Latin Extended',
			'greek' 		=> 'Greek',
			'greek-ext' 	=> 'Greek Extended',
			'cyrillic' 		=> 'Cyrillic',
			'cyrillic-ext' 	=> 'Cyrillic Extended',
			'vietnamese' 	=> 'Vietnamese',
			'khmer' 		=> 'Khmer'
		);

		if ( !empty($control['default']['family']) ) {
			$default_family = $control['default']['family'];
			$default_family_array = array();
			$default_family_array[$default_family] = $font_array[$default_family];
			$font_array = array_merge( $default_family_array, $font_array );
		}
		else $default_family = false;

		$r .= '<select id="'. $html_id .'-family" class="x-typography-family" name="'. $field_name .'[family]" data-tooltip="Select Font Family">';
		foreach ( $font_array as $key => $val ) {

			$class = $val['web-font'] ? 'web-font' : 'standard-font';
			foreach ( (array) $val['variants'] as $k ) $class .= ' variant-'. $k;
			foreach ( (array) $val['subsets'] as $k ) $class .= ' subset-'. $k;
			if ( $key == $default_family ) $class .= ' default';

			$font_name = str_replace( ' ', '-', $key );
			$text = $default_family == $key ? $key.' (Default)' : $key;
			$option_id = $html_id.'-'.$font_name;

			$r .= '<option id="'. $option_id .'" value="'. $key .'"'. selected( $family, $key, false ) .' class="'. $class .'">'. $text .'</option>';
		}
		$r .= '</select>';

		$r .= x_select_editable( array(
			'id' 			=> $field_name.'[fallback]',
			'html_id' 		=> $html_id.'-fallback',
			'choices' 		=> $fallback_array,
			'value' 		=> $fallback,
			'placeholder' 	=> 'Fallback font',
			'class' 		=> 'x-typography-fallback'
		) );
		
		$r .= '<select id="'. $html_id .'-variant" class="x-typography-variant" name="'. $field_name .'[variant]" data-tooltip="Select Font Variant">';
		foreach ( $variant_array as $key => $val ) {
			$r .= '<option class="variant-'. $key .'" value="'. $key .'"'. selected( $variant, $key, false ) .'>'. $val .'</option>';
		}
		$r .= '</select>';

		$r .= x_select_editable( array(
			'id' 			=> $field_name.'[size]',
			'html_id' 		=> $html_id.'-size',
			'choices' 		=> $size_array,
			'value' 		=> $size,
			'placeholder' 	=> 'Font Size (CSS Value)',
			'class' 		=> 'x-typography-size'
		) );

		$r .= '<input id="'. $html_id .'-height" class="x-typography-height" name="'. $field_name .'[height]" type="text" value="'.$height .'" placeholder="Line Height (CSS Value)" />';

		$preview = $control['preview'];
		$preview_vars = array( 'text', 'size', 'height' );
		foreach ( $preview_vars as $key ) if ( !isset($preview[$key]) ) $preview[$key] = '';
		extract( $preview, EXTR_PREFIX_ALL, 'preview' );
		if ( empty($preview_text) ) $preview_text = "0123456789\nABCDEFGHIJKLMNOPQRSTUVWXYZ\nabcdefghijklmnopqrstuvwxyz";
		$style = 'font-size: '. ( !empty($size) ? $size : $preview_size ) .';';
		$style .= 'line-height: '. ( !empty($height) ? $height : $preview_height ) .';';
		$r .= '<textarea id="'. $html_id .'-preview" class="x-typography-preview" spellcheck="false" style="'. $style .'">'. $preview_text .'</textarea>';
		
		$r .= '<div id="'. $html_id .'-web-font-variants" class="x-typography-web-font-variants">';
		$r .= '<h4>Choose the styles<span class="dashicons dashicons-arrow-down-alt2"></span></h4>';
		$r .= '<div style="display: none;">';
		foreach ( $web_font_variant_array as $key => $val ) {

			$check_id = $html_id .'-'. str_replace( ' ', '-', $key );
			$check_name = $field_name.'['.$key.']';

			$class = 'variant-'. $key;
			if ( strpos($key,'italic') !== false ) $class .= ' italic';
			if ( $field_value[$key] == 1 ) $class .= ' user-selected';

			// $r .= '<div class="x-checkbox col-3">';
			// $r .= '<input id="'. $check_id .'" class="'. $class .'" type="checkbox" name="'. $check_name .'" value="1"'. checked( $field_value[$key], 1, false ) .' />';
			// $r .= '<label for="'. $check_id .'">';
			// $r .= '<span class="checkbox-icon"></span>';
			// $r .= '<span class="checkbox-label-text">'. $val .'</span>';
			// $r .= '</label>';
			// $r .= '</div>';

			$r .= x_checkbox( array(
				'id' 				=> $check_name,
				'html_id' 			=> $check_id,
				'value' 			=> $field_value[$key],
				'label' 			=> $val,
				'unchecked_value' 	=> false,
				'class' 			=> 'col-3',
				'class_input' 		=> $class,
				'atts' 				=> array( 'data-variant' => $key )
			) );
		}
		$r .= '</div></div>';
		
		$r .= '<div id="'. $html_id .'-web-font-subsets" class="x-typography-web-font-subsets">';
		$r .= '<h4>Choose the character sets<span class="dashicons dashicons-arrow-down-alt2"></span></h4>';
		$r .= '<div style="display: none;">';
		foreach ( $web_font_subset_array as $key => $val ) {

			$check_id = $html_id .'-'. str_replace( ' ', '-', $key );
			$check_name = $field_name.'['.$key.']';

			// $r .= '<div class="x-checkbox col-3">';
			// $r .= '<input id="'. $check_id .'" class="'. $key .'" type="checkbox" name="'. $check_name .'" value="1"'. checked( $field_value[$key], 1, false ) .' />';
			// $r .= '<label for="'. $check_id .'">';
			// $r .= '<span class="checkbox-icon"></span>';
			// $r .= '<span class="checkbox-label-text">'. $val .'</span>';
			// $r .= '</label>';
			// $r .= '</div>';

			$r .= x_checkbox( array(
				'id' 				=> $check_name,
				'html_id' 			=> $check_id,
				'value' 			=> $field_value[$key],
				'label' 			=> $val,
				'unchecked_value' 	=> false,
				'class' 			=> 'col-3',
				'class_input' 		=> 'subset-'. $key,
				'atts' 				=> array( 'data-subset' => $key )
			) );
		}
		$r .= '</div></div>';
	}
	elseif ( $type == 'editor' ) {
		ob_start();
		wp_editor(
			$field_value,
			$field_name,
			array(
				'textarea_rows' 	=> get_option( 'default_post_edit_rows', 5 ),
				'quicktags' 		=> array( 'buttons' => 'strong,em,link,block,del,ins,img,ul,ol,li,code,spell,close' ),
				'drag_drop_upload' 	=> true
			)
		);
		$r .= ob_get_clean();
	}
	elseif ( $type == 'code' ) {
		$mode = isset($control['mode']) ? $control['mode'] : '';
		$theme = isset($control['theme']) ? $control['theme'] : '';
		$data_atts = !empty($mode) ? ' data-mode="'. $mode .'"' : '';
		$data_atts .= !empty($theme) ? ' data-theme="'. $theme .'"' : '';
		$font_size = isset($control['font_size']) ? $control['font_size'] : '14px';
		$r .= '<textarea id="'. $html_id .'-textarea" name="'. $field_name .'" style="display:none;"'. $data_atts .'>'. $field_value .'</textarea>';
		$r .= '<div id="'. $html_id .'-code" style="font-size: '. $font_size .'; border: 1px solid #dedede; width: 100%; min-height: 87px; position: relative;"></div>';
	}
	elseif ( $type == 'background' ) {

		$background_vars = array( 'id', 'type', 'url', 'color', 'repeat', 'attachment', 'position', 'size' );
		foreach ( $background_vars as $key ) if ( !isset($field_value[$key]) ) $field_value[$key] = '';
		extract( $field_value, EXTR_PREFIX_SAME, 'media' );

		$repeat_array = array(
			'repeat' 	=> 'Tile',
			'repeat-x' 	=> 'Tile Horizontally',
			'repeat-y' 	=> 'Tile Vertically',
			'no-repeat' => 'No Repeat'
		);
		$attachment_array = array(
			'scroll' 	=> 'Scroll',
			'fixed' 	=> 'Fixed',
			'local' 	=> 'Local'
		);
		$position_array = array(
			'left top' 		=> 'Left Top',
			'left center' 	=> 'Left Center',
			'left bottom' 	=> 'Left Bottom',
			'center top' 	=> 'Center Top',
			'center center' => 'Center Center',
			'center bottom' => 'Center Bottom',
			'right top' 	=> 'Right Top',
			'right center' 	=> 'Right Center',
			'right bottom' 	=> 'Right Bottom'
		);
		$size_array = array(
			'cover' 	=> 'Cover',
			'contain' 	=> 'Contain'
		);

		$r .= x_media( array(
			'id' 			=> $field_name,
			'html_id' 		=> $html_id,
			'value' 		=> $field_value,
			'placeholder' 	=> isset($control['placeholder']) ? $control['placeholder'] : null,
			'preview' 		=> $html_id.'-preview'
		) );

		$r .= '<input class="x-color" name="'. $field_name .'[color]" type="text" value="'. $color .'"'. $color .' />';
		$r .= '<div id="'. $html_id .'-preview" class="x-media-preview">';
		if ( !empty($url) ) {
			$r .= '<div class="x-media-image">';
			$r .= '<a href="'. $url . '" target="_blank"><img src="'. $url . '" alt="Preview"></a>';
			$r .= '</div>';
		}
		$r .= '</div>';
		$r .= '<div class="x-background-display-options">';
		$r .= x_select_editable( array(
			'id' 			=> $field_name.'[repeat]',
			'html_id' 		=> $html_id.'-repeat',
			'choices' 		=> $repeat_array,
			'value' 		=> $repeat,
			'placeholder' 	=> 'background-repeat'
		) );
		$r .= x_select_editable( array(
			'id' 			=> $field_name.'[attachment]',
			'html_id' 		=> $html_id.'-attachment',
			'choices' 		=> $attachment_array,
			'value' 		=> $attachment,
			'placeholder' 	=> 'background-attachment'
		) );
		$r .= x_select_editable( array(
			'id' 			=> $field_name.'[position]',
			'html_id' 		=> $html_id.'-position',
			'choices' 		=> $position_array,
			'value' 		=> $position,
			'placeholder' 	=> 'background-position'
		) );
		$r .= x_select_editable( array(
			'id' 			=> $field_name.'[size]',
			'html_id' 		=> $html_id.'-size',
			'choices' 		=> $size_array,
			'value' 		=> $size,
			'placeholder' 	=> 'background-size'
		) );

		$r .= '</div>';
	}
	elseif ( $type == 'taxonomy' || $type == 'category' || $type == 'tag'  ) {

		$term_id = isset($field_value['term_id']) ? $field_value['term_id'] : '';
		$term_name = isset($field_value['name']) ? $field_value['name'] : '';
		$term_taxonomy = isset($field_value['taxonomy']) ? $field_value['taxonomy'] : '';

		$r .= '<input class="x-term-id" type="hidden" name="'. $field_name .'[term_id]" value="'. $term_id .'">';
		$r .= '<input class="x-term-name" type="hidden" name="'. $field_name .'[name]" value="'. $term_name .'">';
		$r .= '<input class="x-term-taxonomy" type="hidden" name="'. $field_name .'[taxonomy]" value="'. $term_taxonomy .'">';

		if ( $type == 'taxonomy' ) {
			$taxonomy = isset($control['taxonomy']) ? $control['taxonomy'] : '';
			if ( empty($taxonomy) ) $taxonomy = array( 'category', 'post_tag' );
			$taxonomies = get_terms( $taxonomy );
			$no_term = 'No Terms';
		}
		elseif ( $type == 'category' ) {
			$taxonomies = get_categories();
			$no_term = 'No Categories';
		}
		else {
			$taxonomies = get_tags();
			$no_term = 'No Tags';
		}

		if ( is_array($taxonomies) ) {
			$r .= '<select class="x-taxonomy-term-select">';
			foreach ( $taxonomies as $term ) {
				$r .= '<option value="'. $term->term_id .'"'. selected( $term_id, $term->term_id, false ) .' data-name="'. $term->name .'" data-taxonomy="'. $term->taxonomy .'">'. $term->name .'</option>';
			}
			$r .= '</select>';
		}
		else {
			$r .= '<select class="x-taxonomy-term-select" disabled="disabled">';
			$r .= '<option>'. $no_term .'</option>';
			$r .= '</select>';
		}
	}
	elseif ( $type == 'taxonomies' || $type == 'categories' || $type == 'tags' ) {

		$r .= '<input type="hidden" name="'. $field_name .'" value="" />';
		if ( is_array($control['icon']) ) {
			foreach ( $control['icon'] as $key => $val )
				$r .= '<input id="'. $html_id.'-'.$key.'-icon" type="hidden" value="'. $val .'" />';
		}
		$r .= '<div class="x-taxonomies-terms">';
		if ( is_array($field_value) ) {

			foreach ( $field_value as $key => $val ) {

				$term_id = $val['term_id'];
				$term_name = $val['name'];
				$term_taxonomy = $val['taxonomy'];

				if ( !term_exists( $term_name, $term_taxonomy ) ) continue;

				if ( is_array($control['icon']) ) {
					$icon = $control['icon'][$term_taxonomy];
					$class = ' with-icon';
				}
				else $icon = $class = '';

				$r .= '<span class="x-taxonomies-term button'. $class .'">';
				$r .= '<input type="hidden" name="'. $field_name.'['.$key.'][term_id]" value="'. $term_id .'" />';
				$r .= '<input type="hidden" name="'. $field_name.'['.$key.'][name]" value="'. $term_name .'" />';
				$r .= '<input type="hidden" name="'. $field_name.'['.$key.'][taxonomy]" value="'. $term_taxonomy .'" />';
				if ( !empty($icon) ) $r .= '<span class="dashicons '. $icon .' icon-taxonomy"></span>';
				$r .= '<span class="x-term-name-text">'. $term_name .'</span>';
				$r .= '<span class="dashicons dashicons-no-alt icon-remove"></span>';
				$r .= '</span>';
			}
		}
		$r .= '</div>';

		if ( $type == 'taxonomies' ) {
			$taxonomy = isset($control['taxonomy']) ? $control['taxonomy'] : '';
			if ( empty($taxonomy) ) $taxonomy = array( 'category', 'post_tag' );
			$taxonomies = get_terms( $taxonomy );
			$no_term = 'No Terms';
		}
		elseif ( $type == 'categories' ) {
			$taxonomy = 'category';
			$taxonomies = get_categories();
			$no_term = 'No Categories';
		}
		else {
			$taxonomy = 'post_tag';
			$taxonomies = get_tags();
			$no_term = 'No Tags';
		}

		if ( is_array($taxonomies) ) {
			$r .= '<select class="x-taxonomies-term-select">';
			foreach ( $taxonomies as $term ) {
				$r .= '<option value="'. $term->term_id .'" data-name="'. $term->name .'" data-taxonomy="'. $term->taxonomy .'">'. $term->name .'</option>';
			}
			$r .= '</select>';
		}
		else {
			$r .= '<select class="x-taxonomies-term-select" disabled="disabled">';
			$r .= '<option>'. $no_term .'</option>';
			$r .= '</select>';
		}
	}
	elseif ( $type == 'post' ) {
		$posts = get_posts( array( 'posts_per_page' => -1, 'post_status' => 'any' ) );
		if ( empty($posts) ) {
			$r .= '<select name="'. $field_name .'" disabled="disabled">';
			$r .= '<option>No Posts</option>';
			$r .= '</select>';
		}
		else {
			$r .= '<select name="'. $field_name .'">';
			foreach ( $posts as $post ) {
				$post_title = strlen($post->post_title) > 43 ? substr( $post->post_title, 0, 43 ) .'...' : $post->post_title;
				$r .= '<option value="'. $post->ID .'"'. selected( $field_value, $post->ID, false ) .' />'. $post_title .'</option>';
			}
			$r .= '</select>';
		}
	}
	elseif ( $type == 'posts' ) {

		$posts = get_posts( array( 'posts_per_page' => -1, 'post_status' => 'any' ) );
		$r .= '<input type="hidden" name="'. $field_name .'" value="" />';
		$r .= '<div class="x-posts-selected">';
		if ( is_array($field_value) ) {
			foreach ( $field_value as $key => $val ) {

				$r .= '<div class="x-selected-post">';
				$r .= '<input class="x-post-value" type="hidden" name="'. $field_name .'['. $key .']" value="'. $val .'" />';

				$r .= '<div class="x-post-image">';
				$post_image = wp_get_attachment_image_src( get_post_thumbnail_id( $val ), array(55,55) );
				$r .= '<span class="dashicons dashicons-format-aside"></span>';
				if ( !empty($post_image) )
					$r .= '<img src="'. $post_image[0] .'" alt="Featured Image" />';
				$r .= '</div>';

				$r .= '<div class="x-post-info">';

				$title = get_the_title($val);
				if ( empty( trim($title) ) ) $title = '<span class="untitled">(Untitled)</span>';

				$r .= '<span class="x-post-name"><a href="'. get_permalink($val) .'" target="_blank">'. $title .'</a></span>';
				$r .= '<span class="x-post-date">'. get_post_time( 'M j, Y', null, $val ) .'</span>';
				$r .= '<span class="x-post-author">'. get_the_author_meta( 'display_name', get_post( $val )->post_author ) .'</span>';

				$r .= '</div>';

				$r .= '<a class="x-post-remove" href="javascript:void(0);">Remove</a>';

				$r .= '</div>';
			}
		}
		$r .= '</div>';
		$r .= '<select class="x-posts-select">';
		foreach ( $posts as $post ) {
			$post_ID = $post->ID;
			$post_image = wp_get_attachment_image_src( get_post_thumbnail_id( $post_ID ), array(55,55) );
			$post_title = $post->post_title;
			$option_text = strlen($post_title) > 43 ? substr( $post_title, 0, 43 ) .'...' : $post_title;
			$r .= '<option value="'. $post_ID .'" data-image="'. $post_image[0] .'" data-title="'. $post_title .'" data-link="'. get_permalink( $post_ID ) .'" data-date="'. get_post_time( 'M j, Y', null, $post_ID ) .'" data-author="'. get_the_author_meta( 'display_name', $post->post_author ) .'" />'. $option_text .'</option>';
		}
		$r .= '</select>';
	}
	elseif ( $type == 'link' ) {

		$background_vars = array( 'url', 'text', 'title', 'class', 'target' );
		foreach ( $background_vars as $key ) {
			if ( !isset($field_value[$key]) ) {
				$field_value[$key] = '';
			}
		}
		extract( $field_value, EXTR_PREFIX_SAME, 'media' );

		$r .= '<input class="x-link-url" type="text" name="'. $field_name .'[url]" value="'. $url .'" placeholder="URL" />';
		$r .= '<input class="x-link-text" type="text" name="'. $field_name .'[text]" value="'. $text .'" placeholder="Label" />';
		$r .= '<input class="x-link-title" type="text" name="'. $field_name .'[title]" value="'. $title .'" placeholder="Title Attribute" />';
		$r .= '<input class="x-link-class" type="text" name="'. $field_name .'[class]" value="'. $class .'" placeholder="CSS Classes (optional)" />';
		$r .= x_checkbox( array(
			'id' 	=> $field_name.'[target]',
			'value' => $target,
			'label' => 'Open link in a new window/tab',
			'class' => 'x-link-target'
		) );
	}
	elseif ( $type == 'gallery' ) {

		// Search the string for the IDs
		preg_match( '/ids=\'(.*?)\'/', $field_value, $matches );

		// If found the IDs in the shortcode then Turn the field value into an array of IDs
		if ( isset($matches[1]) )
			$attachment_ids = explode( ',', $matches[1] );
		else
			$attachment_ids = array();

		// Saved values
		$r .= '<input class="x-gallery-shortcode" type="text" name="'. $field_name .'" value="'. $field_value . '" placeholder="Gallery Shortcode" readonly />';

		// If has gallery attachment IDs
		if ( !empty( $attachment_ids ) ) {
			$r .= '<ul class="x-gallery-list">';
			foreach( $attachment_ids as $html_id ) {
				$thumbnail = wp_get_attachment_image_src( $html_id, 'thumbnail' );
				$r .= '<li><img src="'. $thumbnail[0] .'" alt="'. $html_id .'" /></li>';
			}
			$r .= '</ul>';
		}

		// Gallery buttons
        $r .= '<div class="x-gallery-buttons">';
        $r .= '<button class="button x-gallery-create" type="button">Create Gallery</button>';
        $r .= '<button class="button x-gallery-edit" type="button">Edit Gallery</button>';
        $r .= '<button class="button x-gallery-delete" type="button">Delete</button>';
        $r .= '</div>';
	}
	elseif ( $type == 'info' ) { $r .= $control['info']; }
	elseif ( $type == 'backup' ) {
		$backup = x_get_options( 'x_backups' );
		if ( isset($backup['x_last_backup_time']) ) $last_backup = date( 'j M Y g:i A', $backup['x_last_backup_time'] );
		else $last_backup = __( 'No backups yet' );
		$r .= '<div class="x-last-backup">'. sprintf( __( 'Last Backup: %s' ), $last_backup ) .'</div>' . "\n";
		$r .= '<a class="x-backup-button button" href="javascript:void(0);" title="Backup Options">'. __('Backup Options') .'</a>';
		$r .= '<a class="x-restore-button button" href="javascript:void(0);" title="Restore Options">'. __('Restore Options') .'</a>';
	}
	elseif ( $type == 'transfer' ) {
		$r .= '<textarea rows="7">'. base64_encode(serialize(x_get_options())) .'</textarea>';
		$r .= '<a class="x-import-button button" href="javascript:void(0);" title="Restore Options">'. __('Import Options') .'</a>';
	}
	
	$r .= isset($control['desc2']) ? '<div class="x-desc">'. $control['desc2'] .'</div><!--.x-desc-->' : '';

	$r .= '</div>';

	return $r;
}

/* Returns string for HTML id attribute. */
function x_ui_html_id( $field_name ) { return preg_replace( '/[^A-Za-z0-9]/', '', $field_name ); }

/* Custom radio button */
function x_radio( $args ) {

	$defaults = array(
		'id' 			=> null,	// A unique slug-like ID for the theme setting field.
		'html_id' 		=> null, 	// A unique ID for HTML ID attribute
		'value' 		=> null,	// Radio value.
		'label' 		=> null, 	// String for radio label.
		'checked' 		=> false, 	// Boolean value adds the checked attribute to the current radio button.
		'class' 		=> null,	// CSS classes for radio container 
		'class_input' 	=> null, 	// CSS classes for radio input tag.
		'atts' 			=> null 	// Array of HTML attributes for radio input tag.
	);
	$args = wp_parse_args( $args, $defaults );

	// Turns arguments array into variables
	extract( $args, EXTR_SKIP );

	if ( $html_id == null ) $html_id = x_ui_html_id( $id );

	if ( !empty($class) ) $class = ' '.$class;

	$class_input = !empty($class_input) ? ' class="'. $class_input .'"' : '';

	$attributes = '';
	foreach ( (array)$atts as $key => $val ) {
		$attributes .= ' '.$key.'="'.$val.'"';
	}

	$r .= '<div class="x-radio'. $class .'">';
	$r .= '<input id="'. $html_id .'"'. $class_input .' type="radio" name="'. $id .'" value="'. $value .'" '. checked( $checked, true, false ) . $attributes .' />';
	$r .= '<label for="'. $html_id .'">';
	$r .= '<span class="radio-icon"></span>';
	if ( !empty($label) ) $r .= '<span class="radio-label-text">'. $label .'</span>';
	$r .= '</label>';
	$r .= '</div>';

	return $r;
}

/* Custom checkbox */
function x_checkbox( $args ) {

	$defaults = array(
		'id' 				=> null,	// A unique slug-like ID for the theme setting field.
		'html_id' 			=> null, 	// A unique ID for HTML ID attribute
		'value' 			=> null,	// Checkbox value.
		'label' 			=> null, 	// String for checkbox label.
		'current' 			=> 1, 		// The other value to compare.
		'unchecked_value' 	=> null,	// Fallback value for unchecked checkboxes.
		'class' 			=> null,	// CSS classes for checkbox container 
		'class_input' 		=> null, 	// CSS classes for checkbox input tag.
		'atts' 				=> null 	// Array of HTML attributes for checkbox input tag.
	);
	$args = wp_parse_args( $args, $defaults );
	extract( $args, EXTR_SKIP );

	if ( $html_id == null ) $html_id = x_ui_html_id( $id );

	if ( !empty($class) ) $class = ' '.$class;
	$class_input = !empty($class_input) ? ' class="'. $class_input .'"' : '';

	$attributes = '';
	foreach ( (array)$atts as $key => $val ) {
		$attributes .= ' '.$key.'="'.$val.'"';
	}

	$r .= '<div class="x-checkbox'. $class .'">';
	if ( $unchecked_value !== false ) $r .= '<input type="hidden" name="'. $id .'" value="'. $unchecked_value .'" />';
	$r .= '<input id="'. $html_id .'"'. $class_input .' type="checkbox" name="'. $id .'" value="1"'. checked( $value, $current, false ) . $attributes .' />';
	$r .= '<label for="'. $html_id .'">';
	$r .= '<span class="checkbox-icon"></span>';
	if ( !empty($label) ) $r .= '<span class="checkbox-label-text">'. $label .'</span>';
	$r .= '</label>';
	$r .= '</div>';

	return $r;
}

/* Editable select dropdown options */
function x_select_editable( $args ) {

	$defaults = array(
		'id' 			=> null,
		'html_id' 		=> null,
		'choices' 		=> null,
		'value' 		=> null,
		'placeholder' 	=> null,
		'class' 		=> null
	);
	$args = wp_parse_args( $args, $defaults );
	extract( $args, EXTR_SKIP );

	if ( $html_id == null ) $html_id = x_ui_html_id( $id );

	$text_id = x_ui_html_id( $id ).'text';
	$text = x_get_options( $text_id, $value );
	$placeholder = $placeholder !== null ? ' placeholder="'. $placeholder .'"' : '';
	if ( !empty($class) ) $class = ' '.$class;

	$r .= '<div id="'. $html_id .'" class="x-select-editable'. $class .'">';
	$r .= '<select id="'. $html_id .'-select">';
	foreach ( $choices as $k => $v )
		$r .= '<option value="'. $k .'"'. selected( $value, $k, false ) .' />'. $v .'</option>';
	$r .= '</select>';
	$r .= '<input id="'. $html_id.'-text" type="text" name="'. $text_id .'" value="'. $text .'"'. $placeholder .' />';
	$r .= '<input id="'. $html_id.'-value" type="hidden" name="'. $id .'" value="'. $value .'" />';
	$r .= '</div>';
	
	return $r;
}

/* WP media uploader */
function x_media( $args ) {

	$defaults = array(
		'id' 			=> null,
		'html_id' 		=> null,
		'value' 		=> null,
		'preview' 		=> null,
		'placeholder' 	=> 'Media URL',
		'class' 		=> null
	);
	$args = wp_parse_args( $args, $defaults );
	extract( $args, EXTR_SKIP );

	if ( $html_id == null ) $html_id = x_ui_html_id( $id );

	$media_vars = array( 'id', 'type', 'url' );
	foreach ( $media_vars as $key ) if ( !isset($value[$key]) ) $value[$key] = '';

	$class = !empty($class) ? ' '.$class : '';
	$class .= !empty($value['url']) ? ' with-media' : '';
	$data = 'data-preview="'. ( $preview == null ? $html_id.'-preview' : $preview ) .'"';
	$placeholder = !empty($placeholder) ? ' placeholder="'.$placeholder.'"' : '';

	$r .= '<div id="'. $html_id .'" class="x-media'. $class .'"'. $data .'>';
	$r .= '<input id="'. $html_id .'-id" type="hidden" name="'. $id .'[id]" value="'. $value['id'] .'" />';
	$r .= '<input id="'. $html_id .'-type" type="hidden" name="'. $id .'[type]" value="'. $value['type'] .'" />';
	$r .= '<input id="'. $html_id .'-url" type="text" name="'. $id .'[url]" value="'. $value['url'] .'"'. $placeholder .' />';
	$r .= '<div id="'. $html_id .'-buttons" class="x-media-buttons">';
	$r .= '<button id="'. $html_id .'-add-media" class="button x-add-media" type="button"><span class="dashicons dashicons-admin-media"></span> Media</button>';
	$r .= '<button id="'. $html_id .'-remove-media" class="button x-remove-media" type="button"><span class="dashicons dashicons-no-alt"></span></button>';
	$r .='</div>';

	if ( $preview == null ) {
		$r .= '<div id="'. $html_id .'-preview" class="x-media-preview">';
		if ( $value['type'] == 'image' ) {
			$r .= '<div class="x-media-image">';
			$r .= '<a href="'. $value['url'] . '" target="_blank"><img src="'. $value['url'] . '" alt="Preview"></a>';
			$r .= '</div>';
		}
		$r .= '</div>';
	}
	$r .= '</div>';

	return $r;
}

/* Drag and drop slides */
function x_slide( $args ) {

	$defaults = array(
		'id' 		=> null,
		'html_id' 	=> null,
		'slide' 	=> null,
		'order' 	=> null,
		'class' 	=> null
	);
	$args = wp_parse_args( $args, $defaults );
	extract( $args, EXTR_SKIP );

	$slidevars = array( 'title', 'url', 'link', 'description' );
	foreach ( $slidevars as $key ) if ( !isset($slide[$key]) ) $slide[$key] = '';
	
	$title = empty($slide['title']) ? 'Slide '. ( $order + 1 ) : $slide['title'];

	if ( empty($slide['title']) ) {
		$slide_no = $order + 1;
		$slide_no_att = ' data-slide-no="'. $slide_no .'"';
		$title = 'Slide '. $slide_no;
	}
	else {
		$slide_no_att = '';
		$title = $slide['title'];
	}

	if ( !empty($class) ) $class = ' '.$class;

	$r = '<li class="x-slide'. $class .'"'. $slide_no_att .'>';

	$r .= '<div class="x-slide-header">';
	$r .= '<strong>'. $title .'</strong>';
	$r .= '</div>';

	$r .= '<div class="x-slide-body" style="display: none;">';

	$r .= x_media( array(
		'id' 		=> $id.'['.$order.'][media]',
		'html_id' 	=> $html_id.'-attachment',
		'value' 	=> $slide['media']
	) );

	$r .= '<div class="x-slide-title">';
	$r .= '<input type="text" name="'. $id.'['.$order.'][title]" value="'. stripslashes($slide['title']) .'" placeholder="Title"  />';
	$r .='</div>';

	$r .= '<div class="x-slide-description">';
	$r .= '<textarea name="'. $id.'['.$order.'][description]" placeholder="Description" >'. stripslashes($slide['description']) .'</textarea>';
	$r .='</div>';

	$r .= '<div class="x-slide-link">';
	$r .= '<input type="text" name="'. $id.'['.$order.'][link]" value="'. $slide['link'] .'" placeholder="Link URL" />';
	$r .='</div>';

	$r .= '</div>';

	$r .= '<div class="x-slide-buttons">';
	$r .= '<a class="button x-slide-delete" href="javascript:void(0);"><span class="dashicons dashicons-trash"></span></a>';
	$r .= '</div>';

	$r .= '</li>';

	return $r;
}

?>