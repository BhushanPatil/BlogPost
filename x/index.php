<?php if ( !defined('THEME_DIR') &&  !defined('THEME_URI') ) exit;

/**
 * @package 	X Panel - Admin Options Panel for Wordpress Themes
 * @version 	1.0
 * @author 		Bhushan Patil
 * @copyright 	© 2014 Bhushan Patil
 */

/* Definitions */
define( 'X_DIR', THEME_DIR.'/x' ); 		// Absolute path to the directory of X Panel for the current theme.
define( 'X_URI', THEME_URI.'/x' ); 		// X Panel directory URI for the current theme.
define( 'X_IMG', X_URI.'/img' );		// X Panel images directory URI.

/* Required files */ 
require_once( X_DIR.'/options.php' );	// X Panel theme options defined by theme author.
require_once( X_DIR.'/functions.php' ); // Necessary functions for X Panel.

/* Required action filters */
add_action( 'init', 				'x_options' ); 		// Init X Panel theme options defined by theme author.
add_action( 'admin_init', 			'x_init' );			// Init X Panel.
add_action( 'admin_menu', 			'x_menu_page' );	// Add menu options.
add_action( 'admin_head', 			'x_message' );		// Show custom message on theme activation.
add_action( 'wp_ajax_x_data_post', 	'x_data_post' ); 	// Handler for AJAX requests.

?>