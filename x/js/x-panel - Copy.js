/**
 * Contains the core functionalities to be used
 * inside Framework
 */

// jQuery.noConflict();

jQuery(document).ready( function($) {

	var xDiv = $('#x-container');

	$('.x-color').wpColorPicker();

	function select_layout( layout ) {

		var classes = [ 'x-layout-default', 'x-layout-nav-collapsed', 'x-layout-nav-hidden' ];

		if ( typeof layout === 'undefined' ) {

			$.each( classes, function( key, value ) {

				if ( xDiv.hasClass(value) ) {

					layout = classes[key+1];
					if( typeof layout === 'undefined' ) layout = classes[0];

					xDiv.removeClass(value).addClass(layout);
					$.cookie( 'x-layout', layout, { expires: 7, path: '/' } );

					return false;
				}
			});
		}
		else {
			$.each( classes, function( key, value ) { xDiv.removeClass(value); });
			xDiv.addClass(layout);
			$.cookie( 'x-layout', layout, { expires: 7, path: '/' } );
		}
	}

	var jsCollapsed = 0, w = $(window);
	w.resize( function resize(){
		if ( w.width() < 900 ) {
			if ( jsCollapsed == 0 && xDiv.hasClass('x-layout-default') ) {
				select_layout( 'x-layout-nav-collapsed' );
				jsCollapsed = 1;
			}
			$('#x-layout-toggle').on('click', function(){ jsCollapsed = 2; });
		}
		else if ( jsCollapsed == 1 && xDiv.hasClass('x-layout-nav-collapsed') ) {
			select_layout( 'x-layout-default' );
			jsCollapsed = 0;
		}
		else jsCollapsed = 0;
	}).trigger('resize');
	
	// Change layout
	$(document).on('click', '#x-layout-toggle', function() { select_layout(); });


	// Display selected group
	function select_group( group ) {
		$('#x-nav a').removeClass('selected').filter('a[href="'+ group +'"]').addClass('selected');
		$('.x-group').hide().filter(group).fadeIn(400);
		$.cookie( 'x-group', group, { expires: 7, path: '/' } );
	}
	function getParameterByName( name ) {
	    name = name.replace( /[\[]/, "\\[" ).replace( /[\]]/, "\\]" );
	    var regex = new RegExp( "[\\?&]" + name + "=([^&#]*)" ),
	        results = regex.exec( location.search );
	    return results == null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
	}
	var group = getParameterByName('group');
	if ( group.length ) select_group( '#x-group-'+group );


	// Select Menu Class
	$(document).on('click', '#x-nav a', function(e) {
		e.preventDefault();
		select_group( $(this).attr('href') );
	});

	// Quick Tooltips
	var tooltipTimeout;
	$('[placeholder],[data-tooltip]').hover(function(){

		var tooltip = $(this).data('tooltip'),
			placeholder = $(this).attr('placeholder'),
			value = $(this).attr('value');

		if ( typeof tooltip !== typeof undefined && typeof value === typeof undefined )
			var text = tooltip;
		else if ( typeof tooltip !== typeof undefined && value.length )
			var text = tooltip;
		else if ( typeof placeholder !== typeof undefined && value.length )
			var text = placeholder;
		else
			return;

 		if ( $(this).css('position') == 'absolute' ) var position = $(this).offsetParent().position();
 		else var position = $(this).position();

 	 	tooltipTimeout = setTimeout(function(){
 	 	 	$('<p class="tooltip">'+text+'</p>')
 	 	 		.css(position)
 	 	 		.fadeIn()
 	 	 		.prependTo(xDiv);
 	 	 }, 500);
	}, function() { $('.tooltip').remove(); clearTimeout(tooltipTimeout); });


	// HTML editable select dropdown list fuctions
	$('.x-select-editable').each( function() {
		var select 			= $(this).find('select'),
			inputText 		= $(this).find('input[type=text]'),
			inputValue 		= $(this).find('input[type=hidden]'),
			selectOption 	= select.find('option[value="'+ inputValue.val() +'"]');

		if ( inputText.val().length == 0 || inputValue.val() != select.val() ) select.val('');

		if ( selectOption.length ) inputText.val( selectOption.text() );
		else inputText.val( inputValue.val() );

		select.on('change', function() {
			inputText.val( select.find('option:selected').text() );
			inputValue.val( select.val() ).trigger('input');
		});

		inputText.on('input', function() {
			inputValue.val( inputText.val() ).trigger('input');
		});

		inputText.keydown( function( e ) {
			
			if ( e.keyCode == 38 ) { // Up arrow key
				var selectedOption = select.find('option:selected');
				if ( selectedOption.length )
					selectedOption.attr( 'selected', false ).prev().attr( 'selected', true ).trigger('change');
				else
					select.find('option:last').attr( 'selected', true ).trigger('change');
			}
			else if ( e.keyCode == 40 ) { // Down arrow key
				var selectedOption = select.find('option:selected');
				if ( selectedOption.is(':not(:last-child)') ) {
					if ( selectedOption.length )
						selectedOption.attr( 'selected', false ).next().attr( 'selected', true ).trigger('change');
					else
						select.find('option:first').attr( 'selected', true ).trigger('change');
				}
			}
		});
	});


	// Fold functions
	$('.x-fold').each( function() {

		// Switch
		$(this).find('.x-switch').each( function() {
			var checkbox = $(this).find('input[type="checkbox"]'),
				id = $(this).closest('.x-control').attr('id');

			if ( checkbox[0].checked )
				$( '.x-fold-switch-'+id ).slideDown();
			else
				$( '.x-fold-switch-'+id ).slideUp();

			checkbox.on('change', function() {
				if ( this.checked )
					$( '.x-fold-switch-'+id ).slideDown();
				else
					$( '.x-fold-switch-'+id ).slideUp();
			});
		});

		// Checkbox
		$(this).find('.x-checkbox').each( function() {
			var checkbox = $(this).find('input[type="checkbox"]'),
				id = $(this).closest('.x-control').attr('id');

			if ( checkbox[0].checked )
				$( '.x-fold-checkbox-'+id ).slideDown();
			else
				$( '.x-fold-checkbox-'+id ).slideUp();

			checkbox.on('change', function() {
				if ( this.checked )
					$( '.x-fold-checkbox-'+id ).slideDown();
				else
					$( '.x-fold-checkbox-'+id ).slideUp();
			});
		});

		$(this).find('.x-radio').each( function() {

			var radio = $(this).find('input[type="radio"]'),
				id = radio.attr('id');

			if ( radio[0].checked )
				$( '.x-'+id ).slideDown();
			else
				$( '.x-'+id ).slideUp();

			radio.on('change', function() {

				var controlID = $(this).closest('.x-control').attr('id');
				
				id = $(this).attr('id');

				$( '.x-fold-radio-'+controlID ).slideUp();
				$( '.x-'+id ).slideDown();

			});
		});
	});
	

	// Align Popup Messages
	$.fn.center = function () {
		if ( xDiv.height() < $(window).height() )
			this.css( 'top', ( xDiv.height() - this.height() ) / 2 );
		else
			this.css( 'top', ( $(window).height() - this.height() ) / 2 + $(window).scrollTop() );
		this.css( 'left', ( xDiv.width() - this.width() ) / 2 );
		return this;
	}


	/* AJAX Save Options */
	$(document).on('click', '.x-save-button', function() {

		if ( typeof tinyMCE !== 'undefined' ) tinyMCE.triggerSave();

		var progressImg = $('.x-ajax-save-img'),
			data = {
				type: 'save',
				action: 'x_data_post',
				nonce: $('#x-nonce').val(),
				data: $('#x-form :input[name]').serialize()
			};

		progressImg.fadeIn();

		$.post( ajaxurl, data, function(response) {

			progressImg.fadeOut();
			
			if ( response == 1 ) {
				var successPopup = $('#x-popup-save');
				successPopup.fadeIn(200).find('.x-popup-message').center();
				window.setTimeout( function() { successPopup.fadeOut(); }, 2000 );
			}
			else {
				var errorPopup = $('#x-popup-error');
				errorPopup.fadeIn(200).find('.x-popup-message').center();
				window.setTimeout( function() { errorPopup.fadeOut(); }, 2000 );
			}
		});
	});

	/* AJAX Options Reset */
	$(document).on('click', '.x-reset-button', function() {

		var answer = confirm("Click OK to reset. All settings will be lost and replaced with default settings!");

		if ( answer ) {

			var progressImg = $('.x-ajax-reset-img');
						
			progressImg.fadeIn();
							
			var data = {
				type: 'reset',
				action: 'x_data_post',
				nonce: $('#x-nonce').val()
			};

			$.post( ajaxurl, data, function(response) {

				progressImg.fadeOut();

				if ( response == 1 ) {
					var successPopup = $('#x-popup-reset');
					successPopup.fadeIn(200).find('.x-popup-message').center();
					window.setTimeout( function() {
						// location.reload();
						window.location.href = window.location.href.split('?')[0] +'?page=themeoptions';
					}, 1000 );
				}
				else {
					var errorPopup = $('#x-popup-error');
					errorPopup.fadeIn(200).find('.x-popup-message').center();
					window.setTimeout( function() { errorPopup.fadeOut(); }, 2000 );
				}
			});
		}
	});

	/* AJAX Backup & Restore */
	$(document).on('click', '.x-backup-button', function() {

		var answer = confirm('Click OK to backup your current saved options.');

		if ( answer ) {

			var data = {
				action: 'x_data_post',
				type: 'backup',
				nonce: $('#x-nonce').val()
			};

			$.post( ajaxurl, data, function(response) {
				if ( response == 1 ) {
					var successPopup = $('#x-popup-backup');
					successPopup.fadeIn(200).find('.x-popup-message').center();
					window.setTimeout(function(){ location.reload(); }, 1000);
				}
				else {
					var errorPopup = $('#x-popup-error');
					errorPopup.fadeIn(200).find('.x-popup-message').center();
					window.setTimeout( function() { errorPopup.fadeOut(); }, 2000 );
				}
			});

		}
	});

	/* AJAX Restore Options */
	$(document).on('click', '.x-restore-button', function() {

		var answer = confirm('Warning: All of your current options will be replaced with the data from your last backup! Proceed?')

		if ( answer ) {

			var data = {
				action: 'x_data_post',
				type: 'restore',
				nonce: $('#x-nonce').val()
			};
						
			$.post( ajaxurl, data, function(response) {
				if ( response == 1 ) {
					var successPopup = $('#x-popup-save');
					successPopup.fadeIn(200).find('.x-popup-message').center();
					window.setTimeout(function(){ location.reload(); }, 1000);
				}
				else {
					var errorPopup = $('#x-popup-error');
					errorPopup.fadeIn(200).find('.x-popup-message').center();
					window.setTimeout( function() { errorPopup.fadeOut(); }, 2000 );
				}
			});

		}		
	});

	/* Ajax Transfer (Import/Export) Option */
	$(document).on('click', '.x-import-button', function() {
		
		var answer = confirm('Click OK to import options.')

		if ( answer ) {

			var data = {
				action: 'x_data_post',
				type: 'import',
				nonce: $('#x-nonce').val(),
				data: $(this).siblings('textarea').val()
			};
			
			$.post( ajaxurl, data, function(response) {
				if ( response == 1 ) {
					var successPopup = $('#x-popup-save');
					successPopup.fadeIn(200).find('.x-popup-message').center();
					window.setTimeout(function(){ location.reload(); }, 1000);
				}
				else {
					var errorPopup = $('#x-popup-error');
					errorPopup.fadeIn(200).find('.x-popup-message').center();
					window.setTimeout( function() { errorPopup.fadeOut(); }, 2000 );
				} 
			});

		}			
	});



	// media fucntions
	$(document).on('click', '.x-add-media', function() {

		// Uploading files variable
		var frame,
			attachment
			parent = $(this).parents('.x-media'),
			id = parent.attr('id'),
			preview = $('#'+parent.data('preview'));

		// If the media frame already exists, reopen it.
		if ( frame ) {
			frame.open();
			return;
		}

		// Create WP media frame.
		frame = wp.media();

		// When frame is open select attachments
		frame.on('open',function() {
			var selection = frame.state().get('selection');
			ids = $('#'+id+'-id').val().split(',');
			ids.forEach(function(id) {
				attachment = wp.media.attachment(id);
				attachment.fetch();
				selection.add( attachment ? [attachment] : [] );
			});
		});

		// When an attachment is selected, run a callback
		frame.on( 'select', function() {

			// Grab the selected attachment.
			attachment = frame.state().get('selection').first().toJSON();

			parent.addClass('with-media');
			$('#'+id+'-id').val( attachment.id );
			$('#'+id+'-type').val( attachment.type );
			$('#'+id+'-url').val( attachment.url );

			if ( attachment.type == 'image' ) {

				var maxWidth = '';
				if ( attachment.width < parent.width() ) maxWidth = ' style="max-width:'+ attachment.width +'px;"';
				preview.empty();
				preview.append('<div class="x-media-image"><a href="' + attachment.url + '" target="_blank"><img src="'+ attachment.url +'" alt="Preview"'+ maxWidth +' /></a></div>');
			}
		});

		// Finally, open the modal.
		frame.open();
	});
	$(document).on('click', '.x-remove-media', function() {
		var parent = $(this).parents('.x-media');
		parent.removeClass('with-media');
		parent.find('input').val('');
		$('#'+parent.data('preview')).empty();
    });
    $('.x-media-image img').each( function() {
    	var tmpImg = new Image();
    	tmpImg.src = ( this.getAttribute ? this.getAttribute('src') : false ) || this.src;
    	if ( tmpImg.width < $(this).parents('.x-media-preview').width() ) $(this).css('max-width',tmpImg.width+'px');
    });


    // Range fucntions
	$('.x-range-slider').each( function() {

		var rangeUI 		= $(this),
			inputField 		= $(this).prev(),
			animate 		= rangeUI.data('animate') || true,
			max 			= parseInt(rangeUI.data('max')) || 100,
			min 			= parseInt(rangeUI.data('min')) || 0,
			orientation 	= rangeUI.data('orientation') || 'horizontal',
			range 			= rangeUI.data('range') || 'min',
			step 			= parseInt(rangeUI.data('step')) || 1,
			value 			= parseInt(inputField.val()) || 0;

		rangeUI.slider({
			animate: 		animate,
			max: 			max,
			min: 			min,
			orientation: 	orientation,
			range: 			range,
			step: 			step,
			value: 			value,
			slide: function( e, ui ) { inputField.val(ui.value); },
			change: function( e, ui ) { inputField.val(ui.value); }
		});

		inputField.on( 'change', function() {
			rangeUI.slider( 'value', parseInt(this.value) || 0 );
		});
	});


	// Sorter fucntions
	$('.x-control-sorter').each( function() {

		var sorter = $(this),
			id = $(this).attr('id');

		sorter.find('.x-sorter-group').sortable({
			items: '.x-sorter-item',
			placeholder: 'x-sorter-placeholder',
			connectWith: '.'+id+'-group',
			update: function() {
				sorter.find('input').each( function() {
					var group 	= $(this).closest('.'+id+'-group').data('group'),
						item 	= $(this).data('item');
					$(this).prop( 'name', id+'['+group+']['+item+']' );
				});
			}
		});	
	});


	// Slides fucntions
	$.fn.serializeSlides = function() {
		this.find('.x-slide').each( function( i ) {
			$(this).find('[name]:input').each( function() {
				var name = $(this).attr('name').replace( /\[\d\]/, '['+ i +']' );
				$(this).attr( 'name', name );
			});
			i++;
		});
	};
	$('.x-control-slides').each( function() {
		var slides = $(this).find('.x-slides');
		slides.sortable({
			placeholder: 'x-slide-placeholder',
			handle: '.x-slide-header',
			update: function () { slides.serializeSlides(); }
		});
	});
	// Update slide title upon typing
	$(document).on( 'keyup', '.x-slide-title input', function() {
		var title = this.value, element = $(this).closest('.x-slide-body').prev().find('strong');
		if ( this.timer ) clearTimeout( this.timer );
		this.timer = setTimeout( function(){ element.text( title ); }, 1000 );
	});
	// Switch the "Open" and "Close" state per click then slide up/down (depending on open/close state)
	$(document).on( 'click', '.x-slide-header', function() {
		$(this).next().slideToggle().parent().toggleClass('x-slide-open');
	});
	// Remove individual slide
	$(document).on( 'click', '.x-slide-delete', function() {
		$(this).addClass('active');
		var agree = confirm('Are you sure you wish to delete this slide?');
		if ( agree ) {
			var slide = $(this).closest('.x-slide');
				slides = $(this).closest('.x-slides');
			slide.animate( { opacity: 0, height: 0 }, 400, function() {
				slide.remove();
				slides.serializeSlides();
			});
			
		}
		else $(this).removeClass('active');
	});
	// Add new slide
	$(document).on( 'click', '.x-slide-add', function() {

		var slides 			= $(this).prev(),
			id 				= $(this).closest('.x-control').attr('id'),
			fieldName 		= $(this).closest('.x-control').data('field-name'),
			order 			= slides.find('.x-slide').length,
			slideID 		= id+'-'+order;
			slideFieldName 	= fieldName  +'['+ order +']';
			numArr 			= slides.find('.x-slide').map( function() { return $(this).data('slide-no'); } ).get(),
			maxNum 			= Math.max.apply( Math, numArr );

		if ( maxNum < 1 ) maxNum = 0;
		var slideNo = maxNum + 1;

		var newSlide = '<li class="x-slide" data-slide-no="'+ slideNo +'"><div class="x-slide-header"><strong>Slide '+ slideNo +'</strong></div><div class="x-slide-body" style="display: none;"><div id="'+ slideID +'-attachment" class="x-media" data-preview="'+ slideID +'-attachment-preview"><input id="'+ slideID +'-attachment-id" type="hidden" name="'+ slideFieldName +'[media][id]" value=""><input id="'+ slideID +'-attachment-type" type="hidden" name="'+ slideFieldName +'[media][type]" value=""><input id="'+ slideID +'-attachment-url" type="text" name="'+ slideFieldName +'[media][url]" value="" placeholder="Media URL"><div id="'+ slideID +'-attachment-buttons" class="x-media-buttons"><button id="'+ slideID +'-attachment-add-media" class="button x-add-media" type="button"><span class="dashicons dashicons-admin-media"></span> Media</button><button id="'+ slideID +'-attachment-remove-media" class="button x-remove-media" type="button"><span class="dashicons dashicons-no-alt"></span></button></div><div id="'+ slideID +'-attachment-preview" class="x-media-preview"></div></div><div class="x-slide-title"><input type="text" name="'+ slideFieldName +'[title]" value="" placeholder="Title"></div><div class="x-slide-description"><textarea name="'+ slideFieldName +'[description]" placeholder="Description"></textarea></div><div class="x-slide-link"><input type="text" name="'+ slideFieldName +'[link]" value="" placeholder="Link URL"></div></div><div class="x-slide-buttons"><a class="button x-slide-delete" href="javascript:void(0);"><span class="dashicons dashicons-trash"></span></a></div></li>';

		slides.append( $(newSlide).hide().fadeIn() );
	});


	// Code editor fucntions
	$('.x-control-code').each( function() {

		var id = $(this).attr('id'),
			textarea = $(this).find('textarea'),
			mode = textarea.data('mode'),
			theme = textarea.data('theme');

		if ( $.browser.msie && parseInt( $.browser.version, 10 ) <= 7 || typeof ace == 'undefined' ) {
			$(this).find('#'+id+'-code').hide();
			textarea.show();
		}
		else {
			var editor = ace.edit( id+'-code' );
			editor.getSession().setUseWrapMode( true );
			editor.getSession().setValue( textarea.val() );
			if ( mode ) editor.getSession().setMode( 'ace/mode/'+mode );
			if ( theme ) editor.setTheme( 'ace/theme/'+theme );
			editor.setOptions({ minLines: 5, maxLines: 15 });
			editor.getSession().on( 'change', function() {
				textarea.val( editor.getSession().getValue() );
			});
		}
	});
	

	// Typography fucntions
	function update_font_options( id ) {

		var family 		= $( '#'+ id +'-family' ).children(':selected'),
			variant 	= $( '#'+ id +'-variant' ),
			variantVal 	= variant.val(),
			variants 	= $( '#'+ id +'-web-font-variants' ),
			subsets 	= $( '#'+ id +'-web-font-subsets' );

		variant.find('option').remove();
		var options = '';
		if ( family.hasClass('variant-lighter') )
			options += '<option value="lighter">Lighter</option>';
		if ( family.hasClass('variant-lighteritalic') )
			options += '<option value="lighteritalic">Lighter Italic</option>';
		if ( family.hasClass('variant-normal') )
			options += '<option value="normal">Normal</option>';
		if ( family.hasClass('variant-regular') )
			options += '<option value="regular">Regular</option>';
		if ( family.hasClass('variant-italic') )
			options += '<option value="italic">Italic</option>';
		if ( family.hasClass('variant-bold') )
			options += '<option value="bold">Bold</option>';
		if ( family.hasClass('variant-bolditalic') )
			options += '<option value="bolditalic">Bold Italic</option>';
		if ( family.hasClass('variant-bolder') )
			options += '<option value="bolder">Bolder</option>';
		if ( family.hasClass('variant-bolderitalic') )
			options += '<option value="bolderitalic">Bolder Italic</option>';
		if ( family.hasClass('variant-100') )
			options += '<option value="100">Thin 100</option>';
		if ( family.hasClass('variant-100italic') )
			options += '<option value="100italic">Thin 100 Italic</option>';
		if ( family.hasClass('variant-200') )
			options += '<option value="200">Extra Light 200</option>';
		if ( family.hasClass('variant-200italic') )
			options += '<option value="200italic">Extra Light 200 Italic</option>';
		if ( family.hasClass('variant-300') )
			options += '<option value="300">Light 300</option>';
		if ( family.hasClass('variant-300italic') )
			options += '<option value="300italic">Light 300 Italic</option>';
		if ( family.hasClass('variant-400') )
			options += '<option value="400">Normal 400</option>';
		if ( family.hasClass('variant-400italic') )
			options += '<option value="400italic">Normal 400 Italic</option>';
		if ( family.hasClass('variant-500') )
			options += '<option value="500">Medium 500</option>';
		if ( family.hasClass('variant-500italic') )
			options += '<option value="500italic">Medium 500 Italic</option>';
		if ( family.hasClass('variant-600') )
			options += '<option value="600">Semi-Bold 600</option>';
		if ( family.hasClass('variant-600italic') )
			options += '<option value="600italic">Semi-Bold 600 Italic</option>';
		if ( family.hasClass('variant-700') )
			options += '<option value="700">Bold 700</option>';
		if ( family.hasClass('variant-700italic') )
			options += '<option value="700italic">Bold 700 Italic</option>';
		if ( family.hasClass('variant-800') )
			options += '<option value="800">Extra Bold 800</option>';
		if ( family.hasClass('variant-800italic') )
			options += '<option value="800italic">Extra Bold 800 Italic</option>';
		if ( family.hasClass('variant-900') )
			options += '<option value="900">Ultra Bold 900</option>';
		if ( family.hasClass('variant-900italic') )
			options += '<option value="900italic">Ultra Bold 900 Italic</option>';
		variant.append(options);

		if ( variantVal == '400' ) {

			var four = variant.find('option[value="400"]'),
				regular = variant.find('option[value="regular"]');

			if ( four.length )
				four.attr( 'selected', true );
			else if ( regular.length )
				regular.attr( 'selected', true );
			else
				variant.find('option').eq(0).attr( 'selected', true );
		}
		else if ( variantVal == 'regular' ) {

			var regular = variant.find('option[value="regular"]'),
				normal = variant.find('option[value="normal"]');

			if ( regular.length )
				regular.attr( 'selected', true );
			else if ( normal.length )
				normal.attr( 'selected', true );
			else
				variant.find('option').eq(1).attr( 'selected', true );
		}
		else if ( variantVal == 'normal' ) {

			var normal = variant.find('option[value="normal"]'),
				regular = variant.find('option[value="regular"]');

			if ( normal.length )
				normal.attr( 'selected', true );
			else if ( regular.length )
				regular.attr( 'selected', true );
			else
				variant.find('option').eq(0).attr( 'selected', true );
		}
		else {

			var option = variant.find('option[value="'+ variantVal +'"]');

			if ( option.length ) 
				option.attr( 'selected', true );
			else
				variant.find('option').eq(1).attr( 'selected', true );
		}

		if ( family.hasClass('web-font') ) {

			variants.find('input').attr('disabled',true).parent().hide();

			if ( family.hasClass('variant-100') )
				variants.find('.variant-100').attr('disabled',false).parent().show();
			if ( family.hasClass('variant-200') )
				variants.find('.variant-200').attr('disabled',false).parent().show();
			if ( family.hasClass('variant-300') )
				variants.find('.variant-300').attr('disabled',false).parent().show();
			if ( family.hasClass('variant-400') || family.hasClass('variant-regular') )
				variants.find('.variant-400').attr('disabled',false).parent().show();
			if ( family.hasClass('variant-500') )
				variants.find('.variant-500').attr('disabled',false).parent().show();
			if ( family.hasClass('variant-600') )
				variants.find('.variant-600').attr('disabled',false).parent().show();
			if ( family.hasClass('variant-700') )
				variants.find('.variant-700').attr('disabled',false).parent().show();
			if ( family.hasClass('variant-800') )
				variants.find('.variant-800').attr('disabled',false).parent().show();
			if ( family.hasClass('variant-900') )
				variants.find('.variant-900').attr('disabled',false).parent().show();
			if ( family.hasClass('variant-100italic') )
				variants.find('.variant-100italic').attr('disabled',false).parent().show();
			if ( family.hasClass('variant-200italic') )
				variants.find('.variant-200italic').attr('disabled',false).parent().show();
			if ( family.hasClass('variant-300italic') )
				variants.find('.variant-300italic').attr('disabled',false).parent().show();
			if ( family.hasClass('variant-400italic') || family.hasClass('variant-italic') )
				variants.find('.variant-400italic').attr('disabled',false).parent().show();
			if ( family.hasClass('variant-500italic') )
				variants.find('.variant-500italic').attr('disabled',false).parent().show();
			if ( family.hasClass('variant-600italic') )
				variants.find('.variant-600italic').attr('disabled',false).parent().show();
			if ( family.hasClass('variant-700italic') )
				variants.find('.variant-700italic').attr('disabled',false).parent().show();
			if ( family.hasClass('variant-800italic') )
				variants.find('.variant-800italic').attr('disabled',false).parent().show();
			if ( family.hasClass('variant-900italic') )
				variants.find('.variant-900italic').attr('disabled',false).parent().show();

			if( variants.find('input:not([disabled])').length > 1 ) variants.show();
			else variants.hide();

			subsets.find('input').attr('disabled',true).parent().hide();

			if ( family.hasClass('subset-latin') )
				subsets.find('.subset-latin').attr('disabled',false).parent().show();
			if ( family.hasClass('subset-latin-ext') )
				subsets.find('.subset-latin-ext').attr('disabled',false).parent().show();
			if ( family.hasClass('subset-greek') )
				subsets.find('.subset-greek').attr('disabled',false).parent().show();
			if ( family.hasClass('subset-greek-ext') )
				subsets.find('.subset-greek-ext').attr('disabled',false).parent().show();
			if ( family.hasClass('subset-cyrillic') )
				subsets.find('.subset-cyrillic').attr('disabled',false).parent().show();
			if ( family.hasClass('subset-cyrillic-ext') )
				subsets.find('.subset-cyrillic-ext').attr('disabled',false).parent().show();
			if ( family.hasClass('subset-vietnamese') )
				subsets.find('.subset-vietnamese').attr('disabled',false).parent().show();
			if ( family.hasClass('subset-khmer') )
				subsets.find('.subset-khmer').attr('disabled',false).parent().show();

			if ( subsets.find('input:not([disabled])').length == 1 )
				subsets.find('input:not([disabled])').attr({ disabled:true, checked:true });
			else if ( subsets.find('input:not(.subset-latin):checked').length )
				subsets.find('input').removeAttr('disabled');
			else
				subsets.find('input.subset-latin').attr({ disabled:true, checked:true });

			subsets.show();
		}
		else {
			variants.hide().find('input').attr('disabled',true).parent().hide();
			subsets.hide().find('input').attr('disabled',true).parent().hide();
		}
	}
	function check_font_variants( id ) {

		var variantVal 	= $('#'+id+'-variant').val()
			variants 	= $('#'+id+'-web-font-variants');

		variants.find('input').removeAttr('checked');
		variants.find('input.user-selected').attr('checked',true);
		variants.find('input.js-selected').removeClass('js-selected').removeAttr('disabled');
		
		if ( variantVal == 'regular' ) {
			var regular = variants.find('input.variant-400:not([disabled].js-selected)');
			if ( regular.length ) regular.addClass('js-selected').attr({ disabled:true, checked:true });
			else variants.find('input:not:not(.js-selected)').eq(0).addClass('js-selected').attr({ disabled:true, checked:true });
		}
		else if ( variantVal == 'italic' ) {
			var italic = variants.find('input.variant-400italic:not([disabled].js-selected)');
			if ( italic.length ) italic.addClass('js-selected').attr({ disabled:true, checked:true });
			else variants.find('input.italic:not(.js-selected)').eq(0).addClass('js-selected').attr({ disabled:true, checked:true });
		}
		else variants.find('input.variant-'+variantVal).addClass('js-selected').attr({ disabled:true, checked:true });
	}
	function update_font_preview( id ) {

		var cssLinkID 	= id+'-css-link',
			family 		= $('#'+id+'-family').val(),
			variant 	= $('#'+id+'-variant').val(),
			fallback 	= $('#'+id+'-fallback-value').val(),
			size 		= $('#'+id+'-size-value').val(),
			height 		= $('#'+id+'-height').val(),
			preview 	= $('#'+id+'-preview'),
			option 		= $('#'+id+'-family').children(':selected');

		$( '#'+cssLinkID ).remove();

		if ( option.hasClass('web-font') ) {

			var urlFontStr = family.replace(/\s+/g,'+'), // Replace spaces with "+" sign
				urlVariantStr = '',
				urlSubsetsStr = '',
				subsetsArray = new Array();

			$('#'+ id +'-web-font-subsets').find('input:checked:not([disabled])').each(function() {
				subsetsArray.push( $(this).data('subset') );
			});
			if ( subsetsArray.length ) {
				urlSubsetsStr = '&amp;subset=';
				var i;
				for ( i = 0; i < subsetsArray.length; i++ ) {
					if ( i == (subsetsArray.length-1) ) urlSubsetsStr += subsetsArray[i];
					else urlSubsetsStr += subsetsArray[i]+',';
				}
			}

			if ( variant != 'regular' ) urlVariantStr = ':'+variant;

			$('head').append('<link id="'+ cssLinkID +'" href="http://fonts.googleapis.com/css?family='+ urlFontStr + urlVariantStr + urlSubsetsStr +'" rel="stylesheet" type="text/css">');
		}

		if ( fallback ) preview.css( 'font-family', '\''+family+'\', '+fallback );
		else preview.css( 'font-family', '\''+family+'\'' );

		preview.css( 'font-size', size );
		preview.css( 'line-height', height );

		if ( variant.indexOf('normal') >= 0 || variant.indexOf('regular') >= 0 ) {
			preview.css( { 'font-weight': 'normal', 'font-style': 'normal' } );
		}
		else {

			if ( variant.indexOf('italic') >= 0 ) preview.css( 'font-style', 'italic' );
			else preview.css( 'font-style', '' );

			if ( variant.indexOf('100') >= 0 ) preview.css( 'font-weight', '100' );
			else if ( variant.indexOf('200') >= 0 ) preview.css( 'font-weight', '200' );
			else if ( variant.indexOf('300') >= 0 ) preview.css( 'font-weight', '300' );
			else if ( variant.indexOf('400') >= 0 ) preview.css( 'font-weight', '400' );
			else if ( variant.indexOf('500') >= 0 ) preview.css( 'font-weight', '500' );
			else if ( variant.indexOf('600') >= 0 ) preview.css( 'font-weight', '600' );
			else if ( variant.indexOf('700') >= 0 ) preview.css( 'font-weight', '700' );
			else if ( variant.indexOf('800') >= 0 ) preview.css( 'font-weight', '800' );
			else if ( variant.indexOf('900') >= 0 ) preview.css( 'font-weight', '900' );
			else if ( variant.indexOf('bolder') >= 0 ) preview.css( 'font-weight', 'bolder' );
			else if ( variant.indexOf('bold') >= 0 ) preview.css( 'font-weight', 'bold' );
			else if ( variant.indexOf('lighter') >= 0 ) preview.css( 'font-weight', 'lighter' );
			else preview.css( 'font-weight', '' );
		}
	}
	$('.x-control-typography').each( function() {

		var id = $(this).attr('id');

		update_font_options( id );
		check_font_variants( id );
		update_font_preview( id );

		$( '#'+ id +'-family' ).on('change', function() {
			update_font_options( id );
			check_font_variants( id );
			update_font_preview( id );
		});

		$( '#'+ id +'-variant' ).on('change', function() {
			check_font_variants( id );
			update_font_preview( id );
		});

		$( '#'+ id +'-size-value,#'+ id +'-height' ).on('input', function() {
			update_font_preview( id );
		});

		$( '#'+ id +'-web-font-variants h4,#'+ id +'-web-font-subsets h4' ).on('click', function() {
			$(this).next().slideToggle().parent().toggleClass('active');
		});

		$( '#'+ id +'-web-font-variants input' ).on('click', function() {
			if ( $(this)[0].checked ) $(this).addClass('user-selected');
			else $(this).removeClass('user-selected');
		});

		$( '#'+ id +'-web-font-subsets input' ).on('click',function() {
			var parent = $('#'+ id +'-web-font-subsets');
			if ( parent.find('input:not(.subset-latin):checked').length ) parent.find('input').removeAttr('disabled');
			else parent.find('input.subset-latin').attr({ disabled:true, checked:true });
		});
	});


	$(document).on('change', '.x-taxonomy-term-select', function() {
		var option = $(this).find('option:selected');
		$(this).siblings('.x-term-taxonomy').val( option.data('taxonomy') );
		$(this).siblings('.x-term-name').val( option.data('name') );
		$(this).siblings('.x-term-id').val( option.val() );
	});


	// Categories control fucntions
	function serialize_taxonomies( id ) {
		$('#'+ id +' .x-taxonomies-term').each( function( i ) {
			$(this).find('[name]:input').each( function() {
				var name = $(this).attr('name').replace( /\[\d\]/, '['+i+']' );
				$(this).attr( 'name', name );
			});
			i++;
		});
	}
	$('.x-control-taxonomies').each( function() {
		var id = $(this).attr('id');
		$(this).sortable({
			items: '.x-taxonomies-term',
			placeholder: 'x-taxonomies-term-placeholder',
			start: function ( event, ui ) { ui.placeholder.append( ui.item.find('.x-term-name-text').text() ); },
			update: function ( event, ui ) { serialize_taxonomies( id ); }
		});
	});
	$(document).on('click', '.x-taxonomies-term', function() {
		var id = $(this).closest('.x-taxonomies').attr('id');
		$(this).remove();
		serialize_taxonomies( id );
	});
	$(document).on('change', '.x-taxonomies-term-select', function() {

		var id = $(this).parent().attr('id'),
			order = $('#'+ id +' .x-taxonomies-term').length,
			option = $(this).find('option:selected'),
			term_id = option.val(),
			name = option.data('name'),
			taxonomy = option.data('taxonomy'),
			icon = $( '#'+id+'-'+taxonomy+'-icon' ),
			css_class = '';

		if ( icon.length ) {
			css_class = ' with-icon';
			icon = '<span class="dashicons '+ icon.val() +' icon-taxonomy"></span>';
		}
		else icon = '';

		var append = '<span class="x-taxonomies-term button'+ css_class +'"><input type="hidden" name="'+ id+'['+order+'][term_id]" value="'+ term_id +'" /><input type="hidden" name="'+ id+'['+order+'][name]" value="'+ name +'" /><input type="hidden" name="'+ id+'['+order+'][taxonomy]" value="'+ taxonomy +'" />'+ icon +'<span class="x-term-name-text">'+ name +'</span><span class="dashicons dashicons-no-alt icon-remove"></span></span>';
		$(this).prev().append( $(append).css( 'display', 'inline-block' ).hide().fadeIn(200) );
	});


	function serialize_posts( id ) {
		$('#'+ id +' .x-post-value').each( function( i ) {
			$(this).attr( 'name', id+'['+i+']' );
			i++;
		});
	}
	$('.x-control-posts').each( function() {
		var id = $(this).attr('id');
		$(this).sortable({
			items: '.x-selected-post',
			placeholder: 'x-post-placeholder',
			start: function( event, ui ) { ui.placeholder.append( ui.item.html() ); },
			update: function ( event, ui ) { serialize_posts( id ); }
		});
	});
	$(document).on('click', '.x-post-remove', function() {
		var id = $(this).closest('.x-posts').attr('id');
		$(this).closest('.x-selected-post').remove();
		serialize_posts( id );
	});
	$(document).on('change', '.x-posts-select', function() {
		var id = $(this).parent().attr('id'),
			option = $('#'+ id +' option:selected'),
			order = $('#'+ id +' input').length;
			title = option.data('title'),
			image = option.data('image');

		if ( !title.trim() ) title = '<span class="untitled">(Untitled)</span>';
		if ( image.trim() ) image = '<img src="'+ image +'" alt="Featured Image" />';


		var append = '<div class="x-selected-post"><input class="x-post-value" type="hidden" name="'+ id +'['+ order +']" value="'+ option.val() +'"><div class="x-post-image"><span class="dashicons dashicons-format-aside"></span>'+ image +'</div><div class="x-post-info"><span class="x-post-name"><a href="'+ option.data('link') +'" target="_blank">'+ title +'</a></span><span class="x-post-date">'+ option.data('date') +'</span><span class="x-post-author">'+ option.data('author') +'</span></div><a class="x-post-remove" href="javascript:void(0);">Remove</a></div>';

		$(this).prev().append( $(append).css( 'display', 'block' ).hide().fadeIn(200) );
	});
	

	// Gallery function
	function gallery_selection( shortcode ) {

	    var shortcode = wp.shortcode.next( 'gallery', shortcode ),
	    	defaultPostId = wp.media.gallery.defaults.id,
	    	attachments,
	    	selection;

	    // Bail if we didn't match the shortcode or all of the content.
	    if ( !shortcode ) {
	    	return;
	    }

	    shortcode = shortcode.shortcode;

	    if ( typeof shortcode.get('id') != 'undefined' && typeof defaultPostId != 'undefined' ) {
	        shortcode.set('id', defaultPostId);
	    }

	    attachments = wp.media.gallery.attachments(shortcode);
	    selection = new wp.media.model.Selection(attachments.models, {
	        props   : attachments.props.toJSON(),
	        multiple: true
	    });

	    selection.gallery = attachments.gallery;

	    // Fetch the query's attachments, and then break ties from the query to allow for sorting.
	    selection.more().done(function () {
	        // Break ties with the query.
	        selection.props.set({ query: false  });
	        selection.unmirror();
	        selection.props.unset('orderby');
	    });

	    return selection;
	}
	$(document).on('click', '.x-gallery-create,.x-gallery-edit', function() {

		var frame,
			attachment,
			parent = $(this).closest('.x-control'),
			input = parent.find('.x-gallery-shortcode'),
			selection = gallery_selection( input.val() );

		// Create WP media frame.
		frame = wp.media({
			frame: 		'post',
			state: 		'gallery-edit',
			selection: 	selection
		});

		// When an gallery is created/updated, run a callback
		frame.on('update', function() {

			var library 	= frame.states.get('gallery-edit').get('library'),
				ids 		= library.pluck('id');
				shortcode 	= wp.media.gallery.shortcode( library ).string().replace(/\"/g,"'");

			input.val(shortcode);

			var data = {
				type: 'update_gallery',
				action: 'x_data_post',
				nonce: $('#x-nonce').val(),
				ids: ids
			};
			$.post( ajaxurl, data, function( response ) {
				parent.find('.x-gallery-list').remove();
				input.after(response);
			});
		});

		// Finally, open the modal.
		frame.open();
	});
	$(document).on('click', '.x-gallery-delete', function() {

		$(this).addClass('active');

		var confirmation = confirm('Are you sure you want to delete this Gallery?');

		if ( confirmation ) {
			var parent = $(this).closest('.x-control')
			parent.find('.x-gallery-shortcode').val('');
			parent.find('.x-gallery-list').remove();
		}
		
		$(this).removeClass('active');
	});

});