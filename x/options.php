<?php if ( !defined('X_DIR') ) exit;

function x_options() {

	/* Set the Options Array */
	global $x_options;
	$x_options = array();

	/********** Home Settings **********/
	$x_options[] = array(
		'name' 			=> 'Home Settings',
		'type' 			=> 'heading',
		'icon' 			=> 'dashicons-admin-home',
		'desc' 			=> 'Control and configure the general setup of your theme. Upload your preferred logo, setup your feeds and insert your analytics tracking code.',
	);
	$x_options[] = array(
		'type' 			=> 'info',
		'info' 			=> '<h3 style="margin:0 0 10px;line-height:1;">Welcome to the Options Framework demo.</h3>
		This is a slightly modified version of the original options framework by Devin Price with a couple of aesthetical improvements on the interface and some cool additional features. If you want to learn how to setup these options or just need general help on using it feel free to visit my blog at <a href="google.com">Google.com</a>'
	);
	$x_options[] = array(
		'id' 			=> 'test_text',
		'name' 			=> 'Input Text',
		'desc' 			=> 'A text input field.',
		'type' 			=> 'text',
		'default' 		=> 'Default Value',
		'atts' 			=> array( 'placeholder' => 'Text placeholder' ),
		'desc2' 		=> 'Control and configure the general setup of your theme. Upload your preferred logo, setup your feeds and insert your analytics tracking code.'
	);
	$x_options[] = array(
		'id' 			=> 'test_textarea',
		'name' 			=> 'Textarea',
		'desc' 			=> 'A textarea field.',
		'type' 			=> 'textarea',
		'default' 		=> 'Default Value'
	);
	$x_options[] = array(
		'id' 			=> 'test_select',
		'name' 			=> 'Select Options',
		'desc' 			=> 'A Select Options element',
		'type' 			=> 'select',
		'default' 		=> 'three',
		'choices' 		=> array(
			'default' 	=> 'Default',
			'one' 		=> 'One',
			'two' 		=> 'Two',
			'three' 	=> 'Three',
			'four' 		=> 'Four',
			'five' 		=> 'Five'
		)
	);
	$x_options[] = array(
		'id' 			=> 'test_select_editable',
		'name' 			=> 'Editable Select',
		'desc' 			=> 'A Editable Select Options element.',
		'type' 			=> 'select-editable',
		'placeholder' 	=> 'Editable Select',
		// 'default' 		=> 'three',
		'choices' 		=> array(
			'default' 	=> 'Default',
			'one' 		=> 'One',
			'two' 		=> 'Two',
			'three' 	=> 'Three',
			'four' 		=> 'Four',
			'five' 		=> 'Five'
		)
	);
	$x_options[] = array(
		'id' 			=> 'test_radio',
		'name' 			=> 'Radio Buttons',
		'desc' 			=> 'Radio buttons with default of "two".',
		'type' 			=> 'radio',
		'default' 		=> 'two',
		// 'class' 		=> 'col-2',
		'choices' 		=> array(
			'one' 		=> 'One',
			'two' 		=> 'Two',
			'three' 	=> 'Three'
		)
	);
	$x_options[] = array(
		'id' 			=> 'test_checkbox',
		'name' 			=> 'Checkbox',
		'desc' 			=> 'A checkbox field.',
		'type' 			=> 'checkbox',
		'default' 		=> '1',
		'label' 		=> 'Checkbox Label'
	);
	$x_options[] = array(
		'id' 			=> 'test_multicheck',
		'name' 			=> 'Multicheck',
		'desc' 			=> 'Multicheck description.',
		'type' 			=> 'multi-check',
		'default' 		=> array('two','three'),
		'choices' 		=> array(
			'one' 		=> 'One',
			'two' 		=> 'Two',
			'three' 	=> 'Three'
		)
	);
	$x_options[] = array(
		'id' 			=> 'test_switch',
		'name' 			=> 'Switch',
		'type' 			=> 'switch',
		'label' 		=> 'A On/Off Switch',
		'desc2' 		=> 'Control and configure the general setup of your theme. Upload your preferred logo, setup your feeds and insert your analytics tracking code.'
	);
	$x_options[] = array(
		'id' 			=> 'test_multiswitch',
		'name' 			=> 'Multi Switch',
		'type' 			=> 'multi-switch',
		'default' 		=> array('one'),
		'choices' 		=> array(
			'one' 		=> 'One',
			'two' 		=> 'Two',
			'three' 	=> 'Three'
		)
	);
	$x_options[] = array(
		'id' 			=> 'test_measurement',
		'name' 			=> 'Measurement',
		'desc' 			=> 'A text input field.',
		'type' 			=> 'measurement',
		'units' 		=> array( '', 'px', '%', 'em', 'rem', 'pt', 'pc', 'ex', 'mm', 'cm', 'in' ),
		'default' 		=> array( 'value' => '12', 'unit' => 'px' )
	);
	$x_options[] = array(
		'id' 			=> 'test_radio_image',
		'name' 			=> 'Radio Image',
		'desc' 			=> 'Radio Image with default of "2cr".',
		'type' 			=> 'image',
		'default' 		=> '2cr',
		'choices' 		=> array(
			'1col' 		=> X_IMG .'/1col.png',
			'2cl' 		=> X_IMG .'/2cl.png',
			'2cr' 		=> X_IMG .'/2cr.png',
			'3cm' 		=> X_IMG .'/3cm.png',
			'3cl' 		=> X_IMG .'/3cl.png',
			'3cr' 		=> X_IMG .'/3cr.png'
		)
	);
	$x_options[] = array(
		'id' 			=> 'test_multicheck_image',
		'name' 			=> 'Multicheck Image',
		'desc' 			=> 'Multicheck Image description.',
		'type' 			=> 'multi-image',
		'inline' 		=> true,
		'default' 		=> array('1col','3cr3cr'),
		'choices' 		=> array(
			'1col' 		=> X_IMG .'/1col.png',
			'2cl' 		=> X_IMG .'/2cl.png',
			'2cr' 		=> X_IMG .'/2cr.png',
			'3cm' 		=> X_IMG .'/3cm.png',
			'3cl' 		=> X_IMG .'/3cl.png',
			'3cr' 		=> X_IMG .'/3cr.png'
		)
	);
	$x_options[] = array(
		'id' 			=> 'test_color',
		'name' 			=> 'Color',
		'desc' 			=> 'Pick a color (default: #fff)',
		'type' 			=> 'color',
		'default' 		=> '#8AC007'
	);
	$x_options[] = array(
		'id' 			=> 'test_slider',
		'name' 			=> 'JQuery UI Slider with 5 steps',
		'desc' 			=> 'JQuery UI slider description.<br/>Min: 0, Max: 300, Step: 5, Default: 75',
		'type'			=> 'range',
		'default' 		=> '75',
		// 'min' 			=> '0',
		// 'step' 			=> '5',
		// 'max'			=> '999'
	);
	$x_options[] = array(
		'id' 			=> 'test_sorter',
		'name' 			=> 'Homepage Layout Manager',
		'desc' 			=> 'Organize how you want the layout to appear on the homepage',
		'type' 			=> 'sorter',
		// 'groups' 		=> array(
		// 	'inactive' 	=> 'Inactive',
		// 	'disabled' 	=> 'Disabled',
		// 	'enabled' 	=> 'Enabled'
		// ),
		// 'items' 		=> array(
		// 	'block_100' 	=> '100',
		// 	'block_200' 	=> '200',
		// 	'block_300' 	=> '300',
		// 	'block_400' 	=> '400'
		// ),
		'default' 		=> array(
			'enabled' => array(
				'block_3' 	=> 'Block Three',
				'block_4' 	=> 'Block Four',
				'block_5' 	=> 'Block Five'
			),
			'disabled' => array(
				'block_1' 	=> 'Block One',
				'block_2' 	=> 'Block Two'
			)
		)
	);
	$x_options[] = array(
		'id' 			=> 'test_css',
		'name' 			=> 'CSS',
		'desc' 			=> 'A CSS editor.',
		'type' 			=> 'code',
		'mode' 			=> 'css', // css / html / javascript
		'theme' 		=> 'chrome',
		// ambiance / chaos / chrome / clouds / clouds_midnight / cobalt / crimson_editor / dawn / dreamweaver / eclipse / github / idle_fingers / katzenmilch / kr_theme / kuroir / merbivore / merbivore_soft / mono_industrial / monokai / pastel_on_dark / solarized_dark / solarized_light / terminal / textmate / tomorrow / tomorrow_night / tomorrow_night_blue / tomorrow_night_bright / tomorrow_night_eighties / twilight / vibrant_ink / xcode
		'font_size' 	=> '16px',
		// 'default' 		=> file_get_contents('http://localhost/wordpress/blogpost/wp-content/themes/blogpost/x/css/style.css')
	);
	$x_options[] = array(
		'id' 			=> 'test_typography',
		'name' 			=> 'Typography',
		'desc' 			=> 'This is a typographic specific option.',
		'type' 			=> 'typography',
		'preview' 		=> array( 'text' => 'Grumpy wizards make toxic brew for the evil Queen and Jack.' ),
		'default' 		=> array( 'family' => 'PT Sans', 'variant' => '400', 'size' => '14px', 'height' => '1.75' ),
		// 'sub_control' 	=> 'test_switch',
		// 'fold' 			=> 'switch',
	);
	$x_options[] = array(
		'id' 			=> 'test_slides',
		'name' 			=> 'Slides Options',
		'desc' 			=> 'Unlimited slides with drag and drop sortings.',
		'default' 		=> '',
		'type' 			=> 'slides',
		// 'default' 		=> array(
		// 	'0' => array(
		// 		'title' => '',
		// 		'media' => array(
		// 			'url' => 'http://localhost/wordpress/blogpost/wp-content/uploads/sites/3/2014/02/5569874075_5f02473218_o-660x330.jpg',
		// 			'type' => 'image'
		// 		),
		// 		'link' => '',
		// 		'description' => ''
		// 	)
		// )
	);
	$x_options[] = array(
		'id' 			=> 'test_link',
		'name' 			=> 'Link',
		'desc' 			=> 'A link input field.',
		'type' 			=> 'link'
	);


	/********** Home Settings **********/
	$x_options[] = array(
		'name' 			=> 'Wordpress',
		'heading' 		=> 'Wordpress related controls',
		'type' 			=> 'heading',
		'icon' 			=> 'dashicons-wordpress'
	);
	$x_options[] = array(
		'id' 			=> 'test_upload',
		'name' 			=> 'Media Uploader',
		'desc' 			=> 'Upload images using native media uploader from Wordpress 3.5+.',
		'type' 			=> 'media'
	);
	$x_options[] = array(
		'id' 			=> 'test_backgroud',
		'name' 			=> 'Background',
		'desc' 			=> 'Set custom background.',
		'type' 			=> 'background',
		'placeholder' 	=> 'Background URL',
	);
	$x_options[] = array(
		'id' 			=> 'test_gallery',
		'name' 			=> 'Gallery',
		'desc' 			=> 'The Gallery option type saves list of image attachment IDs.',
		'type' 			=> 'gallery'
	);
	$x_options[] = array(
		'id' 			=> 'test_editor',
		'name' 			=> 'Editor',
		'desc' 			=> 'A editor field.',
		'type' 			=> 'editor',
		'default' 		=> 'Default Value'
	);
	$x_options[] = array(
		'id' 			=> 'test_taxonomy',
		'name' 			=> 'Taxonomy Select',
		'desc' 			=> 'The Taxonomy Select option type displays a list of taxonomy IDs.',
		'type' 			=> 'category',
		'taxonomy' 		=> array( 'category', 'post_tag' )
	);
	$x_options[] = array(
		'id' 			=> 'test_taxonomies',
		'name' 			=> 'Taxonomies Select',
		'desc' 			=> 'Select and order multiple taxonomies.',
		'type' 			=> 'taxonomies',
		'taxonomy' 		=> array( 'category', 'post_tag' ),
		'icon' 			=> array( 'category' => 'dashicons-category', 'post_tag' => 'dashicons-tag' ),
	);
	$x_options[] = array(
		'id' 			=> 'test_post',
		'name' 			=> 'Post Select',
		'desc' 			=> 'The Post Select option type displays a list of post IDs.',
		'type' 			=> 'post',
		// 'default' 		=> '3'
	);
	$x_options[] = array(
		'id' 			=> 'test_posts',
		'name' 			=> 'Posts Select',
		'desc' 			=> 'Select and order multiple posts.',
		'type' 			=> 'posts'
	);


	/********** Backup Options **********/
	$x_options[] = array( 
		'name' 			=> __( 'Backup Options', THEME_TEXTDOMAIN ),
		'heading' 		=> __( 'Backup and Restore Options', THEME_TEXTDOMAIN ),
		'type' 			=> 'heading',
		'icon' 			=> 'dashicons-backup',
		'desc' 			=> __( 'You can use the two buttons below to backup your current options, and then restore it back at a later time. This is useful if you want to experiment on the options but would like to keep the old settings in case you need it back.', THEME_TEXTDOMAIN )
	);
	$x_options[] = array( 
		'id' 			=> 'x_backup',
		'type' 			=> 'backup'
	);
	$x_options[] = array(
		'name' 			=> __( 'Transfer Theme Options Data', THEME_TEXTDOMAIN ),
		'id' 			=> 'x_transfer_data',
		'type' 			=> 'transfer',
		'desc' 			=> __( 'You can tranfer the saved options data between different installs by copying the text inside the text box. To import data from another install, replace the data in the text box with the one from another install and click "Import Options".', THEME_TEXTDOMAIN ),
	);
}


?>