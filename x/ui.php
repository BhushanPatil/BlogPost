<?php if ( !defined('X_DIR') ) exit; ?>

<?php $layout = isset($_COOKIE['x-layout']) ? $_COOKIE['x-layout'] : 'x-layout-default'; ?>

<div id="x-container" class="<?php echo $layout; ?>">

	<h2 id="x-js-warning">
		<img src="<?php echo X_IMG; ?>/save-error.png" alt="Error">&nbsp;&nbsp;<b>JavaScript is not enabled.</b> It is necessary to have Javascript enabled in order to use options panel. <a href="http://www.enable-javascript.com" target="_blank">Here</a> are the instructions how to enable JavaScript in your web browser.
	</h2>
	<script type="text/javascript">document.getElementById('x-js-warning').style.display = 'none';</script>

	<!-- <div id="x-popup-save" class="x-popup"><div class="x-popup-message">Nice work, your settings have been saved successfully.</div></div> -->

	<div id="x-popup-save" class="x-popup"><div class="x-popup-message">Saved</div></div>
	<div id="x-popup-reset" class="x-popup"><div class="x-popup-message">Reset</div></div>
	<div id="x-popup-error" class="x-popup"><div class="x-popup-message">ERROR: Unable to save settings</div></div>
	<div id="x-popup-backup" class="x-popup"><div class="x-popup-message">Backup</div></div>

	<input id="x-nonce" type="hidden" value="<?php echo wp_create_nonce('x_ajax_nonce'); ?>" />

	<form id="x-form" method="post" action="<?php echo esc_attr( $_SERVER['REQUEST_URI'] ) ?>" enctype="multipart/form-data">

		<div id="x-header">
			<h1 id="x-theme-name"><?php echo THEME_NAME; ?></h1>
			<span id="x-theme-version"><?php echo 'v'. THEME_VERSION; ?></span>
			<img id="x-icon" src="<?php echo X_IMG; ?>/icon-option.png" alt="X Framework" />
    	</div><!--#x-header-->

    	<div id="x-top-bar">
    		<div id="x-top-links">
    			<a id="x-layout-toggle" href="#"><sapn class="dashicons dashicons-menu"></sapn></a>
    			<a href="#">Documentation</a>
    			<a href="#">Support</a>
    		</div><!--#x-top-links-->
    		<div id="x-top-buttons">
    			<img src="<?php echo X_IMG; ?>/loading-top.gif" class="x-ajax-loading-img x-ajax-save-img" alt="Working..." />
    			<button id="x-save-button-top" class="button-primary x-save-button" type="button"><?php _e('Save Changes'); ?></button>
			</div><!--#x-top-buttons-->
		</div><!--#x-top-bar-->

		<div id="x-nav">
			<?php x_ui_nav(); ?>
		</div><!--#x-nav-->

		<div id="x-main-content">
			<?php x_ui_options(); ?>
	  	</div><!--#x-main-content-->

		<div id="x-save-bar">
			<button id ="x-reset-button" class="button x-reset-button" type="button"><span class="dashicons dashicons-update"></span> <?php _e('Reset All'); ?></button>
			<img src="<?php echo X_IMG; ?>/loading-bottom.gif" class="x-ajax-loading-img x-ajax-reset-img" alt="Working..." />
			<button id ="x-save-button-bottom" class="button-primary x-save-button" type="button"><?php _e('Save Changes'); ?></button>
			<img src="<?php echo X_IMG; ?>/loading-bottom.gif" class="x-ajax-loading-img x-ajax-save-img" alt="Working..." />
		</div><!--#x-save-bar-->

	</form><!--#x-form-->

</div><!--#x-container-->