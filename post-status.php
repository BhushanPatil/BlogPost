<?php
/**
 * The template for displaying posts in the Status post format
 */
?>
<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

	<?php BP_entry_thumbnail(); ?>

	<?php BP_entry_content( array(
		'excerpt' => TRUE,
		'before' => '<div class="entry-content"><p>',
		'after' => '</p></div><!--END .entry-content -->'
	) ); ?>
	
	<footer class="entry-footer">
		<?php BP_entry_meta(); ?>
	</footer><!--END .entry-footer -->

</article><!--END #post-<?php the_ID(); ?> -->