<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages and that other
 * 'pages' on your WordPress site will use a different template.
 */
?>

<?php get_header(); ?>

	<div class="grid-container">
    
        <?php $sidebar_position = BP_get_sidebar_position(); ?>
		<?php if ( $sidebar_position == 'left-sidebar' ) get_sidebar(); ?>
		
        <?php if ( $sidebar_position == 'no-sidebar' ) : ?><div id="primary-content" class="col-12">
		<?php else : ?><div id="primary-content" class="col-8"><?php endif; ?>

			<?php get_template_part( 'loop', 'page' ); ?>
			
		</div><!--END #primary-content -->

		<?php if ( $sidebar_position == 'right-sidebar' ) get_sidebar(); ?>
		
	</div><!--END .grid-container -->

<?php get_footer(); ?>