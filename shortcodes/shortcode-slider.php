<?php


/**
 * Register shortcode
 */
add_shortcode('slider', 'BP_slider_shortcode');


/**
 * The Slider shortcode.
 *
 * This implements the functionality of the Slider Shortcode for displaying
 * WordPress images on a post.
 *
 * @param array $attr Attributes of the shortcode.
 * @return string HTML content to display slider.
 */
function BP_slider_shortcode( $attr ) {
	$post = get_post();

	static $instance = 0;
	$instance++;

	if ( !empty( $attr['ids']) ) {
		// 'ids' is explicitly ordered, unless you specify otherwise.
		if ( empty($attr['orderby']) ) $attr['orderby'] = 'post__in';
		$attr['include'] = $attr['ids'];
	}

	// Allow plugins/themes to override the default slider template.
	$output = apply_filters( 'post_slider', '', $attr );
	if ( $output != '' ) return $output;

	// We're trusting author input, so let's at least make sure it looks like a valid orderby statement
	if ( isset($attr['orderby']) ) {
		$attr['orderby'] = sanitize_sql_orderby( $attr['orderby'] );
		if ( !$attr['orderby'] ) unset( $attr['orderby'] );
	}

	extract( shortcode_atts( array(
		'order' 			=> 'ASC',
		'orderby' 			=> 'menu_order ID',
		'id' 				=> $post ? $post->ID : 0,
		'size' 				=> 'full',
		'include' 			=> '',
		'exclude' 			=> '',
		'caption' 			=> 'true',
		'carousel' 			=> 'true',
		'height' 			=> NULL,

		// FlexSlider Options
		'animation' 		=> 'slide',
		'direction' 		=> 'horizontal',
		'animationLoop' 	=> 'false',
		'slideshow' 		=> 'false',
		'pauseOnAction' 	=> 'true',
		'pauseOnHover' 		=> 'false',
		'controlNav' 		=> 'false',
		'directionNav' 		=> 'true',
		'prevText' 			=> '<i class="fa fa-angle-left"></i>',
		'nextText' 			=> '<i class="fa fa-angle-right"></i>',
		'pausePlay' 		=> 'false',
		'pauseText' 		=> '<i class="fa fa-pause"></i>',
		'playText' 			=> '<i class="fa fa-play"></i>'
	), $attr, 'slider' ) );

	$prevText = str_replace("'", "\"", $prevText);
	$prevText = str_replace("\"", "\\\"", $prevText);
	$nextText = str_replace("'", "\"", $nextText);
	$nextText = str_replace("\"", "\\\"", $nextText);
	$pauseText = str_replace("'", "\"", $pauseText);
	$pauseText = str_replace("\"", "\\\"", $pauseText);
	$playText = str_replace("'", "\"", $playText);
	$playText = str_replace("\"", "\\\"", $playText);

	$id = intval($id);
	if ( 'RAND' == $order ) $orderby = 'none';

	if ( !empty( $include) ) {
		$_attachments = get_posts( array('include' => $include, 'post_status' => 'inherit', 'post_type' => 'attachment', 'post_mime_type' => 'image', 'order' => $order, 'orderby' => $orderby) );
		$attachments = array();
		foreach ( $_attachments as $key => $val ) $attachments[$val->ID] = $_attachments[$key];
	}
	elseif ( !empty( $exclude) )
		$attachments = get_children( array('post_parent' => $id, 'exclude' => $exclude, 'post_status' => 'inherit', 'post_type' => 'attachment', 'post_mime_type' => 'image', 'order' => $order, 'orderby' => $orderby) );
	else
		$attachments = get_children( array('post_parent' => $id, 'post_status' => 'inherit', 'post_type' => 'attachment', 'post_mime_type' => 'image', 'order' => $order, 'orderby' => $orderby) );

	if ( empty($attachments) ) return '';

	if ( is_feed() ) {
		$output = "\n";
		foreach ( $attachments as $att_id => $attachment )
			$output .= wp_get_attachment_link($att_id, $size, true) . "\n";
		return $output;
	}

	// wp_enqueue_script('flexslider');

	$selector_slider = 'slider-'. $instance;
	$selector_carousel = $selector_slider .'-carousel';
	
	$classes = 'slider';
	if ( $animationLoop === 'true' ) $classes .= ' with-animationloop';

	if ( $height === NULL ) $height_style = '';
	else $height_style = ' style="height: '. str2cssunit( $height ) .' !important"';
	
	$output .= '<div id="'. $selector_slider .'" class="'. $classes .'">';
	$output .= '<ul class="slides">';
	foreach( $attachments as $id => $attachment ) {
		$output .= '<li'. $height_style .'>';
		$output .= '<figure class="image">';
		$output .= wp_get_attachment_image( $id, $size, false );
		if ( $caption === 'true' && trim( $attachment->post_excerpt ) ) $output .= '<p class="image-caption">'. wptexturize( $attachment->post_excerpt ) .'</p>';
		$output .= '</figure>';
		$output .= '</li>';
	}
	$output .= "</ul>";
	$output .= "</div>";


	if ( $carousel === 'true' ) {
		$output .= '<div id="'. $selector_carousel .'" class="slider-carousel">';
		$output .= '<ul class="slides">';
		foreach( $attachments as $id => $attachment ) $output .= '<li>'. wp_get_attachment_image( $id, array($itemWidth,$itemWidth) ) .'</li>';
		$output .= "</ul>";
		$output .= "</div>";
		$carousel_script = '$("#'. $selector_carousel .':not(.loaded)").flexslider({
			namespace: "",
			selector: ".slides > li",
			animation: "slide",
			animationLoop: false,
			slideshow: false,
			controlNav: false,
			directionNav: false,
			prevText: "<i class=\"fa fa-angle-left\"></i>",
			nextText: "<i class=\"fa fa-angle-right\"></i>",
			asNavFor: "#'. $selector_slider .'",
			itemWidth: 35,
			itemMargin: 0,
			start: function(slider) { slider.addClass("loaded"); }
		});';
		$sync = 'sync: "#'. $selector_carousel .'",';
	}
	else {
		$carousel_script = '';
		$sync = '';
	}


	if ( $animationLoop === 'true' ) $start = '$(\'#'. $selector_slider .' .slides > li\').css("cursor","pointer").click(function(){ slider.flexAnimate(slider.getTarget("next")); });';
	else $start = '$(\'#'. $selector_slider .' .slides > li:not(li:last-child)\').css("cursor","pointer").click(function(){ slider.flexAnimate(slider.getTarget("next")); });';
	$script = '<script type="text/javascript">
	jQuery(document).ready(function($){
		'. $carousel_script .'
		$("#'. $selector_slider .':not(.loaded)").flexslider({
			namespace: "",
			selector: ".slides > li",
			animation: "'. $animation .'",
			direction: "'. $direction .'",
			animationLoop: '. $animationLoop .',
			smoothHeight: true,
			slideshow: '. $slideshow .',
			pauseOnAction: '. $pauseOnAction .',
			pauseOnHover: '. $pauseOnHover .',
			controlNav: '. $controlNav .',
			directionNav: '. $directionNav .',
			prevText: "'. $prevText .'",
			nextText: "'. $nextText .'",
			pausePlay: '. $pausePlay .',
			pauseText: "'. $pauseText .'",
			playText: "'. $playText .'",
			'. $sync .'
			start: function(slider) { slider.addClass("loaded"); '. $start .' }
		});
	});</script>';

	$output .= preg_replace( '/\s\s+/', ' ', $script );

	return $output;
}



?>