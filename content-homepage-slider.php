<?php
/**
 * Post slider for our theme homepage.
 */

$loop = new WP_Query( array(
	'post_type' =>'post',
	'meta_query' => array( array('key' => '_thumbnail_id') ),
	'tax_query' => array(
	    array(
	        'taxonomy' => 'category',
	        'field' => 'slug',
	        'terms' => array ( 'slider' )
	    )
	)
) );

if ( $loop->have_posts() ) {
	// wp_enqueue_script('flexslider');
	?>
	<header id="content-header">
		<div class="grid-container">
			<div class="featured-posts col-12">
				<div class="flexslider loading">
					<ul class="slides">
						<?php while ( $loop->have_posts() ) : $loop->the_post(); ?>
						<li id="slide-post-<?php the_ID(); ?>" class="slide-post">
							<figure class="slide-post-image">
								<a href="<?php the_permalink() ?>" title="<?php the_title_attribute(); ?>"><?php the_post_thumbnail( 'slider-thumb' ); ?></a>
							</figure>
							<article class="slide-post-info">
								<header class="slide-post-title">
									<h1><a href="<?php the_permalink() ?>" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a></h1>
								</header>
								<p class="slide-post-content"><?php BP_excerpt_max_char(); ?></p>
								<footer class="slide-post-meta">
									<?php BP_entry_meta( array('meta'=>'%datelink%') ); ?>
								</footer>
							</article>
						</li><!--END #slide-post-<?php the_ID(); ?> -->
						<?php endwhile; ?>
					</ul><!--END .slides -->
				</div><!--END .flexslider -->
			</div><!--END .featured-posts -->
		</div><!--END .grid-container -->
	</header><!--END #content-header -->
	<script type="text/javascript">
	$(window).load(function(){
		// GALLERY: Set felxslider galleries
		$('.flexslider').flexslider({
			animation: "fade",
			smoothHeight: true,
			slideshow: false,
			controlNav: false,
			prevText: "<i class=\"fa fa-angle-left\"></i>",
			nextText: "<i class=\"fa fa-angle-right\"></i>",
			start: function(slider) {
				slider.addClass("slide").removeClass('loading');
				$('.flexslider .slides li > img').click(function(){
					slider.flexAnimate(slider.getTarget("next"));
				});
			},
		});
	});
	</script>
	<?php
	
	/* Reset the global $the_post as this query will have stomped on it */
	wp_reset_postdata();
}
?>