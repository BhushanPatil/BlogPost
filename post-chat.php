<?php
/**
 * The template for displaying posts in the Chat post format
 */
?>
<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

	<?php BP_entry_thumbnail(); ?>

	<?php BP_entry_content(); ?>

	<footer class="entry-footer">
		<?php BP_entry_meta(); ?>
	</footer><!--END .entry-footer -->

</article><!--END #post-<?php the_ID(); ?> -->