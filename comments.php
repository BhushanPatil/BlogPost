<?php
/**
 * The template for displaying Comments.
 * The area of the page that contains comments and the comment form.
 *
 * If the current post is protected by a password and the visitor has not yet
 * entered the password we will return early without loading the comments.
 */
?>

<?php if ( post_password_required() ) return; ?>

<section id="comments" class="comments-area">

	<?php if ( have_comments() ) : ?>
		<div class="comments-title">
			<h3><?php printf( _nx( 'One Comment on %2$s', '%1$s Comments on %2$s', get_comments_number(), 'comments title', THEME_TEXTDOMAIN ),
						number_format_i18n( get_comments_number() ), '<span>' . get_the_title() . '</span>' ); ?></h3>
		</div><!--END .comments-title -->

		<ol class="comment-list">
			<?php
				wp_list_comments( array(
					'style' 		=> 'ol',
					'short_ping'	=> true,
					'avatar_size' 	=> 74,
					'format' 		=> 'html5',
				) );
			?>
		</ol><!--END .comment-list -->

		<?php // Are there comments to navigate through? ?>
		<?php if ( get_comment_pages_count() > 1 && get_option( 'page_comments' ) ) : ?>
		<nav class="navigation comment-navigation" role="navigation">
			<div class="nav-prev"><?php previous_comments_link( __( '&larr; Older Comments', THEME_TEXTDOMAIN ) ); ?></div>
			<div class="nav-next"><?php next_comments_link( __( 'Newer Comments &rarr;', THEME_TEXTDOMAIN ) ); ?></div>
		</nav><!-- .comment-navigation -->
		<?php endif; // Check for comment navigation ?>

		<?php if ( ! comments_open() && get_comments_number() ) : ?>
		<p class="no-comments"><?php _e( 'Comments are closed.' , THEME_TEXTDOMAIN ); ?></p>
		<?php endif; ?>

	<?php endif; // have_comments() ?>

	<?php
	$fields = array(
	    'comment_field' => '<p class="comment-form-comment"><textarea id="comment" name="comment" cols="45" rows="8" aria-required="true"></textarea></p>',
	    'must_log_in' => '<p class="must-log-in">' .  sprintf( __( 'You must be <a href="%s">logged in</a> to post a comment.', THEME_TEXTDOMAIN ), wp_login_url( apply_filters( 'the_permalink', get_permalink( ) ) ) ) . '</p>',
	    'logged_in_as' => '<p class="logged-in-as">' . sprintf( __( 'Logged in as <a href="%1$s">%2$s</a>. <a href="%3$s" title="Log out of this account">Log out &raquo;</a>', THEME_TEXTDOMAIN ), admin_url( 'profile.php' ), $user_identity, wp_logout_url( apply_filters( 'the_permalink', get_permalink( ) ) ) ) . '</p>',
	    'comment_notes_before' => '',
	    'comment_notes_after' => '',
	    'title_reply' => __( 'Leave a Reply', THEME_TEXTDOMAIN ),
	    'title_reply_to' => __( 'Leave a Reply to %s', THEME_TEXTDOMAIN ),
	    'cancel_reply_link' => __( 'Cancel Reply', THEME_TEXTDOMAIN ),
	    'label_submit' => __( 'Post Comment', THEME_TEXTDOMAIN )
	);
	comment_form($fields);
	?>

</section><!--END .comments-area -->