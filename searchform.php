<?php
/**
 * Default searchform
 */
?>
<form class="search-form" method="get" action="<?php echo esc_url( home_url( '/' ) ); ?>" role="search">
	<input class="search-input" type="search" value="<?php echo get_search_query(); ?>" name="s" id="s" placeholder="Search <?php bloginfo( 'name' ); ?>">
	<button class="search-button gray" type="submit" id="searchsubmit"><i class="fa fa-search"></i></button>
</form><!--END .search-form -->