<div id="post-entries">

	<?php /* The loop */
	while ( have_posts() ) : the_post(); ?>

	<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

		<?php BP_entry_thumbnail(); ?>

		<header class="entry-header">
			<?php BP_entry_title(); ?>
			<?php BP_entry_meta( array('meta'=>'%edit%') ); ?>
		</header><!--END .entry-header -->

		<?php BP_entry_content(); ?>
		
	</article><!--END #post-<?php the_ID(); ?> -->

	<?php endwhile; ?>

</div><!--END #post-entries -->

<?php comments_template(); ?>