<?php
/**
 * The template for displaying posts in the Aside post format
 */
?>
<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

	<?php BP_entry_thumbnail(); ?>

	<header class="entry-header">
		<?php BP_entry_title(); ?>
	</header><!--END .entry-header -->

	<?php BP_entry_content( 'excerpt=TRUE' ); ?>

	<footer class="entry-footer">
		<?php BP_entry_meta(); ?>
	</footer><!--END .entry-footer -->

</article><!--END #post-<?php the_ID(); ?> -->