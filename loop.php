<div id="post-entries">

	<?php
	/* The loop */
	if ( have_posts() ) :
		while ( have_posts() ) :
			the_post();
			get_template_part( 'post', get_post_format() );
		endwhile;
		if ( is_single() ) :
			get_template_part( 'author-bio' );
			get_template_part( 'related-posts' );
			comments_template();
		endif;
	else :
		get_template_part( 'post', 'none' );
	endif;
	?>

</div><!--END #post-entries -->

<?php BP_paging_nav(); ?>