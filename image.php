<?php
/**
 * The template for displaying image attachments.
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 */
?>

<?php get_header(); ?>

		<div class="grid-container">

			<div class="primary-content col-8">

				<article id="post-<?php the_ID(); ?>" <?php post_class( 'image-attachment' ); ?>>

					<div class="entry-attachment"><?php BP_the_attached_image(); ?></div>

					<header class="entry-header">
						<div class="entry-title"><h1><?php the_title(); ?></h1></div>
						<div class="entry-meta">
							<?php
								$published_text = __( '<span class="attachment-meta"><time class="entry-date" datetime="%1$s">%2$s</time></span><span class="post-title"><a href="%3$s" title="Return to %4$s" rel="gallery">%5$s</a></span>', THEME_TEXTDOMAIN );
								$post_title = get_the_title( $post->post_parent );
								if ( empty( $post_title ) || 0 == $post->post_parent )
									$published_text = '<span class="attachment-meta"><time class="entry-date" datetime="%1$s">%2$s</time></span>';
								printf( $published_text,
									esc_attr( get_the_date( 'c' ) ),
									esc_html( get_the_date() ),
									esc_url( get_permalink( $post->post_parent ) ),
									esc_attr( strip_tags( $post_title ) ),
									$post_title
								);
								$metadata = wp_get_attachment_metadata();
								printf( '<span class="attachment-meta full-size-link"><a href="%1$s" title="%2$s">%3$s (%4$s &times; %5$s)</a></span>',
									esc_url( wp_get_attachment_url() ),
									esc_attr__( 'Link to full-size image', THEME_TEXTDOMAIN ),
									__( 'Full resolution', THEME_TEXTDOMAIN ),
									$metadata['width'],
									$metadata['height']
								);
								edit_post_link( __( 'Edit', THEME_TEXTDOMAIN ), '<span class="edit-link">', '</span>' );
							?>
						</div>
					</header><!--END .entry-header -->

					<?php if ( has_excerpt() ) : ?>
					<div class="entry-content"><?php the_excerpt(); ?></div>
					<?php endif; ?>

				</article><!--END .hentry -->

				<nav class="navigation image-navigation" role="navigation">
					<div class="nav-prev"><?php previous_image_link( false, __( '<i class="fa fa-angle-left"></i> Previous', THEME_TEXTDOMAIN ) ); ?></div>
					<div class="nav-next"><?php next_image_link( false, __( 'Next <i class="fa fa-angle-right"></i>', THEME_TEXTDOMAIN ) ); ?></div>
				</nav><!--END .navigation -->
				
				<?php comments_template(); ?>

			</div><!--END .primary-content -->

			<?php get_sidebar(); ?>

		</div><!--END .grid-container -->

		<?php BP_post_nav(); ?>

<?php get_footer(); ?>