<?php
/**
 * The template for displaying posts in the Gallery post format
 */
?>
<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

	<?php $the_content = get_the_content();
	$shortcodes = array( 'gallery', 'slider' ); // Schorcode to search in post content.
	$gallery = BP_get_shortcode( $the_content, $shortcodes );
	if ( !post_password_required() && !empty( $gallery ) ) : ?>
	<div class="entry-gallery"><?php
		$slider = str_replace('[gallery', '[slider', $gallery);
		if ( strpos( $slider, 'carousel' ) === false ) $slider = str_replace('[slider', '[slider carousel="false"', $slider);
		if ( strpos( $slider, 'title' ) === false ) $slider = str_replace('[slider', '[slider title="false"', $slider);
		if ( strpos( $slider, 'description' ) === false ) $slider = str_replace('[slider', '[slider description="false"', $slider);
		if ( strpos( $slider, 'size' ) === false ) $slider = str_replace('[slider', '[slider size="full"', $slider);
		if ( strpos( $slider, 'position' ) === false ) $slider = str_replace('[slider', '[slider position="stretch"', $slider);
		echo do_shortcode( $slider );
	?></div><!--END .entry-gallery -->
	<?php endif; ?>
	
	<header class="entry-header">
		<?php BP_entry_title(); ?>
		<?php BP_entry_meta(); ?>
	</header><!--END .entry-header -->

	<?php BP_entry_content( array( 'content' => BP_content_strip_shortcode( $the_content, $shortcodes ) ) ); ?>

	<?php if ( is_single() ) : ?>
	<footer class="entry-footer">
		<?php BP_entry_tags(); ?>
		<?php BP_entry_social_media_share(); ?>
	</footer><!--END .entry-footer -->
	<?php endif; ?>

</article><!--END #post-<?php the_ID(); ?> -->