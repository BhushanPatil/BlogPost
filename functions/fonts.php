<?php

function BP_fonts() {
	$fonts = array();

	$fonts['Arial'] = array(
		'styles' 		=> 'normal,bold,italic,bolditalic',
		'fallback' 		=> 'Helvetica',
		'type'			=> 'sans-serif'
	);
	$fonts['Arial Black'] = array(
		'styles' 		=> 'normal,italic',
		'fallback' 		=> 'Gadget',
		'type'			=> 'sans-serif'
	);
	$fonts['Comic Sans MS'] = array(
		'styles' 		=> 'normal,bold,italic,bolditalic',
		'fallback' 		=> 'cursive',
		'type'			=> 'sans-serif'
	);
	$fonts['Impact'] = array(
		'styles' 		=> 'normal,bold,italic,bolditalic',
		'fallback' 		=> 'Charcoal',
		'type'			=> 'sans-serif'
	);
	$fonts['Lucida Sans Unicode'] = array(
		'styles' 		=> 'normal,bold,italic,bolditalic',
		'fallback' 		=> 'Lucida Grande',
		'type'			=> 'sans-serif'
	);
	$fonts['Tahoma'] = array(
		'styles' 		=> 'normal,bold,italic,bolditalic',
		'fallback' 		=> 'Geneva',
		'type'			=> 'sans-serif'
	);
	$fonts['Trebuchet MS'] = array(
		'styles' 		=> 'normal,bold,italic,bolditalic',
		'fallback' 		=> 'Helvetica',
		'type'			=> 'sans-serif'
	);
	$fonts['Verdana'] = array(
		'styles' 		=> 'normal,bold,italic,bolditalic',
		'fallback' 		=> 'Geneva',
		'type'			=> 'sans-serif'
	);


	$fonts['Georgia'] = array(
		'styles' 		=> 'normal,bold,italic,bolditalic',
		'type'			=> 'serif'
	);
	$fonts['Palatino Linotype'] = array(
		'styles' 		=> 'normal,bold,italic,bolditalic',
		'fallback' 		=> 'Book Antiqua,Palatino',
		'type'			=> 'serif'
	);
	$fonts['Times New Roman'] = array(
		'styles' 		=> 'normal,bold,italic,bolditalic',
		'fallback' 		=> 'Times',
		'type'			=> 'serif'
	);


	$fonts['Courier New'] = array(
		'styles' 		=> 'normal,bold,italic,bolditalic',
		'fallback' 		=> 'Courier',
		'type'			=> 'monospace'
	);
	$fonts['Lucida Console'] = array(
		'styles' 		=> 'normal,bold,italic,bolditalic',
		'fallback' 		=> 'Monaco',
		'type'			=> 'monospace'
	);


	$fonts['Open Sans'] = array(
		'styles' 		=> '400,400italic,300,300italic,600,600italic,700,700italic,800,800italic',
		'character_set' => 'latin,cyrillic-ext,greek-ext,greek,vietnamese,latin-ext,cyrillic',
		'type'			=> 'sans-serif'
	);
	$fonts['Oswald'] = array(
		'styles' 		=> '400,300,700',
		'character_set' => 'latin,latin-ext',
		'type'			=> 'sans-serif'
	);
	$fonts['Droid Sans'] = array(
		'styles' 		=> '400,700',
		'character_set' => 'latin',
		'type'			=> 'sans-serif'
	);
	$fonts['Lato'] = array(
		'styles' 		=> '400,400italic,100,100italic,300,300italic,700,700italic,900,900italic',
		'character_set' => 'latin',
		'type'			=> 'sans-serif'
	);
	$fonts['Open Sans Condensed'] = array(
		'styles' 		=> '300,300italic,700',
		'character_set' => 'latin,cyrillic-ext,latin-ext,greek-ext,greek,vietnamese,cyrillic',
		'type'			=> 'sans-serif'
	);
	$fonts['PT Sans'] = array(
		'styles' 		=> '400,400italic,700,700italic',
		'character_set' => 'latin,latin-ext,cyrillic',
		'type'			=> 'sans-serif'
	);
	$fonts['PT Sans Narrow'] = array(
		'styles' 		=> '400,700',
		'character_set' => 'latin,latin-ext,cyrillic',
		'type'			=> 'sans-serif'
	);
	$fonts['PT Serif'] = array(
		'styles' 		=> '400,400italic,700,700italic',
		'character_set' => 'latin,cyrillic',
		'type'			=> 'serif'
	);
	$fonts['Ubuntu'] = array(
		'styles' 		=> '400,400italic,300,300italic,500,500italic,700,700italic',
		'character_set' => 'latin,cyrillic-ext,cyrillic,greek-ext,greek,latin-ext',
		'type'			=> 'sans-serif'
	);
	$fonts['Yanone Kaffeesatz'] = array(
		'styles' 		=> '400,200,300,700',
		'character_set' => 'latin,latin-ext',
		'type'			=> 'sans-serif'
	);
	$fonts['Roboto Condensed'] = array(
		'styles' 		=> '400,400italic,300,300italic,700,700italic',
		'character_set' => 'latin,cyrillic-ext,latin-ext,greek-ext,cyrillic,greek,vietnamese',
		'type'			=> 'sans-serif'
	);
	$fonts['Source Sans Pro'] = array(
		'styles' 		=> '400,400italic,200,200italic,300,300italic,600,600italic,700,700italic,900,900italic',
		'character_set' => 'latin,latin-ext',
		'type'			=> 'sans-serif'
	);
	$fonts['Nunito'] = array(
		'styles' 		=> '400,300,700',
		'character_set' => 'latin',
		'type'			=> 'sans-serif'
	);
	$fonts['Francois One'] = array(
		'styles' 		=> '400',
		'character_set' => 'latin,latin-ext',
		'type'			=> 'sans-serif'
	);
	$fonts['Roboto'] = array(
		'styles' 		=> '400,400italic,100,100italic,300,300italic,500,500italic,700,700italic,900,900italic',
		'character_set' => 'latin,cyrillic-ext,latin-ext,cyrillic,greek-ext,greek,vietnamese',
		'type'			=> 'sans-serif'
	);
	$fonts['Raleway'] = array(
		'styles' 		=> '400,100,200,300,600,500,700,800,900',
		'character_set' => 'latin',
		'type'			=> 'sans-serif'
	);
	$fonts['Arimo'] = array(
		'styles' 		=> '400,400italic,700italic,700',
		'character_set' => 'latin,cyrillic-ext,latin-ext,greek-ext,cyrillic,greek,vietnamese',
		'type'			=> 'sans-serif'
	);
	$fonts['Cuprum'] = array(
		'styles' 		=> '400,400italic,700italic,700',
		'character_set' => 'latin,latin-ext,cyrillic',
		'type'			=> 'sans-serif'
	);
	$fonts['Play'] = array(
		'styles' 		=> '400,700',
		'character_set' => 'latin,cyrillic-ext,cyrillic,greek-ext,greek,latin-ext',
		'type'			=> 'sans-serif'
	);
	$fonts['Dosis'] = array(
		'styles' 		=> '400,200,300,500,600,700,800',
		'character_set' => 'latin,latin-ext',
		'type'			=> 'sans-serif'
	);
	$fonts['Abel'] = array(
		'styles' 		=> '400',
		'character_set' => 'latin',
		'type'			=> 'sans-serif'
	);
	$fonts['Droid Serif'] = array(
		'styles' 		=> '400,400italic,700,700italic',
		'character_set' => 'latin',
		'type'			=> 'serif'
	);
	$fonts['Arvo'] = array(
		'styles' 		=> '400,400italic,700,700italic',
		'character_set' => 'latin',
		'type'			=> 'serif'
	);
	$fonts['Lora'] = array(
		'styles' 		=> '400,400italic,700,700italic',
		'character_set' => 'latin',
		'type'			=> 'serif'
	);
	$fonts['Rokkitt'] = array(
		'styles' 		=> '400,700',
		'character_set' => 'latin',
		'type'			=> 'serif'
	);
	$fonts['Bitter'] = array(
		'styles' 		=> '400,400italic,700',
		'character_set' => 'latin,latin-ext',
		'type'			=> 'serif'
	);
	$fonts['Merriweather'] = array(
		'styles' 		=> '400,300,900,700',
		'character_set' => 'latin',
		'type'			=> 'serif'
	);
	$fonts['Vollkorn'] = array(
		'styles' 		=> '400,400italic,700italic,700',
		'character_set' => 'latin',
		'type'			=> 'serif'
	);
	$fonts['Cantata One'] = array(
		'styles' 		=> '400',
		'character_set' => 'latin,latin-ext',
		'type'			=> 'serif'
	);
	$fonts['Kreon'] = array(
		'styles' 		=> '400,300,700',
		'character_set' => 'latin',
		'type'			=> 'serif'
	);
	$fonts['Josefin Slab'] = array(
		'styles' 		=> '400,400italic,100,100italic,300,300italic,600,700,700italic,600italic',
		'character_set' => 'latin',
		'type'			=> 'serif'
	);
	$fonts['Playfair Display'] = array(
		'styles' 		=> '400,400italic,700,700italic,900italic,900',
		'character_set' => 'latin,latin-ext,cyrillic',
		'type'			=> 'serif'
	);
	$fonts['Bree Serif'] = array(
		'styles' 		=> '400',
		'character_set' => 'latin,latin-ext',
		'type'			=> 'serif'
	);
	$fonts['Crimson Text'] = array(
		'styles' 		=> '400,400italic,600,600italic,700,700italic',
		'character_set' => 'latin',
		'type'			=> 'serif'
	);
	$fonts['Old Standard TT'] = array(
		'styles' 		=> '400,400italic,700',
		'character_set' => 'latin',
		'type'			=> 'serif'
	);
	$fonts['Sanchez'] = array(
		'styles' 		=> '400,400italic',
		'character_set' => 'latin,latin-ext',
		'type'			=> 'serif'
	);
	$fonts['Crete Round'] = array(
		'styles' 		=> '400,400italic',
		'character_set' => 'latin,latin-ext',
		'type'			=> 'serif'
	);
	$fonts['Cardo'] = array(
		'styles' 		=> '400,400italic,700',
		'character_set' => 'latin,greek-ext,greek,latin-ext',
		'type'			=> 'serif'
	);
	$fonts['Noticia Text'] = array(
		'styles' 		=> '400,400italic,700,700italic',
		'character_set' => 'latin,vietnamese,latin-ext',
		'type'			=> 'serif'
	);
	$fonts['Judson'] = array(
		'styles' 		=> '400,400italic,700',
		'character_set' => 'latin',
		'type'			=> 'serif'
	);
	$fonts['Lobster'] = array(
		'styles' 		=> '400',
		'character_set' => 'latin,cyrillic-ext,latin-ext,cyrillic',
		'type'			=> 'cursive'
	);
	$fonts['Unkempt'] = array(
		'styles' 		=> '400,700',
		'character_set' => 'latin',
		'type'			=> 'cursive'
	);
	$fonts['Changa One'] = array(
		'styles' 		=> '400,400italic',
		'character_set' => 'latin',
		'type'			=> 'cursive'
	);
	$fonts['Special Elite'] = array(
		'styles' 		=> '400',
		'character_set' => 'latin',
		'type'			=> 'cursive'
	);
	$fonts['Chewy'] = array(
		'styles' 		=> '400',
		'character_set' => 'latin',
		'type'			=> 'cursive'
	);
	$fonts['Comfortaa'] = array(
		'styles' 		=> '400,300,700',
		'character_set' => 'latin,cyrillic-ext,greek,latin-ext,cyrillic',
		'type'			=> 'cursive'
	);
	$fonts['Boogaloo'] = array(
		'styles' 		=> '400',
		'character_set' => 'latin',
		'type'			=> 'cursive'
	);
	$fonts['Fredoka One'] = array(
		'styles' 		=> '400',
		'character_set' => 'latin',
		'type'			=> 'cursive'
	);
	$fonts['Luckiest Guy'] = array(
		'styles' 		=> '400',
		'character_set' => 'latin',
		'type'			=> 'cursive'
	);
	$fonts['Cherry Cream Soda'] = array(
		'styles' 		=> '400',
		'character_set' => 'latin',
		'type'			=> 'cursive'
	);
	$fonts['Lobster Two'] = array(
		'styles' 		=> '400,400italic,700,700italic',
		'character_set' => 'latin',
		'type'			=> 'cursive'
	);
	$fonts['Righteous'] = array(
		'styles' 		=> '400',
		'character_set' => 'latin,latin-ext',
		'type'			=> 'cursive'
	);
	$fonts['Squada One'] = array(
		'styles' 		=> '400',
		'character_set' => 'latin',
		'type'			=> 'cursive'
	);
	$fonts['Black Ops One'] = array(
		'styles' 		=> '400',
		'character_set' => 'latin,latin-ext',
		'type'			=> 'cursive'
	);
	$fonts['Happy Monkey'] = array(
		'styles' 		=> '400',
		'character_set' => 'latin,latin-ext',
		'type'			=> 'cursive'
	);
	$fonts['Passion One'] = array(
		'styles' 		=> '400,700,900',
		'character_set' => 'latin,latin-ext',
		'type'			=> 'cursive'
	);
	$fonts['Nova Square'] = array(
		'styles' 		=> '400',
		'character_set' => 'latin',
		'type'			=> 'cursive'
	);
	$fonts['Metamorphous'] = array(
		'styles' 		=> '400',
		'character_set' => 'latin,latin-ext',
		'type'			=> 'cursive'
	);
	$fonts['Poiret One'] = array(
		'styles' 		=> '400',
		'character_set' => 'latin,latin-ext,cyrillic',
		'type'			=> 'cursive'
	);
	$fonts['Bevan'] = array(
		'styles' 		=> '400',
		'character_set' => 'latin',
		'type'			=> 'cursive'
	);
	$fonts['Shadows Into Light'] = array(
		'styles' 		=> '400',
		'character_set' => 'latin',
		'type'			=> 'cursive'
	);
	$fonts['The Girl Next Door'] = array(
		'styles' 		=> '400',
		'character_set' => 'latin',
		'type'			=> 'cursive'
	);
	$fonts['Coming Soon'] = array(
		'styles' 		=> '400',
		'character_set' => 'latin',
		'type'			=> 'cursive'
	);
	$fonts['Dancing Script'] = array(
		'styles' 		=> '400,700',
		'character_set' => 'latin',
		'type'			=> 'cursive'
	);
	$fonts['Pacifico'] = array(
		'styles' 		=> '400',
		'character_set' => 'latin',
		'type'			=> 'cursive'
	);
	$fonts['Crafty Girls'] = array(
		'styles' 		=> '400',
		'character_set' => 'latin',
		'type'			=> 'cursive'
	);
	$fonts['Calligraffitti'] = array(
		'styles' 		=> '400',
		'character_set' => 'latin',
		'type'			=> 'cursive'
	);
	$fonts['Rock Salt'] = array(
		'styles' 		=> '400',
		'character_set' => 'latin',
		'type'			=> 'cursive'
	);
	$fonts['Amatic SC'] = array(
		'styles' 		=> '400,700',
		'character_set' => 'latin',
		'type'			=> 'cursive'
	);
	$fonts['Leckerli One'] = array(
		'styles' 		=> '400',
		'character_set' => 'latin',
		'type'			=> 'cursive'
	);
	$fonts['Tangerine'] = array(
		'styles' 		=> '400,700',
		'character_set' => 'latin',
		'type'			=> 'cursive'
	);
	$fonts['Reenie Beanie'] = array(
		'styles' 		=> '400',
		'character_set' => 'latin',
		'type'			=> 'cursive'
	);
	$fonts['Satisfy'] = array(
		'styles' 		=> '400',
		'character_set' => 'latin',
		'type'			=> 'cursive'
	);
	$fonts['Gloria Hallelujah'] = array(
		'styles' 		=> '400',
		'character_set' => 'latin',
		'type'			=> 'cursive'
	);
	$fonts['Permanent Marker'] = array(
		'styles' 		=> '400',
		'character_set' => 'latin',
		'type'			=> 'cursive'
	);
	$fonts['Covered By Your Grace'] = array(
		'styles' 		=> '400',
		'character_set' => 'latin',
		'type'			=> 'cursive'
	);
	$fonts['Walter Turncoat'] = array(
		'styles' 		=> '400',
		'character_set' => 'latin',
		'type'			=> 'cursive'
	);
	$fonts['Patrick Hand'] = array(
		'styles' 		=> '400',
		'character_set' => 'latin,vietnamese,latin-ext',
		'type'			=> 'cursive'
	);
	$fonts['Schoolbell'] = array(
		'styles' 		=> '400',
		'character_set' => 'latin',
		'type'			=> 'cursive'
	);
	$fonts['Indie Flower'] = array(
		'styles' 		=> '400',
		'character_set' => 'latin',
		'type'			=> 'cursive'
	);
	return $fonts;
}

function BP_font( $font_name ) {
	$fonts = BP_fonts();
	return $fonts[$font_name];
}

function BP_enqueue_font( $font_name, $extend_character_set = FALSE ) {
	if ( $font_name == 'default' ) return;
	$font = BP_font( $font_name );
	$query_args = array();
	$query_args['family'] = str_replace( ' ', '+', $font_name );
	if ( !empty($font['styles']) && !empty($query_args['family']) ) $query_args['family'] .= ':'. $font['styles'];
	
	if ( $extend_character_set != FALSE ) $query_args['subset'] = 'latin,cyrillic-ext,greek-ext,greek,latin-ext,vietnamese,cyrillic';

	$font_slug = 'font-'. str_replace( ' ', '-', $font_name );
	$font_url = add_query_arg( $query_args, ( is_ssl() ? 'https' : 'http') ."://fonts.googleapis.com/css" );
	wp_enqueue_style( $font_slug, $font_url, NULL, NULL, NULL, FALSE );
}

function BP_font_css_rule( $font_name, $selector, $extend_character_set = FALSE ) {
	if ( empty($font_name) ) return;
	switch ( $font_name ) {
		case 'Arial':
			return $selector ." { font-family: Arial, Helvetica, Lucida, sans-serif; }";
			break;
		case 'Arial Black':
			return $selector ." { font-family: 'Arial Black', Arial, Helvetica, Lucida, sans-serif; }";
			break;
		case 'Lucida':
			return $selector ." { font-family: Lucida, Helvetica, Arial, sans-serif; }";
			break;
		case 'Lucida Grande':
			return $selector ." { font-family: 'Lucida Grande', Lucida, Helvetica, Arial, sans-serif; }";
			break;
		case 'Helvetica':
			return $selector ." { font-family: Helvetica, Arial, Lucida, sans-serif; }";
			break;
		case 'Helvetica Neue':
			return $selector ." { font-family: 'Helvetica Neue', Helvetica, Arial, 'Lucida Grande', Lucida, sans-serif; }";
			break;
		case 'Helvetica Neue Light':
			return $selector ." { font-family: 'HelveticaNeue-Light', 'Helvetica Neue Light', 'Helvetica Neue', Helvetica, Arial, 'Lucida Grande', Lucida, sans-serif; }";
			break;
		case 'Georgia':
			return $selector ." { font-family: Georgia, 'Times New Roman', serif; }";
			break;
		case 'Times New Roman':
			return $selector ." { font-family: 'Times New Roman', Georgia, serif; }";
			break;
		default:
			$font = BP_fonts( $font_name );
			if ( !empty($font['type']) ) return $selector ." { font-family: '". $font_name ."', ". BP_get_websafe_fonts( $font['type'] ) ."; }";
			else return $selector ." { font-family: '". $font_name ."'; }";
			break;
	}
}

function BP_get_websafe_fonts( $type = 'sans-serif' ) {
	switch ( $type ) {
		case 'sans-serif':
			return "Helvetica, Arial, Lucida, sans-serif";
			break;
		case 'serif':
			return "Georgia, 'Times New Roman', serif";
			break;
		case 'cursive':
			return 'cursive';
			break;
	}
}



// print_r(array_merge( BP_get_default_font(), BP_get_google_font( 'AIzaSyBmzfJsfXkXP9PUvwfq53jA1l1YJNxBT4g' ) ));

?>