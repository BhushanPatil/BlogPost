<?php


function BP_get_sidebar_position() {
	if ( is_singular() ) {
		global $wp_query;
		$meta = get_post_meta( $wp_query->post->ID );
		wp_reset_query();
		if ( empty($meta['sidebar_position'][0]) || $meta['sidebar_position'][0] == "default" )
			return get_theme_mod( 'sidebar_position' );
		else return $meta['sidebar_position'][0];
	}
	else return get_theme_mod( 'sidebar_position' );
}

?>