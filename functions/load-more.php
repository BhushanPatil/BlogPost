<?php
/**
 * Functions to load content via AJAX
 */


/* Display load more button */
function BP_load_more( $scroll_auto_load = FALSE, $echo = TRUE ) {

	$load_more_string = __( 'Load More', THEME_TEXTDOMAIN );
	$loading_string = __( 'Loading...', THEME_TEXTDOMAIN );
	$no_results_string = __( 'No more posts to display', THEME_TEXTDOMAIN );

	/* Enqueue AJAX load more script */
	wp_enqueue_script( 'load-more' );

	/* Localize script */
	$ajax_args = array(
		'ajaxUrl' 			=> admin_url( 'admin-ajax.php' ),
		'loadMoreString' 	=> $load_more_string,
		'loadingString' 	=> $loading_string,
		'noResultsString' 	=> $no_results_string,
	);
	wp_localize_script( 'load-more', 'loadMoreVars', $ajax_args );

	/* Build query data */
	global $wp_query;
	
	$max_page = $wp_query->max_num_pages;
	$s = get_query_var( 's' );
	$post_format = get_query_var( 'post_format' );
	$post_type = get_query_var( 'post_type' );
	$author = isset( $wp_query->author ) ? $wp_query->author : '';
	if ( is_category() ) {
		$archive_type = 'category';
		$archive_id = get_query_var( 'cat' );
	}
	else if (is_tag() ) {
		$archive_type = 'post_tag';
		$archive_id = get_query_var( 'tag' );
	}
	else if( is_date() ) {
		$archive_type = 'date';
		$archive_month = get_query_var( 'month' );
		$archive_year = get_query_var( 'year' );
		$archive_m = get_query_var( 'm' );
	}

	/* Display load more if next page exists */
	if ( $max_page > 1 ) {
		
		$output .= '<input id="page" type="hidden" value="1" />';
		$output .= '<input id="max-page" type="hidden" value="'. $max_page .'" />';
		if ( !empty( $s ) ) $output .= '<input id="s" type="hidden" value="'. $s .'" />';
		if ( !empty( $post_type ) && !is_array( $post_type ) ) $output .= '<input id="post-type" type="hidden" value="'. $post_type .'" />';
		if ( !empty( $post_format ) ) $output .= '<input id="post-format" type="hidden" value="'. $post_format .'" />';
		if ( !empty( $author ) ) $output .= '<input id="author" type="hidden" value="'. $author .'" />';
		if ( isset( $archive_type ) ) $output .= '<input id="archive-type" type="hidden" value="'. $archive_type .'" />';
		if ( isset( $archive_id ) ) $output .= '<input id="archive-id" type="hidden" value="'. $archive_id .'" />';
		if ( isset( $archive_month ) ) $output .= '<input id="archive-month" type="hidden" value="'. $archive_month .'" />';
		if ( isset( $archive_year ) ) $output .= '<input id="archive-year" type="hidden" value="'. $archive_year .'" />';
		if ( isset( $archive_m ) ) $output .= '<input id="archive-m" type="hidden" value="'. $archive_m .'" />';
		$output .= '<input id="nonce" type="hidden" value="'. wp_create_nonce( 'load_more_nonce' ) .'" />';
		
		if ( $scroll_auto_load === TRUE ) $output .= '<a id="load-more" class="scroll-auto-load" href="#">'. $load_more_string .'</a>';
		else $output .= '<a id="load-more" href="#">'. $load_more_string .'</a>'; 
		
		if ( $echo ) echo $output;
		else return $output;
	}
}


/* Get AJAX data and execute load more query and print output */
function BP_load_more_query() {

	check_ajax_referer( 'load_more_nonce', 'loadMore_nonce' );
	
	/* Get $_POST Data */
	$page 			= $_POST['loadMore_page'];
	$post_type 		= $_POST['loadMore_post_type'];
	$post_format 	= $_POST['loadMore_post_format'];
	$archive_type 	= $_POST['loadMore_archive_type'];
	$archive_id 	= $_POST['loadMore_archive_id'];
	$archive_month 	= $_POST['loadMore_archive_month'];
	$archive_year 	= $_POST['loadMore_archive_year'];
	$archive_m 		= $_POST['loadMore_archive_m'];
	$author 		= $_POST['loadMore_author'];
	$s 				= $_POST['loadMore_s'];

	/* Loop Arguments */
	$args = array( 'paged' => $page, 'post_type' => $post_type, 'post_status' => 'publish' );

	/* Archives: category, post_tag, date */
	if ( !empty( $archive_type ) ) {
		switch( $archive_type ) {
			case 'category':
				if ( !empty( $archive_id ) ) $args['cat'] = $archive_id;
				break;
			case 'post_tag':
				if ( !empty( $archive_id ) ) $args['tag'] = $archive_id;
				break;
			case 'date':
				if ( !empty( $archive_month ) ) $args['month'] = $archive_month;
				if ( !empty( $archive_year ) ) $args['year'] = $archive_year;
				if ( !empty( $archive_m ) ) $args['m'] = $archive_m;
				break;
		}
	}

	/* Post Format */
	if ( !empty( $post_format ) ) {
		$args['tax_query'] = array(
			array(
				'taxonomy' 	=> 'post_format',
				'field' 	=> 'slug',
				'terms' 	=> $post_format
			)
		);
	}

	/* Author archives */
	if ( !empty( $author ) ) $args['author'] = $author;

	/* Search results */
	if ( !empty( $s ) ) $args['s'] = $s;

	/* Query posts */
	query_posts( $args );

	/* The Loop */
	if ( have_posts() ) {
		ob_start();	// Capture output
		while ( have_posts() ) {
			the_post();
			get_template_part( 'post', get_post_format() );
		}
		echo ob_get_clean(); // Print captured output
	}

	/* Kill WordPress execution */
	wp_die();
}


/**
 * Wordpress AJAX action Hooks
 */
add_action( 'wp_ajax_load_more_query', 'BP_load_more_query' );
add_action( 'wp_ajax_nopriv_load_more_query', 'BP_load_more_query' );


?>