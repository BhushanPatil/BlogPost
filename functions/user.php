<?php

/* Custom User profile settings, Enables more social contact fields */
function BP_user_social_contact_fields( $contactmethods ) {
	if ( !isset( $contactmethods['twitter'] ) )
		$contactmethods['twitter'] = 'Twitter';
	if ( !isset( $contactmethods['facebook'] ) )
		$contactmethods['facebook'] = 'Facebook';
	if ( !isset( $contactmethods['googleplus'] ) )
		$contactmethods['googleplus'] = 'Google+';
	if ( !isset( $contactmethods['youtube'] ) )
		$contactmethods['youtube'] = 'YouTube';
	if ( !isset( $contactmethods['vimeo'] ) )
		$contactmethods['vimeo'] = 'Vimeo';
	if ( !isset( $contactmethods['pinterest'] ) )
		$contactmethods['pinterest'] = 'Pinterest';
	if ( !isset( $contactmethods['dribbble'] ) )
		$contactmethods['dribbble'] = 'Dribbble';
	if ( !isset( $contactmethods['flickr'] ) )
		$contactmethods['flickr'] = 'Flickr';
	if ( !isset( $contactmethods['instagram'] ) )
		$contactmethods['instagram'] = 'Instagram';
	if ( !isset( $contactmethods['linkedin'] ) )
		$contactmethods['linkedin'] = 'LinkedIn';
	if ( !isset( $contactmethods['github'] ) )
		$contactmethods['github'] = 'GitHub';
	
	return $contactmethods;
}
add_filter( 'user_contactmethods', 'BP_user_social_contact_fields', 10, 1 );

function BP_author_social_links( $args = '' ) {
	$defaults = array(
		'before' => '<div class="author-social-links">',
		'after' => '</div><!-- .author-social-links -->',
		'user_id' => NULL,
		'echo' => TRUE
	);
	extract( wp_parse_args( $args, $defaults ), EXTR_SKIP );

	if ( !$user_id ) {
		global $authordata;
		$user_id = isset( $authordata->ID ) ? $authordata->ID : 0;
	}
	else $authordata = get_userdata( $user_id );

	$icons = array();
	$icons['twitter'] = 'fa fa-twitter-square';
	$icons['facebook'] = 'fa fa-facebook-square';
	$icons['googleplus'] = 'fa fa-google-plus-square';
	$icons['youtube'] = 'fa fa-youtube-square';
	$icons['vimeo'] = 'fa fa-vimeo-square';
	$icons['pinterest'] = 'fa fa-pinterest-square';
	$icons['dribbble'] = 'fa fa-dribbble';
	$icons['flickr'] = 'fa fa-flickr';
	$icons['instagram'] = 'fa fa-instagram';
	$icons['linkedin'] = 'fa fa-linkedin-square';
	$icons['github'] = 'fa fa-github-square';
	$fields = BP_user_social_contact_fields( NULL );
	$output = '';
	foreach ( $fields as $field => $value ) {
		$meta_data = get_the_author_meta( $field, $user_id );
		if ( !empty( $meta_data ) ) {
			$output .= '<li><a class="author-'. $field  .'" href="'. $meta_data .'" title="'. sprintf ( __( '%1$s on %2$s', THEME_TEXTDOMAIN ), $authordata->display_name, $value ) .'" target="_blank"><i class="'. $icons[$field] .'"></i></a></li>';
		}
	}
	if ( empty($output) ) return;
	else $output = '<ul>'. $output . '</ul>';

	if ( $echo ) echo $before . $output . $after;
	else return $before . $output . $after;
}

?>