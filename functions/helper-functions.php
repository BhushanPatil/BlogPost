<?php


/* Return links from post content */
function BP_get_links( $content = false ) {
	if ( $content === false)
		$content = get_the_content( false );
	if (preg_match_all("|<a.*(?=href=\"([^\"]*)\")[^>]*>([^<]*)</a>|i", $content, $matches) ) {
		$matches[html] = $matches[0][0];
		$matches[url] = $matches[1][0];
		$matches[text] = $matches[2][0];
		$matches[desciption] = trim(BP_str_replace_once( $matches[0][0], '', $content) );
		return $matches;
	}
	return false;
}


/* Return links from post content */
function BP_str_replace_once( $search, $replacement, $subject ) {
	$pos = strpos( $subject, $search );
	if ( $pos !== false)
		return substr_replace( $subject, $replacement, $pos, strlen( $search) );
}


function BP_get_images( $string, $echo = TRUE ) {

	$dom = new domDocument;

	$dom->loadHTML( $string ); // Load the html into the object

	$dom->preserveWhiteSpace = false; // Discard white space

	$imgs = $dom->getElementsByTagName( 'img' );

	if ( $echo == TRUE ) {
		foreach ( $imgs as $img ) {
			$src = $img->getAttribute( 'src' );
			$alt = $img->getAttribute( 'alt' );
			$height = $img->getAttribute( 'height' ) ? ' height="'. $img->getAttribute( 'height' ) .'"' : '';
			$width = $img->getAttribute( 'width' ) ? ' width="'. $img->getAttribute( 'width' ) .'"' : '';
			echo '<img src="'. $src .'" alt="'.$alt .'"'. $height . $width .' />';
		}
	}
	else {
		$img_array = array();
		foreach ( $imgs as $img ) {
			$img_array[] = array(
				'src' => $img->getAttribute( 'src' ),
				'alt' => $img->getAttribute( 'alt' ),
				'height' => $img->getAttribute( 'height' ),
				'width' => $img->getAttribute( 'width' )
			);
		}
		return $img_array;
	}
}


function BP_get_image( $string, $echo = TRUE ) {

	$dom = new domDocument;

	$dom->loadHTML( $string ); // Load the html into the object

	$dom->preserveWhiteSpace = false; // Discard white space

	$imgs = $dom->getElementsByTagName( 'img' );
	
	if ( $echo == TRUE ) {
		foreach ( $imgs as $img ) {
			$src = $img->getAttribute( 'src' );
			$alt = $img->getAttribute( 'alt' );
			$height = $img->getAttribute( 'height' ) ? ' height="'. $img->getAttribute( 'height' ) .'"' : '';
			$width = $img->getAttribute( 'width' ) ? ' width="'. $img->getAttribute( 'width' ) .'"' : '';
			echo '<img src="'. $src .'" alt="'.$alt .'"'. $height . $width .' />';
			break;
		}
	}
	else {
		$img_array = array();
		foreach ( $imgs as $img ) {
			$img_array[] = array(
				'src' => $img->getAttribute( 'src' ),
				'alt' => $img->getAttribute( 'alt' ),
				'height' => $img->getAttribute( 'height' ),
				'width' => $img->getAttribute( 'width' )
			);
			break;
		}
		return $img_array;
	}
}


function bool2str( $bool ) {
	if ( $bool === true ) return 'true';
	return 'false';
}
function str2bool( $str ) {
	if ( $str === 'true' ) return true;
	return false;
}
function str2cssunit( $str ) {
	$str = str_replace( ' ', '', $str );
	if ( empty( $str ) ) return false;
	if ( is_numeric( $str ) ) return $str .'px';
	if ( strpos( $str, '%' ) === false ||
		strpos( $str, 'in' ) === false ||
		strpos( $str, 'cm' ) === false ||
		strpos( $str, 'mm' ) === false ||
		strpos( $str, 'em' ) === false ||
		strpos( $str, 'ex' ) === false ||
		strpos( $str, 'pt' ) === false ||
		strpos( $str, 'pc' ) === false ||
		strpos( $str, 'px' ) === false ) return $str;
	return false;
}


/* Get the WordPress excerpt by specifying a maximum amount of characters */
function BP_excerpt_max_char( $charlength = 140, $echo = TRUE ) {
	$excerpt = get_the_excerpt();
	$charlength++;
	$output = '';
	if ( mb_strlen( $excerpt ) > $charlength ) {
		$subex = mb_substr( $excerpt, 0, $charlength - 5 );
		$exwords = explode( ' ', $subex );
		$excut = - ( mb_strlen( $exwords[ count( $exwords ) - 1 ] ) );
		if ( $excut < 0 ) $output .= mb_substr( $subex, 0, $excut );
		else $output .= $subex;
		$output .= '…';
	}
	else $output .= $excerpt;
	if ( $echo == TRUE ) echo $output;
	else return $output;
}


?>