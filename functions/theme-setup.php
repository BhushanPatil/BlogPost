<?php


/* Sets up theme defaults and registers the various WordPress features that theme supports. */
function BP_theme_setup() {
	/* Makes theme available for translation. Translations can be added to the /languages/ directory. */
	load_theme_textdomain( THEME_TEXTDOMAIN, THEME_DIR . '/languages' );

	/* Adds RSS feed links to <head> for posts and comments. */
	// add_theme_support( 'automatic-feed-links' );

	/* This theme supports all available post formats by default. See http://codex.wordpress.org/Post_Formats */
	add_theme_support( 'post-formats', array( 'aside', 'audio', 'chat', 'gallery', 'image', 'link', 'quote', 'status', 'video' ) );

	/* This theme uses a custom image size for featured images, displayed on "standard" posts and pages. */
	add_theme_support( 'post-thumbnails' );
	// set_post_thumbnail_size( 604, 270, true ); 
	// add_image_size( 'slider-thumb', 853, 480, true );

	/* This theme uses wp_nav_menu() in two location. */
	register_nav_menus (
		array(
			'main'		=> __( 'Main', THEME_TEXTDOMAIN ),
			'footer'	=> __( 'Footer', THEME_TEXTDOMAIN ),
		)
	);

	/* Add custom background support. */
	add_theme_support( 'custom-background' );
}
add_action( 'after_setup_theme', 'BP_theme_setup' );


/* Enqueues scripts and styles for front end. */
function BP_scripts_styles() {
	/* Register jQuery */
	wp_deregister_script( 'jquery' ); // First, deregister wordpress jquery
	wp_register_script( 'jquery', THEME_URI ."/js/jquery-2.1.0.min.js", FALSE, NULL, FALSE );

	/* Register AJAX load more script */
	wp_register_script( 'load-more', THEME_URI .'/js/load-more.js', array( 'jquery' ), NULL, TRUE );
	
	/* Enqueue Flexslider: Responsive slider toolkit */
	wp_enqueue_script( 'flexslider', THEME_URI .'/js/jquery.flexslider-min.js', array( 'jquery' ), NULL, TRUE );

	/* Enqueue Doubletaptogo: Responsive and Touch-Friendly DropDown Navigation */
	wp_enqueue_script( 'doubletaptogo', THEME_URI .'/js/doubletaptogo.min.js', array( 'jquery' ), NULL, TRUE );
	
	/* Enqueue Custom JavaScript file with functionality specific to Theme. */
	wp_enqueue_script( 'functions', THEME_URI .'/js/theme-functions.js', array( 'jquery' ), NULL, TRUE );

	/* Enqueue theme stylesheet */
	wp_enqueue_style( 'theme-style', get_bloginfo('stylesheet_url'), NULL, NULL, NULL, FALSE );

	/* Enqueue default font if it is used */
	$heading_font = get_theme_mod( 'heading_font' );
	BP_enqueue_font( empty($heading_font['face']) || $heading_font['face'] == "default" ? 'PT Sans Narrow' : $heading_font['face'] );
	$body_font = get_theme_mod( 'body_font' );
	BP_enqueue_font( empty($body_font['face']) || $body_font['face'] == "default" ? 'PT Sans' : $body_font['face'] );
}
add_action( 'wp_enqueue_scripts', 'BP_scripts_styles' );


/* Creates a nicely formatted and more specific title element text for output in head of document, based on current view. */
function BP_wp_title( $title, $seprator ) {
	global $paged, $page;

	if ( is_feed() ) return $title;

	/* Add the site name. */
	$title .= get_bloginfo( 'name' );

	/* Add the site description for the home/front page. */
	$site_description = get_bloginfo( 'description', 'display' );
	if ( $site_description && ( is_home() || is_front_page() ) ) $title = "$title $seprator $site_description";

	/* Add a page number if necessary. */
	if ( $paged >= 2 || $page >= 2 ) $title = $title .' '. $seprator .' '. __( 'Page', THEME_TEXTDOMAIN ) .' '. max( $paged, $page );

	return $title;
}
add_filter( 'wp_title', 'BP_wp_title', 10, 2 );


/* Extends the default WordPress body classes. */
function BP_body_class( $classes ) {
	$classes[] = BP_get_sidebar_position();
	if ( !get_option( 'show_avatars' ) ) $classes[] = 'no-avatars';
	if ( get_theme_mod( 'responsive' ) ) $classes[] = 'responsive';
	return $classes;
}
add_filter( 'body_class', 'BP_body_class' );


/* Remove image dimension attributes */
function remove_thumbnail_dimensions( $html, $post_id, $post_image_id ) {
	$html = preg_replace( '/(width|height)=\"\d*\"\s/', "", $html );
	return $html;
}
add_filter( 'post_thumbnail_html', 'remove_thumbnail_dimensions', 10, 3 );


/* Configure the content more link */
function BP_the_content_more_link( $more_link, $more_link_text ) {
	return str_replace( $more_link_text, 'more', ' …'. $more_link );
}
add_filter( 'the_content_more_link', 'BP_the_content_more_link', 10, 2 );


/* Custom excerpt content length */
function BP_excerpt_length( $length ) {
	$post_format = get_post_format();
	if ( $post_format == 'aside' || $post_format == 'image' || $post_format == 'quote' || $post_format == 'status' ) return 999;
	else return 55;
}
add_filter( 'excerpt_length', 'BP_excerpt_length' );


/* Configure excerpt more link */
function BP_the_excerpt_more_link( $more ) {
	$post_format = get_post_format();
	if ( $post_format == 'aside' || $post_format == 'image' || $post_format == 'quote' || $post_format == 'status' ) return;
	else return ' … <a href="'. get_permalink() . '" class="more-link">more</a>';
}
add_filter( 'excerpt_more', 'BP_the_excerpt_more_link' );


/* Configure categories list for styling purpose */
function BP_cat_count($links) {
	$links = str_replace( '</a> (', '</a> <span>', $links );
	$links = str_replace( ')', '</span>', $links );
	return $links;
}
add_filter( 'wp_list_categories', 'BP_cat_count' );


/* Configure tag cloud for styling purpose */
// function BP_tag_icon( $links ) {
// 	$links = preg_replace( '#(<a [^>]*>)#', '$1<i class="fa fa-tag"></i>&nbsp;&nbsp;', $links );
// 	return $links;
// }
// add_filter( 'wp_tag_cloud', 'BP_tag_icon' );


/* Exclude Pages from search */
function BP_exclude_pages_in_search( $query ) {
	if ( $query->is_search ) $query->set( 'post_type', 'post' );
	return $query;
}
add_filter( 'pre_get_posts', 'BP_exclude_pages_in_search' );


/* Register sidebars */
function BP_sidebars_init() {
	register_sidebar( array(
		'name'          => __( 'Main Widget Area', THEME_TEXTDOMAIN ),
		'id'            => 'sidebar-1',
		'description'   => __( 'Appears on posts and pages in the sidebar.', THEME_TEXTDOMAIN ),
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside><!--END .widget -->' . "\n",
		'before_title'  => '<div class="widget-header"><h3 class="widget-title">',
		'after_title'   => '</h3></div>',
	) );
	register_sidebar( array(
		'name'          => __( 'Footer Widget Area', THEME_TEXTDOMAIN ),
		'id'            => 'sidebar-2',
		'description'   => __( 'Appears in the footer section of the site.', THEME_TEXTDOMAIN ),
		'before_widget' => '<div class="widget-col col-3"><aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside><!--END .widget --></div>' . "\n",
		'before_title'  => '<div class="widget-header"><h3 class="widget-title">',
		'after_title'   => '</h3></div>',
	) );
}
add_action( 'widgets_init', 'BP_sidebars_init' );


?>