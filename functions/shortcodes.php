<?php


/* Return shortcodes from post content */
function BP_get_shortcodes( $content, $tags, $html = false ) {
	$tags = (array)$tags;
	$shortcodes = array();
	if ( preg_match_all( '/'. get_shortcode_regex() .'/s', $content, $matches, PREG_SET_ORDER ) ) {
		foreach ( $matches as $shortcode ) {
			foreach ( $tags as $key => $val ) {
				if ( $val === $shortcode[2] ) {
					if ( $html ) $shortcodes[] = do_shortcode_tag( $shortcode );
					else $shortcodes[] = $shortcode[0];
				}
			}
		}
	}
	return $shortcodes;
}


/* Return a shortcode from post content */
function BP_get_shortcode( $content, $tags, $html = false ) {
	$shortcodes = BP_get_shortcodes( $content, $tags, $html );
	return $shortcodes[0];
}


/* Delete shortcodes from post content */
function BP_strip_shortcodes( $content, $tags ) {
	$tags = (array)$tags;
	if ( preg_match_all( '/'. get_shortcode_regex() .'/s', $content, $matches, PREG_SET_ORDER ) ) {
		foreach ( $matches as $shortcode ) {
			foreach ( $tags as $key => $val ) {
				if ( $val === $shortcode[2] ) {
					$pos = strpos( $content, $shortcode[0] );
					if ( $pos !== false) $content = substr_replace( $content, '', $pos, strlen( $shortcode[0]) );
				}
			}
		}
	}
	return $content;
}


/* Delete a shortcode from post content */
function BP_strip_shortcode( $content, $tags ) {
	$tags = (array)$tags;
	if ( preg_match_all( '/'. get_shortcode_regex() .'/s', $content, $matches, PREG_SET_ORDER ) ) {
		foreach ( $matches as $shortcode ) {
			foreach ( $tags as $key => $val ) {
				if ( $val === $shortcode[2] ) {
					$pos = strpos( $content, $shortcode[0] );
					if ( $pos !== false) return substr_replace( $content, '', $pos, strlen( $shortcode[0]) );
				}
			}
		}
	}
	return $content;
}


function BP_content_strip_shortcodes( $content = null, $tags, $more_link_textt = null, $stripteaser = false ) {
	if ( $content === null ) $content = get_the_content( $more_link_text, $strip_teaser );
	$tags = (array)$tags;
	$content = BP_strip_shortcodes( $content, $tags );
	$content = apply_filters( 'the_content', $content );
	$content = str_replace( ']]>', ']]&gt;', $content );
	return $content;
}


function BP_content_strip_shortcode( $content = null, $tags, $more_link_textt = null, $stripteaser = false ) {
	if ( $content === null ) $content = get_the_content( $more_link_text, $strip_teaser );
	$tags = (array)$tags;
	$content = BP_strip_shortcode( $content, $tags );
	$content = apply_filters( 'the_content', $content );
	$content = str_replace( ']]>', ']]&gt;', $content );
	return $content;
}


?>