<?php


/* ============================================================================ *
 * Meta boxes																*
 * ============================================================================ */
function BP_meta_boxes() {

	$image_path =  THEME_URI . '/images/';

	// "Post Options" Metabox for Posts
	$meta_boxes[] = array(
		'id' => 'meta-box-post-options',
		'title' => 'Post Options',
		'page' => 'post',
		'context' => 'normal',
		'priority' => 'high',
		'fields' => array(
			array(
				'label_name' => 'Sidebar Position',
				'label_description' => 'Select the layout you want on this specific post.',
				'id' => 'sidebar-position',
				'name' => 'sidebar_position',
				'type' => 'radio',
				'option_checked' => 'default',
				'options' => array( 
					array( 'value' => 'default', 'title' => 'Default: Overrides default theme layout.', 'image' => $image_path .'layout-default.png' ),
					array( 'value' => 'no-sidebar', 'title' => 'No sidebar', 'image' => $image_path .'layout-no-sidebar.png' ),
					array( 'value' => 'left-sidebar', 'title' => 'Left sidebar', 'image' => $image_path .'layout-left-sidebar.png' ),
					array( 'value' => 'right-sidebar', 'title' => 'Right sidebar', 'image' => $image_path .'layout-right-sidebar.png' )
				)
			)
		)
	);

	// "Page Options" Metabox for Pages
	$meta_boxes[] = array(
		'id' => 'meta-box-post-options',
		'title' => 'Page Options',
		'page' => 'page',
		'context' => 'normal',
		'priority' => 'high',
		'fields' => array(
			array(
				'label_name' => 'Sidebar Position',
				'label_description' => 'Select the layout you want on this specific page.',
				'id' => 'sidebar-position',
				'name' => 'sidebar_position',
				'type' => 'radio',
				'option_checked' => 'default',
				'options' => array( 
					array( 'value' => 'default', 'title' => 'Default: Overrides default theme layout.', 'image' => $image_path .'layout-default.png' ),
					array( 'value' => 'no-sidebar', 'title' => 'No sidebar', 'image' => $image_path .'layout-no-sidebar.png' ),
					array( 'value' => 'left-sidebar', 'title' => 'Left sidebar', 'image' => $image_path .'layout-left-sidebar.png' ),
					array( 'value' => 'right-sidebar', 'title' => 'Right sidebar', 'image' => $image_path .'layout-right-sidebar.png' )
				)
			)
		)
	);

	// Add meta boxes 
	foreach ( $meta_boxes as $meta_box ) BP_add_meta_box( $meta_box );
}
add_action( 'add_meta_boxes', 'BP_meta_boxes' );


?>