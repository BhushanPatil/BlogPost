<?php
/**
 * This file contains functions to manage meta boxes
 */


/* Add custom Meta Boxes */
function BP_add_meta_box( $meta_box ) {

	if( !is_array( $meta_box ) ) return;

	if ( $meta_box['page'] == 'post-standard' ||
		$meta_box['page'] == 'post-aside' ||
		$meta_box['page'] == 'post-gallery' ||
		$meta_box['page'] == 'post-link' ||
		$meta_box['page'] == 'post-image' ||
		$meta_box['page'] == 'post-quote' ||
		$meta_box['page'] == 'post-status' ||
		$meta_box['page'] == 'post-video' ||
		$meta_box['page'] == 'post-audio' ||
		$meta_box['page'] == 'post-chat' ) {
		add_filter( 'postbox_classes_post_'. $meta_box['id'], 'BP_meta_box_'. str_replace( "-", "_", $meta_box['page'] ) .'_class' );
		$meta_box['page'] = 'post';
	}

	$callback = create_function( '$post, $meta_box', 'BP_meta_box_html( $post, $meta_box["args"] );' ); // Create a callback function

	add_meta_box( $meta_box['id'], $meta_box['title'], $callback, $meta_box['page'], $meta_box['context'], $meta_box['priority'], $meta_box );
}
function BP_meta_box_post_standard_class( $class ) { array_push( $class, 'meta-box-for-post-standard' ); return $class; }
function BP_meta_box_post_aside_class( $class ) { array_push( $class, 'meta-box-for-post-aside' ); return $class; }
function BP_meta_box_post_gallery_class( $class ) { array_push( $class, 'meta-box-for-post-gallery' ); return $class; }
function BP_meta_box_post_link_class( $class ) { array_push( $class, 'meta-box-for-post-link' ); return $class; }
function BP_meta_box_post_image_class( $class ) { array_push( $class, 'meta-box-for-post-image' ); return $class; }
function BP_meta_box_post_quote_class( $class ) { array_push( $class, 'meta-box-for-post-quote' ); return $class; }
function BP_meta_box_post_status_class( $class ) { array_push( $class, 'meta-box-for-post-status' ); return $class; }
function BP_meta_box_post_video_class( $class ) { array_push( $class, 'meta-box-for-post-video' ); return $class; }
function BP_meta_box_post_audio_class( $class ) { array_push( $class, 'meta-box-for-post-audio' ); return $class; }
function BP_meta_box_post_chat_class( $class ) { array_push( $class, 'meta-box-for-post-chat' ); return $class; }


/* Create content for a custom Meta Box */
function BP_meta_box_html( $post, $meta_box ) {

	if( !is_array($meta_box) ) return false;

	if( isset( $meta_box['description'] ) && $meta_box['description'] != '' ) echo '<p>'. $meta_box['description'] .'</p>';

	wp_nonce_field( basename(__FILE__), 'BP_meta_box_nonce' );

	echo '<table class="form-table">';

	$i = 0;

	foreach ( $meta_box['fields'] as $field ) {

		// Get current post meta data
		$meta = get_post_meta( $post->ID, $field['name'], true );

		if ( isset( $meta_box['description'] ) && $meta_box['description'] != '' )
			echo '<tr style=\'border-top: 1px solid #EEE;\'><th><label for="'. $field['id'] .'">'. $field['label_name'] .':<span style=\'display:block;color:#999;font-size:12px;font-weight:normal;\'>'. $field['label_description'] .'</span></label></th>';
		else {
			if ( $i == 0 ) 
				echo '<tr><th><label for="'. $field['id'] .'">'. $field['label_name'] .':<span style=\'display:block;color:#999;font-size:12px;font-weight:normal;\'>'. $field['label_description'] .'</span></label></th>';
			else
				echo '<tr style=\'border-top: 1px solid #EEE;\'><th><label for="'. $field['id'] .'">'. $field['label_name'] .':<span style=\'display:block;color:#999;font-size:12px;font-weight:normal;\'>'. $field['label_description'] .'</span></label></th>';
		}

		$i++;

		switch( $field['type'] ) {
			case 'text':
				echo '<td><input type="text" name="BP_meta['. $field['name'] .']" id="'. $field['id'] .'" value="'. ($meta ? $meta : $field['value']) .'" placeholder="'. $field['placeholder'] .'" size="30" style="'. $field['style'] .'"></td>';
				break;

			case 'textarea':
				echo '<td><textarea name="BP_meta['. $field['name'] .']" id="'. $field['id'] .'" placeholder="'. $field['placeholder'] .'" rows="8" cols="5" style="'. $field['style'] .'">'. ($meta ? $meta : $field['value']) .'</textarea></td>';
				break;
				
			case 'radio':
				echo '<td>';
				foreach( $field['options'] as $option ) {
					$option_id = str_replace( '_', '-', $field['id'] .'-'. str_replace( ' ', '-', $option['value'] ) );
					if ( !empty($meta) && empty($option_checked) && $option['value'] === $meta ) $option_checked = ' checked="checked"';
					else if ( empty($meta) && empty($option_checked) && $option['value'] === $field['option_checked'] ) $option_checked = ' checked="checked"';
					else $option_checked = '';
					if ( !empty( $option['image'] ) ) {
						echo '<style>input#'. $option_id .' { display: none; } input#'. $option_id .' + img { display:inline-block; margin-right:4px; border:3px solid #CECECE; cursor:pointer; } input#'. $option_id .':checked + img { border-color: #ABABAB; } </style>';
						echo '<label for="'. $option_id .'" title="'. $option['title'] .'"><input type="radio" id="'. $option_id .'" name="BP_meta['. $field['name'] .']" value="'. $option['value'] .'"'. $option_checked .'><img src="'. $option['image'] .'" alt="'. $option['title'] .'" /></label>';
					}
					else if ( !empty( $option['text'] ) )
						echo '<label for="'. $option_id .'" title="'. $option['title'] .'"><input type="radio" id="'. $option_id .'" name="BP_meta['. $field['name'] .']" value="'. $option['value'] .'"'. $option_checked .'>&nbsp;'. $option['text'] .'</label>&nbsp;&nbsp;';
					else
						echo '<label for="'. $option_id .'" title="'. $option['title'] .'"><input type="radio" id="'. $option_id .'" name="BP_meta['. $field['name'] .']" value="'. $option['value'] .'"'. $option_checked .'>&nbsp;';
				}
				echo '</td>';
				break;

			case 'checkbox':
				echo '<td>';
				foreach( $field['checkboxes'] as $checkbox ) {
					$checkbox_id = str_replace('_', '-', $field['id'] .'-'. str_replace(' ', '-', $checkbox['value']));
					$checkbox_name = $field['name'] .'_'. $checkbox['value'];
					$checkbox_checked = '';
					if( $meta ) {
						if ( $meta == 'on' || $meta == 'checked' ) $checkbox_checked = ' checked="checked"';
					}
					else if ( $checkbox['checked'] == 'on' || $checkbox['checked'] == 'checked' ) $checkbox_checked = ' checked="checked"';
					echo '<input type="hidden" name="BP_meta['. $checkbox_name .']" value="off">';
					echo '<label for="'. $checkbox_id .'"><input type="checkbox" id="'. $checkbox_id .'" name="BP_meta['. $checkbox_name .']" value="on"'. $checkbox_checked .'>&nbsp;'. $checkbox['text'] .'</label>&nbsp;&nbsp;';
				}
				echo '</td>';
				break;

			case 'select':
				echo'<td><select name="BP_meta['. $field['name'] .']" id="'. $field['id'] .'" style="'. $field['style'] .'">';
				foreach( $field['options'] as $option ) {
					echo'<option value="'. $option["value"] .'"';
					if ( $meta ) { if ( $meta == $option["value"] ) echo ' selected="selected"'; }
					else if ( $field['option_selected'] == $option["value"] ) echo ' selected="selected"';
					echo '>'. $option["text"] .'</option>';
				}
				echo'</select></td>';
				break;
		}
		echo '</tr>';
	}
	echo '</table>';
}


/* Save Meta Box data */
function BP_save_meta_box( $post_id ) {
	// Verify nonce
	if ( ! isset($_POST['BP_meta']) || ! isset($_POST['BP_meta_box_nonce']) || ! wp_verify_nonce( $_POST['BP_meta_box_nonce'], basename( __FILE__ ) ) ) return;

	// Check autosave
	if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) return;

	// Check permissions
	if ( 'page' == $_POST['post_type'] ) {
		if ( !current_user_can( 'edit_page', $post_id ) ) return;
	}
	else if ( !current_user_can( 'edit_post', $post_id ) ) return;

	// Save/Update Meta Box data
	foreach( $_POST['BP_meta'] as $key => $val ) {
		if ( $val == '' ) delete_post_meta( $post_id, $key, get_post_meta( $post_id, $key, true ) );
		else update_post_meta( $post_id, $key, $val );
	}
}
add_action( 'save_post', 'BP_save_meta_box' );


/* Enqueue meta box javascript in admin area */
function BP_admin_meta_boxes_js() {
	wp_enqueue_script( 'metaboxes', THEME_URI .'/js/admin-meta-boxes.js', array( 'jquery' ), NULL );
}
add_action( 'admin_enqueue_scripts', 'BP_admin_meta_boxes_js' );


?>