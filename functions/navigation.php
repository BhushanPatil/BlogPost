<?php


/* Displays navigation to next/previous set of posts when applicable. */
function BP_paging_nav( $args = '' ) {
	$defaults = array(
		'before' => '<nav class="paging-navigation" role="navigation">',
		'after' => '</nav><!--END .paging-navigation -->',
		'style' => get_theme_mod( 'pagination_style', 'numbers-1' ),
		'echo' => TRUE
	);
	extract( wp_parse_args( $args, $defaults ), EXTR_SKIP );

	global $wp_query;
	if ( $wp_query->max_num_pages <= 1 ) return;

	if ( $style == 1 || $style == 'next-prev' ) {
		$output = BP_paginate_links( array(
			'current' 			=> max( 1, get_query_var( 'paged' ) ),
			'total' 			=> $wp_query->max_num_pages,
			'mid_size' 			=> 0,
			'show_current' 		=> FALSE,
			'first_last' 		=> FALSE,
			'prev_text' 		=> '<i class="fa fa-angle-left"></i> '. __( 'Prev', THEME_TEXTDOMAIN ),
			'next_text' 		=> __( 'Next', THEME_TEXTDOMAIN ) .' <i class="fa fa-angle-right"></i>',
			'class_prev' 		=> 'button float-left',
			'class_next' 		=> 'button float-right'
		));
	}
	else if ( $style == 2 || $style == 'numbers-1' ) {
		$output = BP_paginate_links( array(
			'current' 			=> max( 1, get_query_var( 'paged' ) ),
			'total' 			=> $wp_query->max_num_pages,
			'first_last' 		=> FALSE,
			'prev_text' 		=> '<i class="fa fa-angle-left"></i> '. __( 'Prev', THEME_TEXTDOMAIN ),
			'next_text' 		=> __( 'Next', THEME_TEXTDOMAIN ) .' <i class="fa fa-angle-right"></i>',
			'mid_size' 			=> 3,
			'class_mid' 		=> 'button h5',
			'class_current' 	=> 'button h5 main-color no-events',
			'class_prev' 		=> 'button h5',
			'class_next' 		=> 'button h5'
		));
	}
	else if ( $style == 3 || $style == 'numbers-2' ) {
		$output = BP_paginate_links( array(
			'current' 			=> max( 1, get_query_var( 'paged' ) ),
			'total' 			=> $wp_query->max_num_pages,
			'prev_text' 		=> '<i class="fa fa-angle-left"></i> '. __( 'Prev', THEME_TEXTDOMAIN ),
			'next_text' 		=> __( 'Next', THEME_TEXTDOMAIN ) .' <i class="fa fa-angle-right"></i>',
			'first_text' 		=> '',
			'last_text' 		=> '',
			'class_mid' 		=> 'button h5',
			'class_current' 	=> 'button h5 main-color no-events',
			'class_prev' 		=> 'button h5',
			'class_next' 		=> 'button h5',
			'class_first' 		=> 'button h5',
			'class_last' 		=> 'button h5',
			'class_dots' 		=> 'h5',
			'dots_text' 		=> '&nbsp;<i class="fa fa-ellipsis-h"></i>&nbsp;'
		));
	}
	else if ( $style == 4 || $style == 'load-more' ) {
		$output = BP_load_more( NULL, FALSE );
	}
	else if ( $style == 5 || $style == 'scroll-auto-load' ) {
		$output = BP_load_more( TRUE, FALSE );
	}

	if ( empty($output) ) return;

	if ( $echo ) echo $before . $output . $after;
	else return $before . $output . $after;
}


/* Displays navigation to next/previous post when applicable. */
function BP_post_nav() {
	global $post;
	$previous = ( is_attachment() ) ? get_post( $post->post_parent ) : get_adjacent_post( FALSE, '', TRUE );
	if ( !$next && !$previous ) return; // Don't print empty markup if there's nowhere to navigate.
	?>
	<footer id="content-footer">
		<div class="grid-container">
			<nav class="post-navigation col-8" role="navigation">
				<div class="nav-prev"><?php previous_post_link( '%link', _x( '<span><i class="fa fa-chevron-left"></i> Previous</span>%title', 'Previous post link', 'bp' ) ); ?></div>
				<div class="nav-next"><?php next_post_link( '%link', _x( '<span>Next <i class="fa fa-chevron-right"></i></span>%title', 'Next post link', 'bp' ) ); ?></div>
			</nav><!--END .post-navigation -->
		</div>
	</footer><!--END #content-footer -->
	<?php
}


/* Displays breadcrumbs when applicable. */
function BP_breadcrumbs( $args = '' ) {
	$defaults = array(
		'before' => '<nav id="breadcrumbs">',
		'after' => '</nav><!--END #breadcrumbs -->',
		'found_posts' => TRUE,
		'container' => TRUE,
		'container_before' => '<header id="content-header"><div class="grid-container"><div class="col-12">',
		'container_after' => '</div></header><!--END #content-header -->',
		'echo' => TRUE,
	);
	extract( wp_parse_args( $args, $defaults ), EXTR_SKIP );

	$output = '';

	if ( is_category() )
		$output .= __( 'Category Archives:', THEME_TEXTDOMAIN ) .' '. single_cat_title( null, FALSE );

	// elseif ( is_page() )
	// 	$output .= get_the_title();

	elseif ( is_search() )
		$output .= sprintf( __( 'Search Results for... "%s"', THEME_TEXTDOMAIN ), '<em>'. get_search_query() .'</em>' );

	elseif ( is_tag() )
		$output .= __( 'Tag Archives:', THEME_TEXTDOMAIN ) .' '. single_tag_title( null, FALSE );

	elseif ( is_author() )
		$output .= sprintf( __( 'Author: %s', THEME_TEXTDOMAIN ), get_the_author() );

	elseif ( is_tax( 'post_format', 'post-format-aside' ) )
		$output .= __( 'Asides', THEME_TEXTDOMAIN );
	elseif ( is_tax( 'post_format', 'post-format-audio' ) )
		$output .= __( 'Audio', THEME_TEXTDOMAIN );
	elseif ( is_tax( 'post_format', 'post-format-chat' ) )
		$output .= __( 'Chat', THEME_TEXTDOMAIN );
	elseif ( is_tax( 'post_format', 'post-format-gallery' ) )
		$output .= __( 'Galleries', THEME_TEXTDOMAIN );
	elseif ( is_tax( 'post_format', 'post-format-image' ) )
		$output .= __( 'Images', THEME_TEXTDOMAIN );
	elseif ( is_tax( 'post_format', 'post-format-link' ) )
		$output .= __( 'Links', THEME_TEXTDOMAIN );
	elseif ( is_tax( 'post_format', 'post-format-quote' ) )
		$output .= __( 'Quotes', THEME_TEXTDOMAIN );
	elseif ( is_tax( 'post_format', 'post-format-status' ) )
		$output .= __( 'Status', THEME_TEXTDOMAIN );
	elseif ( is_tax( 'post_format', 'post-format-video' ) )
		$output .= __( 'Videos', THEME_TEXTDOMAIN );

	elseif ( is_archive() ) {
		if ( is_day() )
			$output .= sprintf( __( 'Daily Archives: %s', THEME_TEXTDOMAIN ), get_the_date() );
		elseif ( is_month() )
			$output .= sprintf( __( 'Monthly Archives: %s', THEME_TEXTDOMAIN ), get_the_date( _x( 'F Y', 'monthly archives date format', THEME_TEXTDOMAIN ) ) );
		elseif ( is_year() )
			$output .= sprintf( __( 'Yearly Archives: %s', THEME_TEXTDOMAIN ), get_the_date( _x( 'Y', 'yearly archives date format', THEME_TEXTDOMAIN ) ) );
		else
			$output .= __( 'Archives', THEME_TEXTDOMAIN );
	}

	if ( !empty($output) ) {

		$output = '<div id="breadcrumbs-links"><h1>'. $output .'</h1></div><!--END #breadcrumbs-links -->';

		if ( $found_posts ) {
			global $wp_query;
			$output .= '<div id="breadcrumbs-post-count"><span>';
			$output .= sprintf( '<i class="fa fa-file-text"></i>&nbsp;'. _n( '1 Post', '%s Posts', $wp_query->found_posts, THEME_TEXTDOMAIN ), $wp_query->found_posts );
			$output .= '</span></div><!--END #breadcrumbs-post-count -->';
		}

		$output = $before . $output . $after;
		if ( $container ) $output = $container_before . $output . $container_after;
		if ( $echo ) echo $output;
		else return $output;
	}
}


?>