<?php


function BP_theme_customizer_options( $wp_customize ) {

	$priority = 100;

	//
	$wp_customize->remove_section( 'static_front_page' );

	/*********** Section: Site Title, Tagline & Logo ***********/
	$wp_customize->add_section( 'title_tagline' , array(
		'title' 		=> 'Site Title, Tagline & Logo'
	) );

	/* Setting[Checkbox]: Show Tagline */
	$wp_customize->add_setting( 'show_tagline', array(
		'default' 		=> TRUE
    ) );
	$wp_customize->add_control(
	    'show_tagline',
	    array(
	        'section'   => 'title_tagline',
	        'label'     => 'Show Tagline',
	        'type'      => 'checkbox'
	    )
	);

	/* Setting[Image]: Logo */
	$wp_customize->add_setting( 'site_logo' );
	$wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'site_logo', array(
		'label' 		=> 'Logo',
	    'section' 		=> 'title_tagline',
	    'settings' 		=> 'site_logo'
	) ) );



	/*********** Section: Fonts ***********/
	$wp_customize->add_section(
		'fonts',
		array(
			'title'     => 'Fonts',
			'priority'  => $priority++
		)
	);

	/* Load google fonts choices */
	$fonts = x_get_fonts();
	$font_choices = array();
	$font_choices['default'] = 'Default Theme Font';
	foreach ( $fonts as $font_name => $font_properties ) $font_choices[$font_name] = $font_name;

	/* Setting[Select]: Header Font */
	$wp_customize->add_setting(
		'test_typography[family]',
		array( 'default'    => 'default' )
	);
	$wp_customize->add_control(
		'test_typography[family]',
		array(
			'section' 	=> 'fonts',
			'label' 	=> 'Header Font',
			'type' 		=> 'select',
			'choices'	=> $font_choices
		)
	);



	
	/*********** Section: Layout Options ***********/
	$wp_customize->add_section(
		'layout_options',
		array(
			'title'     => 'Layout Options',
			'priority'  => $priority++
		)
	);

	/* Setting[Radio]: Sidebar Position */
	$wp_customize->add_setting(
		'sidebar_position',
		array( 'default'    => 'right-sidebar' )
	);
	$wp_customize->add_control(
		'sidebar_position',
		array(
			'section' 	=> 'layout_options',
			'label' 	=> 'Sidebar Position',
			'type' 		=> 'radio',
			'choices' 	=> array(
				'right-sidebar' => 'Right Sidebar',
				'left-sidebar' 	=> 'Left Sidebar',
				'no-sidebar' 	=> 'No Sidebar'
			)
		)
	);

	/* Setting[Select]: Pagination Style */
	$wp_customize->add_setting(
		'pagination_style',
		array( 'default'    => 'numbers-2' )
	);
	$wp_customize->add_control(
		'pagination_style',
		array(
			'section' 	=> 'layout_options',
			'label' 	=> 'Pagination Style',
			'type' 		=> 'select',
			'choices' 	=> array(
				'next-prev' 			=> 'Next Prev',
				'numbers-1' 			=> 'Page Numbers 1',
				'numbers-2' 			=> 'Page Numbers 2',
				'load-more' 			=> 'Load More',
				'scroll-auto-load' 		=> 'Scroll Auto Load'
			)
		)
	);


	
	/*********** Section: Footer Options ***********/
	$wp_customize->add_section(
		'footer_options',
		array(
			'title'     => 'Footer Options',
			'priority'  => $priority++
		)
	);

	/* Setting[Text]: Copyright Text */
	$wp_customize->add_setting(
		'copyright_text',
		array(
			'default'    => '&copy; '. date("Y") .' <a href="'. home_url( '/' ) .'">'. get_bloginfo( 'name' ) .'</a>. Proudly Powered by <a href="http://wordpress.org">WordPress</a>.'
		)
	);
	$wp_customize->add_control(
		'copyright_text',
		array(
			'section'   => 'footer_options',
			'label'     => 'Copyright Text',
			'type'      => 'text'
		)
	);

}
add_action( 'customize_register', 'BP_theme_customizer_options' );


function BP_customizer_css() {
	$body_font = get_theme_mod( 'body_font' );
	$heading_font = get_theme_mod( 'body_font' );
	?><style type="text/css">
	<?php echo BP_font_css_rule( empty($heading_font['face']) ? 'PT Sans' : $heading_font['face'], 'body' ); ?>
	<?php echo BP_font_css_rule( empty($body_font['face']) ? 'PT Sans Narrow' : $body_font['face'], 'h1, h2, h3, h4, h5, h6' ); ?>
	<?php echo BP_font_css_rule( empty($heading_font['face']) ? 'PT Sans' : $heading_font['face'], '#main-menu.desktop li.fa > a' ); ?>
	<?php echo BP_font_css_rule( empty($heading_font['face']) ? 'PT Sans' : $heading_font['face'], '#tagline h2' ); ?>
	</style><?php
}
add_action( 'wp_head', 'BP_customizer_css' );


function BP_social_links( $args = '' ) {

	$defaults = array(
		'before' 	=> '<div id="social-links">',
		'after' 	=> '</div><!--END #social-links -->'
	);
	extract( wp_parse_args( $args, $defaults ), EXTR_SKIP );

	$social_links = array();

	if ( !empty( get_theme_mod( 'twitter_link' ) ) ) 		$social_links['twitter_link'] = 		get_theme_mod( 'twitter_link' );
	if ( !empty( get_theme_mod( 'facebook_link' ) ) ) 		$social_links['facebook_link'] = 		get_theme_mod( 'facebook_link' );
	if ( !empty( get_theme_mod( 'google_plus_link' ) ) ) 	$social_links['google_plus_link'] = 	get_theme_mod( 'google_plus_link' );
	if ( !empty( get_theme_mod( 'youtube_link' ) ) ) 		$social_links['youtube_link'] = 		get_theme_mod( 'youtube_link' );
	if ( !empty( get_theme_mod( 'vimeo_link' ) ) ) 			$social_links['vimeo_link'] = 			get_theme_mod( 'vimeo_link' );
	if ( !empty( get_theme_mod( 'pinterest_link' ) ) ) 		$social_links['pinterest_link'] = 		get_theme_mod( 'pinterest_link' );
	if ( !empty( get_theme_mod( 'dribbble_link' ) ) ) 		$social_links['dribbble_link'] = 		get_theme_mod( 'dribbble_link' );
	if ( !empty( get_theme_mod( 'flickr_link' ) ) ) 		$social_links['flickr_link'] = 			get_theme_mod( 'flickr_link' );
	if ( !empty( get_theme_mod( 'flickr_link' ) ) ) 		$social_links['flickr_link'] = 			get_theme_mod( 'flickr_link' );
	if ( !empty( get_theme_mod( 'instagram_link' ) ) ) 		$social_links['instagram_link'] = 		get_theme_mod( 'instagram_link' );
	if ( !empty( get_theme_mod( 'linkedin_link' ) ) ) 		$social_links['linkedin_link'] = 		get_theme_mod( 'linkedin_link' );
	if ( !empty( get_theme_mod( 'github_link' ) ) ) 		$social_links['github_link'] = 			get_theme_mod( 'github_link' );
	if ( !empty( get_theme_mod( 'rss_link' ) ) ) 			$social_links['rss_link'] = 			get_theme_mod( 'rss_link' );

	if ( !empty( $social_links ) ) {
		echo $before .'<ul>';

		/* Twitter Link */
		if ( !empty( $social_links['twitter_link'] ) )
			echo '<li><a href="'. $social_links['twitter_link'] .'" title="Twitter" class="twitter-link"><i class="fa fa-twitter"></i></a></li>';

		/* Facebook Link */
		if ( !empty( $social_links['facebook_link'] ) )
			echo '<li><a href="'. $social_links['facebook_link'] .'" title="Facebook" class="facebook-link"><i class="fa fa-facebook"></i></a></li>';

		/* Google+ Link */
		if ( !empty( $social_links['google_plus_link'] ) )
			echo '<li><a href="'. $social_links['google_plus_link'] .'" title="Google+" class="googleplus-link"><i class="fa fa-google-plus"></i></a></li>';

		/* YouTube Link */
		if ( !empty( $social_links['youtube_link'] ) )
			echo '<li><a href="'. $social_links['youtube_link'] .'" title="YouTube" class="youtube-link"><i class="fa fa-youtube"></i></a></li>';

		/* Vimeo Link */
		if ( !empty( $social_links['vimeo_link'] ) )
			echo '<li><a href="'. $social_links['vimeo_link'] .'" title="Vimeo" class="vimeo-link"><i class="fa fa-vimeo-square"></i></a></li>';

		/* Pinterest Link */
		if ( !empty( $social_links['pinterest_link'] ) )
			echo '<li><a href="'. $social_links['pinterest_link'] .'" title="Pinterest" class="pinterest-link"><i class="fa fa-pinterest"></i></a></li>';

		/* Dribbble Link */
		if ( !empty( $social_links['dribbble_link'] ) )
			echo '<li><a href="'. $social_links['dribbble_link'] .'" title="Dribbble" class="dribbble-link"><i class="fa fa-dribbble"></i></a></li>';

		/* Flickr Link */
		if ( !empty( $social_links['flickr_link'] ) )
			echo '<li><a href="'. $social_links['flickr_link'] .'" title="Flickr" class="flickr-link"><i class="fa fa-flickr"></i></a></li>';

		/* Instagram Link */
		if ( !empty( $social_links['instagram_link'] ) )
			echo '<li><a href="'. $social_links['instagram_link'] .'" title="Instagram" class="instagram-link"><i class="fa fa-instagram"></i></a></li>';

		/* LinkedIn Link */
		if ( !empty( $social_links['linkedin_link'] ) )
			echo '<li><a href="'. $social_links['linkedin_link'] .'" title="LinkedIn" class="linkedin-link"><i class="fa fa-linkedin"></i></a></li>';

		/* GitHub Link */
		if ( !empty( $social_links['github_link'] ) )
			echo '<li><a href="'. $social_links['github_link'] .'" title="GitHub" class="github-link"><i class="fa fa-github"></i></a></li>';

		/* RSS Link */
		if ( !empty( $social_links['rss_link'] ) )
			echo '<li><a href="'. $social_links['rss_link'] .'" title="RSS" class="rss-link"><i class="fa fa-rss"></i></a></li>';

		echo '</ul>'. $after;
	}
}


?>