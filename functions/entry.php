<?php

/* Prints entry title for current post inside loop. */
function BP_entry_title( $args = '' ) {
	$defaults = array(
		'before' => '<div class="entry-title">',
		'after' => '</div><!--END .entry-title -->',
		'heading' => 'h1',
		'single' => ( is_singular() ) ? TRUE : FALSE,
		'show_id' => FALSE, // Show post ID if title is empty
		'echo' => TRUE
	);
	extract( wp_parse_args( $args, $defaults ), EXTR_SKIP );
	$title = the_title( NULL, NULL, FALSE );
	if ( empty($title) ) {
		if ( $show_id === TRUE ) $title = get_the_ID();
		else return;
	}
	if ( $single === TRUE ) $output = $title;
	else $output = '<a href="'. get_permalink() .'" title="'. the_title_attribute( array( 'echo' => FALSE ) ) .'" rel="bookmark">'. $title .'</a>';

	if ( $heading === 'h1' || $heading === 'h2' || $heading === 'h3' || $heading === 'h4' || $heading === 'h5' || $heading === 'h6' )
		$output = '<'. $heading .'>'. $output .'</'. $heading .'>';

	if ( $echo ) echo $before . $output . $after;
	else return $before . $output . $after;
}


/* Return entry meta data array */
function BP_entry_thumbnail( $args = '' ) {

	$defaults = array(
		'post_id' => NULL,
		'size' => 'post-thumbnail',
		'before' => '<figure class="entry-thumbnail">',
		'after' => '</figure><!--END .entry-thumbnail -->',
		'caption_img' => FALSE,
		'no_image' => FALSE,
		'single' => ( is_singular() ) ? TRUE : FALSE,
		'echo' => TRUE
	);
	extract( wp_parse_args( $args, $defaults ), EXTR_SKIP );

	if ( post_password_required() ) return;

	if ( has_post_thumbnail() ) {
		if ( is_array($size) )
			$thumbnail = '<img src="'. BP_image_resize( wp_get_attachment_url( get_post_thumbnail_id() ), $size[0], $size[1], TRUE ) .'" class="thumbnail wp-post-image" alt="'. get_the_title() .'" />';
		else
			$thumbnail = get_the_post_thumbnail( $post_id, $size, $attr );
	}
	else if ( $caption_img === true ) {
		$the_shortcode = BP_get_shortcode( get_the_content(), array( 'caption' ) );
		if ( !empty( $the_shortcode ) ) {
			// $the_shortcode = preg_replace( '/(width|height)=\"\d*\"\s/', "", $the_shortcode );
			$thumbnail = do_shortcode( strip_tags( $the_shortcode, '<img>' ) );
		}
	}

	if ( empty($thumbnail) && $no_image )
		$thumbnail = '<img src="'. THEME_URI .'/images/no-image-2.png" class="thumbnail wp-post-image" alt="No thumbnail" />';

	if ( !empty($thumbnail) ) {
		if ( $single === TRUE ) $output = $thumbnail;
		else $output = '<a href="'. get_permalink() .'" title="'. the_title_attribute( array( 'echo' => FALSE ) ) .'" rel="bookmark">'. $thumbnail .'</a>';

		if ( $echo ) echo $before . $output . $after;
		else return $before . $output . $after;
	}
}


/**
 * Prints HTML with meta information for current post inside loop.: author, categories, tags, permalink, and date
 *
 * %sticky% 	= Sticky Post
 * %format% 	= Post Format
 * %formatlink% = Post Format Link
 * %date% 		= Date
 * %datelink% 	= Date Link
 * %author% 	= Author
 * %authorlink% = Author Link
 * %cats%		= Entry Categories
 * %tags% 		= Entry Tags
 * %comments% 	= Comments Link
 * %edit%		= Post Edit Link
*/
function BP_entry_meta( $args = '' ) {
	$defaults = array(
		'meta' => '%sticky%%datelink%%cats%%comments%%edit%',
		'before' => '<div class="entry-meta">',
		'after' =>  '</div><!--END .entry-meta -->',
		'date_format' => NULL,
		'show_icon' => TRUE,
		'echo' => TRUE
	);
	extract( wp_parse_args( $args, $defaults ), EXTR_SKIP );

	$icon = '';

	/* Post sticky */
	if ( is_sticky() && is_home() && !is_paged() ) {
		if ( $show_icon ) $icon = '<i class="fa fa-thumb-tack"></i> ';
		$meta = str_replace( '%sticky%', '<span class="sticky-post">'. $icon . __( 'Sticky', THEME_TEXTDOMAIN ) .'</span>', $meta );
	}

	/* Post format */
	$format = get_post_format();
	if ( $format == 'aside' ) {
		if ( $show_icon ) $icon = '<i class="fa fa-file-text"></i> ';
		$meta = str_replace( '%format%', '<span class="post-format">'. $icon . get_post_format_string( $format ) .'</span>', $meta );
		$meta = str_replace( '%formatlink%', '<span class="post-format"><a class="entry-format" href="'. esc_url( get_post_format_link( $format ) ) .'">'. $icon . get_post_format_string( $format ) .'</a></span>', $meta );
	}
	elseif ( $format == 'audio' ) {
		if ( $show_icon ) $icon = '<i class="fa fa-volume-up"></i> ';
		$meta = str_replace( '%format%', '<span class="post-format">'. $icon . get_post_format_string( $format ) .'</span>', $meta );
		$meta = str_replace( '%formatlink%', '<span class="post-format"><a class="entry-format" href="'. esc_url( get_post_format_link( $format ) ) .'">'. $icon . get_post_format_string( $format ) .'</a></span>', $meta );
	}
	elseif ( $format == 'chat' ) {
		if ( $show_icon ) $icon = '<i class="fa fa-comments-o"></i> ';
		$meta = str_replace( '%format%', '<span class="post-format">'. $icon . get_post_format_string( $format ) .'</span>', $meta );
		$meta = str_replace( '%formatlink%', '<span class="post-format"><a class="entry-format" href="'. esc_url( get_post_format_link( $format ) ) .'">'. $icon . get_post_format_string( $format ) .'</a></span>', $meta );
	}
	elseif ( $format == 'gallery' ) {
		if ( $show_icon ) $icon = '<i class="fa fa-picture-o"></i> ';
		$meta = str_replace( '%format%', '<span class="post-format">'. $icon . get_post_format_string( $format ) .'</span>', $meta );
		$meta = str_replace( '%formatlink%', '<span class="post-format"><a class="entry-format" href="'. esc_url( get_post_format_link( $format ) ) .'">'. $icon . get_post_format_string( $format ) .'</a></span>', $meta );
	}
	elseif ( $format == 'image' ) {
		if ( $show_icon ) $icon = '<i class="fa fa-picture-o"></i> ';
		$meta = str_replace( '%format%', '<span class="post-format">'. $icon . get_post_format_string( $format ) .'</span>', $meta );
		$meta = str_replace( '%formatlink%', '<span class="post-format"><a class="entry-format" href="'. esc_url( get_post_format_link( $format ) ) .'">'. $icon . get_post_format_string( $format ) .'</a></span>', $meta );
	}
	elseif ( $format == 'link' ) {
		if ( $show_icon ) $icon = '<i class="fa fa-link"></i> ';
		$meta = str_replace( '%format%', '<span class="post-format">'. $icon . get_post_format_string( $format ) .'</span>', $meta );
		$meta = str_replace( '%formatlink%', '<span class="post-format"><a class="entry-format" href="'. esc_url( get_post_format_link( $format ) ) .'">'. $icon . get_post_format_string( $format ) .'</a></span>', $meta );
	}
	elseif ( $format == 'quote' ) {
		if ( $show_icon ) $icon = '<i class="fa fa-quote-left"></i> ';
		$meta = str_replace( '%format%', '<span class="post-format">'. $icon . get_post_format_string( $format ) .'</span>', $meta );
		$meta = str_replace( '%formatlink%', '<span class="post-format"><a class="entry-format" href="'. esc_url( get_post_format_link( $format ) ) .'">'. $icon . get_post_format_string( $format ) .'</a></span>', $meta );
	}
	elseif ( $format == 'status' ) {
		if ( $show_icon ) $icon = '<i class="fa fa-comment"></i> ';
		$meta = str_replace( '%format%', '<span class="post-format">'. $icon . get_post_format_string( $format ) .'</span>', $meta );
		$meta = str_replace( '%formatlink%', '<span class="post-format"><a class="entry-format" href="'. esc_url( get_post_format_link( $format ) ) .'">'. $icon . get_post_format_string( $format ) .'</a></span>', $meta );
	}
	elseif ( $format == 'video' ) {
		if ( $show_icon ) $icon = '<i class="fa fa-play-circle-o"></i> ';
		$meta = str_replace( '%format%', '<span class="post-format">'. $icon . get_post_format_string( $format ) .'</span>', $meta );
		$meta = str_replace( '%formatlink%', '<span class="post-format"><a class="entry-format" href="'. esc_url( get_post_format_link( $format ) ) .'">'. $icon . get_post_format_string( $format ) .'</a></span>', $meta );
	}
	

	/* Post date */
	if ( $show_icon ) $icon = '<i class="fa fa-clock-o"></i> ';
	$meta = str_replace( '%date%', '<span class="date">'. $icon .'<time class="entry-date" datetime="'. get_the_date( 'c' ) .'">'. get_the_date( $date_format ) .'</time></span>', $meta );
	$meta = str_replace( '%datelink%', '<span class="date"><a href="'. get_permalink() .'" title="'. sprintf( __( 'Permalink to %s', THEME_TEXTDOMAIN ), the_title_attribute( 'echo=0' ) ) .'" rel="bookmark">'. $icon .'<time class="entry-date" datetime="'. get_the_date( 'c' ) .'">'. get_the_date( $date_format ) .'</time></a></span>', $meta );


	/* Post author */
	if ( $show_icon ) $icon = '<i class="fa fa-user"></i> ';
	$meta = str_replace( '%author%', '<span class="author vcard">'. $icon . get_the_author() .'</span>', $meta );
	$meta = str_replace( '%authorlink%', '<span class="author vcard"><a class="url fn n" href="'. get_author_posts_url( get_the_author_meta( 'ID' ) ) .'" title="'. sprintf( __( 'Posts by %s', THEME_TEXTDOMAIN ), get_the_author() ) .'" rel="author">'. $icon . get_the_author() .'</a></span>', $meta );
	
	/* Translators: used between list items, there is a space after the comma. */
	if ( $show_icon ) $icon = '<i class="fa fa-folder-open"></i> ';
	$categories_list = get_the_category_list( __( '<span class="seprator">,</span> ', THEME_TEXTDOMAIN ) );
	if ( $categories_list )
		$meta = str_replace( '%cats%', '<span class="categories-links">'. $icon . $categories_list .'</span>', $meta );

	/* Translators: used between list items, there is a space after the comma. */
	if ( $show_icon ) $icon = '<i class="fa fa-tags"></i> ';
	$tag_list = get_the_tag_list( $icon, __( '<span class="seprator">,</span> ', THEME_TEXTDOMAIN ) );
	if ( $tag_list ) $meta = str_replace( '%tags%', '<span class="tags-links">'. $tag_list .'</span>', $meta );


	/* Post Comments */
	// if ( comments_open() ) {
		if ( $show_icon ) $icon = '<i class="fa fa-tags"></i> ';
		$comment .= '<span class="comments">';
		$comment .= BP_get_comments_popup_link( 
			$icon . __( 'No Comments', THEME_TEXTDOMAIN ),
			$icon . __( '1 Comment', THEME_TEXTDOMAIN ),
			$icon . __( '% Comments', THEME_TEXTDOMAIN ),
			'comments-link',
			$icon . __( 'Comments Off', THEME_TEXTDOMAIN )
		);
		$comment .= '</span>';
		$meta = str_replace( '%comments%', $comment, $meta );
	// }

	/* Edit Link */
	if ( get_edit_post_link() ) {
		if ( $show_icon ) $icon = '<i class="fa fa-pencil"></i> ';
		$meta = str_replace( '%edit%', '<span class="edit-link"><a class="post-edit-link" href="'. get_edit_post_link() .'">'. $icon . __( 'Edit', THEME_TEXTDOMAIN ) .'</a></span>', $meta );
	}

	$meta = str_replace( '%sticky%', '', $meta );
	$meta = str_replace( '%format%', '', $meta );
	$meta = str_replace( '%formatlink%', '', $meta );
	$meta = str_replace( '%date%', '', $meta );
	$meta = str_replace( '%datelink%', '', $meta );
	$meta = str_replace( '%author%', '', $meta );
	$meta = str_replace( '%authorlink%', '', $meta );
	$meta = str_replace( '%cats%', '', $meta );
	$meta = str_replace( '%tags%', '', $meta );
	$meta = str_replace( '%comments%', '', $meta );
	$meta = str_replace( '%edit%', '', $meta );

	if ( empty(trim($meta)) ) return;
	if ( $echo ) echo $before . $meta . $after;
	else return $before . $meta . $after;
}


function BP_entry_content( $args = '' ) {
	$defaults = array(
		'content' => NULL,
		'more_link_text' => NULL,
		'strip_teaser' => NULL,
		'before' => '<div class="entry-content">',
		'after' => '</div><!--END .entry-content -->',
		'excerpt' => FALSE,
		'echo' => TRUE,
		'paging' => TRUE,
		'paging_before' => '<nav class="post-paging-navigation"><span class="pages-text">Pages:</span>',
		'paging_after' => '</nav><!--END .post-page-navigation -->',
		'paging_link_before' => '<span class="page-number">',
		'paging_link_after' => '</span>',
	);
	extract( wp_parse_args( $args, $defaults ), EXTR_SKIP );
	if ( is_search() || $excerpt == TRUE ) $output = get_the_excerpt();
	else {
		if ( $content === NULL ) $content = get_the_content( $more_link_text, $strip_teaser );
		$content = apply_filters( 'the_content', $content );
		$output = str_replace( ']]>', ']]&gt;', $content );
		if ( $paging === TRUE ) {
			$output .= wp_link_pages( array(
				'before' => $paging_before,
				'after' => $paging_after,
				'link_before' => $paging_link_before,
				'link_after' => $paging_link_after,
				'echo' => FALSE
			));
		}
	}
	if ( empty($output) ) return;
	if ( $echo ) echo $before . $output . $after;
	else return $before . $output . $after;
}


function BP_entry_excerpt( $args = '' ) {
	$defaults = array(
		'length' => 140,
		'before' => '<div class="entry-content">',
		'after' => '</div><!--END .entry-content -->',
		'echo' => TRUE
	);
	extract( wp_parse_args( $args, $defaults ), EXTR_SKIP );
	$excerpt = get_the_excerpt();
	if ( empty($excerpt) ) return;
	$length++;
	$output = '';
	if ( mb_strlen( $excerpt ) > $length ) {
		$subex = mb_substr( $excerpt, 0, $length - 5 );
		$exwords = explode( ' ', $subex );
		$excut = -( mb_strlen( $exwords[ count( $exwords ) - 1 ] ) );
		if ( $excut < 0 ) $output .= mb_substr( $subex, 0, $excut );
		else $output .= $subex;
		$output .= '…';
	}
	else $output .= $excerpt;
	if ( $echo ) echo $before . $output . $after;
	else return $before . $output . $after;
}


function BP_entry_tags( $args = '' ) {
	$defaults = array(
		'before' => '<div class="entry-tags"><span class="tags-label-text">'. __( 'Tags:', THEME_TEXTDOMAIN) .'</span> ',
		'after' => '</div><!--END .entry-tags -->',
		'link_class' => 'entry-tag',
		'echo' => TRUE
	);
	extract( wp_parse_args( $args, $defaults ), EXTR_SKIP );
	$output = '';
	$tags = get_the_tags();
	if ( $tags ) {
		foreach( $tags as $tag ) {
			$output .= '<a class="'. $link_class .'" href="'. get_tag_link( $tag->term_id ) .'"'. ( $tag->description ? ' title="'. $tag->description .'"' : '' ) .'>'. $tag->name .'</a>';
		}
	}
	if ( empty($output) ) return;
	if ( $echo ) echo $before . $output . $after;
	else return $before . $output . $after;
}


function BP_entry_social_media_share1( $args = '' ) {
	$defaults = array(
		'before' => '<div class="post-share"><span class="share-label-text">Share on:</spna>',
		'after' => '</div><!--END .post-social-share -->',
		'echo' => TRUE
	);
	extract( wp_parse_args( $args, $defaults ), EXTR_SKIP );

	$output = '<a class="facebook-share-link" href="http://www.facebook.com/sharer.php?u='. get_permalink() .'&amp;t='. get_the_title() .'" target="blank" title="'. sprintf( __( 'Share &quot;%s&quot; on Facebook', THEME_TEXTDOMAIN ), the_title( NULL, NULL, FALSE ) ) .'"><i class="fa fa-facebook-square" tiy></i></a>';
	$output .= '<a class="twitter-share-link" href="http://twitter.com/home?status='. get_permalink() .'" target="_blank" title="'. sprintf( __( 'Share &quot;%s&quot; on Twitter', THEME_TEXTDOMAIN ), the_title( NULL, NULL, FALSE ) ) .'"><i class="fa fa-twitter-square"></i></a>';
	$output .= '<a class="google-plus-share-link" href="https://plusone.google.com/_/+1/confirm?hl=en-US&amp;url='. get_permalink() .'" target="_blank" title="'. sprintf( __( 'Share &quot;%s&quot; on Google+', THEME_TEXTDOMAIN ), the_title( NULL, NULL, FALSE ) ) .'"><i class="fa fa-google-plus-square"></i></a>';

	$output .= '<a class="pinterest-share-link" href="#" target="_blank" title="'. sprintf( __( 'Share &quot;%s&quot; on Pinterest', THEME_TEXTDOMAIN ), the_title( NULL, NULL, FALSE ) ) .'"><i class="fa fa-pinterest-square"></i></a>';


	if ( $echo ) echo $before . $output . $after;
	else return $before . $output . $after;
}


function BP_entry_social_media_share( $args = '' ) {
	$defaults = array(
		'before' => '<div class="post-share"><span class="share-label-text">Share on:</span>',
		'after' => '</div><!--END .post-social-share -->',
		'echo' => TRUE
	);
	extract( wp_parse_args( $args, $defaults ), EXTR_SKIP );

	$output = '<a class="facebook-share-link" href="http://www.facebook.com/sharer.php?u='. get_permalink() .'&amp;t='. get_the_title() .'" target="blank" title="'. sprintf( __( 'Share &quot;%s&quot; on Facebook', THEME_TEXTDOMAIN ), the_title( NULL, NULL, FALSE ) ) .'">Facebook</a>';
	$output .= '<a class="twitter-share-link" href="http://twitter.com/home?status='. get_permalink() .'" target="_blank" title="'. sprintf( __( 'Share &quot;%s&quot; on Twitter', THEME_TEXTDOMAIN ), the_title( NULL, NULL, FALSE ) ) .'">Twitter</a>';
	$output .= '<a class="google-plus-share-link" href="https://plusone.google.com/_/+1/confirm?hl=en-US&amp;url='. get_permalink() .'" target="_blank" title="'. sprintf( __( 'Share &quot;%s&quot; on Google+', THEME_TEXTDOMAIN ), the_title( NULL, NULL, FALSE ) ) .'">Google+</a>';
	$output .= '<a class="pinterest-share-link" href="#" target="_blank" title="'. sprintf( __( 'Share &quot;%s&quot; on Pinterest', THEME_TEXTDOMAIN ), the_title( NULL, NULL, FALSE ) ) .'">Pinterest</a>';


	if ( $echo ) echo $before . $output . $after;
	else return $before . $output . $after;
}



?>