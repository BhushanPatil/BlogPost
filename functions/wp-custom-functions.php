<?php


/**
 * Display or retrieve list of pages with optional home link.
 *
 * The arguments are listed below and part of the arguments are for {@link
 * wp_list_pages()} function. Check that function for more info on those
 * arguments.
 *
 * <ul>
 * <li><strong>sort_column</strong> - How to sort the list of pages. Defaults
 * to 'menu_order, post_title'. Use column for posts table.</li>
 * <li><strong>menu_class</strong> - Class to use for the div ID which contains
 * the page list. Defaults to 'menu'.</li>
 * <li><strong>echo</strong> - Whether to echo list or return it. Defaults to
 * echo.</li>
 * <li><strong>link_before</strong> - Text before show_home argument text.</li>
 * <li><strong>link_after</strong> - Text after show_home argument text.</li>
 * <li><strong>show_home</strong> - If you set this argument, then it will
 * display the link to the home page. The show_home argument really just needs
 * to be set to the value of the text of the link.</li>
 * </ul>
 *
 * @param array|string $args
 * @return string html menu
 */
function BP_page_menu( $args = '' ) {

	$defaults = array(
		'sort_column' => 'menu_order, post_title',
		'menu_class' => 'menu',
		'echo' => TRUE,
		'link_before' => '',
		'link_after' => ''
	);
	$args = apply_filters( 'wp_page_menu_args', wp_parse_args( $args, $defaults ) );

	$menu = '';

	$list_args = $args;

	// Show Home in the menu
	if ( ! empty($args['show_home']) ) {

		if ( TRUE === $args['show_home'] || '1' === $args['show_home'] || 1 === $args['show_home'] ) $text = __( 'Home', THEME_TEXTDOMAIN );
		else $text = $args['show_home'];

		$class = '';

		if ( is_front_page() && !is_paged() ) $class = 'class="current_page_item"';

		$menu .= '<li ' . $class . '><a href="' . home_url( '/' ) . '">' . $args['link_before'] . $text . $args['link_after'] . '</a></li>';
		
		// If the front page is a page, add it to the exclude list
		if ( get_option('show_on_front') == 'page' ) {

			if ( !empty( $list_args['exclude'] ) ) $list_args['exclude'] .= ',';
			else $list_args['exclude'] = '';

			$list_args['exclude'] .= get_option('page_on_front');
		}
	}

	$list_args['echo'] = FALSE;
	$list_args['title_li'] = '';
	$menu .= str_replace( array( "\r", "\n", "\t" ), '', wp_list_pages($list_args) );

	if ( $menu ) $menu = '<ul class="' . esc_attr($args['menu_class']) . '">' . $menu . "</ul>\n";

	$menu = apply_filters( 'BP_page_menu', $menu, $args );

	if ( $args['echo'] ) echo $menu;
	else return $menu;
}


/**
 * Displays the link to the comments popup window for the current post ID.
 *
 * Is not meant to be displayed on single posts and pages. Should be used on the
 * lists of posts
 *
 * @global string $wpcommentspopupfile  The URL to use for the popup window.
 * @global int    $wpcommentsjavascript Whether to use JavaScript. Set when function is called.
 *
 * @param string $zero      Optional. The string to display when no comments. Default FALSE.
 * @param string $one       Optional. The string to display when only one comment is available. Default FALSE.
 * @param string $more      Optional. The string to display when there are more than one comment. Default FALSE.
 * @param string $css_class Optional. The CSS class to use for comments. Default empty.
 * @param string $none      Optional. The string to display when comments have been turned off. Default FALSE.
 * @return null Returns null on single posts and pages.
 */
function BP_get_comments_popup_link( $zero = FALSE, $one = FALSE, $more = FALSE, $css_class = 'comments-link', $none = FALSE ) {
	global $wpcommentspopupfile, $wpcommentsjavascript;

	$id = get_the_ID();

	if ( FALSE === $zero ) $zero = __( 'No Comments', THEME_TEXTDOMAIN );
	if ( FALSE === $one ) $one = __( '1 Comment', THEME_TEXTDOMAIN );
	if ( FALSE === $more ) $more = __( '% Comments', THEME_TEXTDOMAIN );
	if ( FALSE === $none ) $none = __( 'Comments Off', THEME_TEXTDOMAIN );

	$number = get_comments_number( $id );

	$link = '';

	if ( 0 == $number && !comments_open() && !pings_open() )
		return $none;

	if ( post_password_required() )
		return __( 'Enter your password to view comments.', THEME_TEXTDOMAIN );

	$link .= '<a href="';
	if ( $wpcommentsjavascript ) {
		if ( empty( $wpcommentspopupfile ) )
			$home = home_url();
		else
			$home = get_option('siteurl');
		$link .= $home .'/'. $wpcommentspopupfile .'?comments_popup='. $id;
		$link .= '" onclick="wpopen(this.href); return FALSE"';
	}
	elseif ( is_single() ) {
		if ( $number > 0 )
			$link .= '#comments"';
		else
			$link .= '#respond"';
	}
	else {
		// if comments_popup_script() is not in the template, display simple comment link
		if ( 0 == $number )
			$link .= get_permalink() .'#respond"';
		else
			$link .= get_comments_link() .'"';
	}

	if ( !empty( $css_class ) )
		$link .= ' class="'. $css_class .'" ';

	$title = the_title_attribute( array( 'echo' => 0 ) );
	
	$attributes = '';

	/**
	 * Filter the comments popup link attributes for display.
	 *
	 * @param string $attributes The comments popup link attributes. Default empty.
	 */
	$link .= apply_filters( 'comments_popup_link_attributes', $attributes );

	$link .= ' title="'. esc_attr( sprintf( __( 'Comment on %s', THEME_TEXTDOMAIN ), $title ) ) .'">';

	if ( $number > 1 )
		$link .= str_replace( '%', number_format_i18n($number), ( FALSE === $more ) ? __( '% Comments', THEME_TEXTDOMAIN ) : $more );
	elseif ( $number == 0 )
		$link .= ( FALSE === $zero ) ? __( 'No Comments', THEME_TEXTDOMAIN ) : $zero;
	else // must be one
		$link .= ( FALSE === $one ) ? __( '1 Comment', THEME_TEXTDOMAIN ) : $one;

	$link .= '</a>';

	return $link;
}


/* Retrieve paginated link for archive post pages. */
function BP_paginate_links( $args = '' ) {
	$defaults = array(
		'base' 				=> '%_%', // http://example.com/all_posts.php%_% : %_% is replaced by format (below)
		'format' 			=> '?page=%#%', // ?page=%#% : %#% is replaced by the page number
		'total' 			=> 1,
		'current' 			=> 0,
		'show_all' 			=> FALSE,
		'show_current' 		=> TRUE,
		'prev_next' 		=> TRUE,
		'prev_text' 		=> __( '&laquo; Prev', THEME_TEXTDOMAIN ),
		'next_text' 		=> __( 'Next &raquo;', THEME_TEXTDOMAIN ),
		'first_last' 		=> TRUE,
		'first_text' 		=> '',
		'last_text' 		=> '',
		'mid_size' 			=> 2,
		'show_dots' 		=> TRUE,
		'dots_text' 		=> '…',
		'type' 				=> 'plain',
		'add_args' 			=> FALSE, // array of query args to add
		'add_fragment' 		=> '',
		'class' 			=> '',
		'class_mid' 		=> '',
		'class_current' 	=> '',
		'class_prev' 		=> '',
		'class_next' 		=> '',
		'class_first' 		=> '',
		'class_last' 		=> '',
		'class_dots' 		=> ''
	);

	$args = wp_parse_args( $args, $defaults );
	extract($args, EXTR_SKIP);

	// Who knows what else people pass in $args
	$total = (int) $total;
	if ( $total < 2 ) return;
	
	$current  = (int) $current;
	$mid_size = 0 <= (int) $mid_size ? (int) $mid_size : 2;
	$add_args = is_array($add_args) ? $add_args : FALSE;
	if ( empty($first_text) ) $first_text = '1';
	if ( empty($last_text) ) $last_text = (int) $total;
	if ( !empty($class) ) 			$class = ' '. $class;
	if ( !empty($class_mid) ) 		$class_mid = ' '. $class_mid;
	if ( !empty($class_current) ) 	$class_current = ' '. $class_current;
	if ( !empty($class_prev) ) 		$class_prev = ' '. $class_prev;
	if ( !empty($class_next) ) 		$class_next = ' '. $class_next;
	if ( !empty($class_first) ) 	$class_first = ' '. $class_first;
	if ( !empty($class_last) ) 		$class_last = ' '. $class_last;
	if ( !empty($class_dots) ) 		$class_dots = ' '. $class_dots;

	$page_links = array();
	$n = 0;

	if ( $prev_next && 1 < $current ) :
		$link = get_pagenum_link($current - 1);
		if ( $add_args ) $link = add_query_arg( $add_args, $link );
		$link .= $add_fragment;
		$page_links[] = '<a class="nav-link prev'. $class . $class_prev .'" href="' . esc_url( apply_filters( 'paginate_links', $link ) ) . '">'. $prev_text .'</a>';
	elseif ( $prev_next === 'disabled' && $current == 1 ) :
		$link = get_pagenum_link($current - 1);
		if ( $add_args ) $link = add_query_arg( $add_args, $link );
		$link .= $add_fragment;
		$page_links[] = '<a class="nav-link prev'. $class . $class_prev .' disabled" href="' . esc_url( apply_filters( 'paginate_links', $link ) ) . '">'. $prev_text .'</a>';
	endif;

	if ( $first_last && 1 < $current - $mid_size ) :
		$link = get_pagenum_link(1);
		if ( $add_args ) $link = add_query_arg( $add_args, $link );
		$link .= $add_fragment;
		$page_links[] = '<a class="nav-link first-page'. $class . $class_first .'" href="'. esc_url( apply_filters( 'paginate_links', $link ) ) .'">'. $first_text .'</a>';
		if ( $show_dots ) $page_links[] = '<span class="nav-link dots'. $class . $class_dots .'">'. $dots_text .'</span>';
	endif;

	for ( $n = 1; $n <= $total; $n++ ) :
		$n_display = number_format_i18n($n);
		if ( $n == $current ) :
			if ( $show_current ) :
				$page_links[] = '<span class="nav-link page-number current'. $class . $class_current .'">'. $n_display .'</span>';
			endif;
		else :
			if ( $show_all || ( $n >= $current - $mid_size && $n <= $current + $mid_size ) ) :
				$link = get_pagenum_link($n_display);
				if ( $add_args ) $link = add_query_arg( $add_args, $link );
				$link .= $add_fragment;
				$page_links[] = '<a class="nav-link page-number'. $class . $class_mid .'" href="'. esc_url( apply_filters( 'paginate_links', $link ) ) .'">'. $n_display .'</a>';
			endif;
		endif;
	endfor;

	if ( $first_last && $total > $current + $mid_size ) :
		if ( $show_dots ) $page_links[] = '<span class="dots nav-link'. $class . $class_dots .'">'. $dots_text .'</span>';
		$link = get_pagenum_link($total);
		if ( $add_args ) $link = add_query_arg( $add_args, $link );
		$link .= $add_fragment;
		$page_links[] = '<a class="nav-link last-page'. $class . $class_last .'" href="'. esc_url( apply_filters( 'paginate_links', $link ) ) .'">'. $last_text .'</a>';
	endif;

	if ( $prev_next && $current < $total ) :
		$link = get_pagenum_link($current + 1);
		if ( $add_args ) $link = add_query_arg( $add_args, $link );
		$link .= $add_fragment;
		$page_links[] = '<a class="nav-link next'. $class . $class_next .'" href="' . esc_url( apply_filters( 'paginate_links', $link ) ) . '">' . $next_text . '</a>';
	elseif ( $prev_next === 'disabled' && $current == $total ) :
		$link = get_pagenum_link($current + 1);
		if ( $add_args ) $link = add_query_arg( $add_args, $link );
		$link .= $add_fragment;
		$page_links[] = '<a class="nav-link next'. $class . $class_next .' disabled" href="' . esc_url( apply_filters( 'paginate_links', $link ) ) . '">' . $next_text . '</a>';
	endif;

	switch ( $type ) :
		case 'array' :
			return $page_links;
			break;
		case 'list' :
			return "<ul class='nav-link'>\n\t<li>". join( "</li>\n\t<li>", $page_links ) ."</li>\n</ul>\n";
			break;
		default :
			return join( "\n", $page_links );
			break;
	endswitch;
}


?>