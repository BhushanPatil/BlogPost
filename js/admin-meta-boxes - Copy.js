/*----------------------------------------------------------------------------------*/
/*	Display meta boxes for particular post format as needed
/*----------------------------------------------------------------------------------*/
jQuery(document).ready(function($) {

	// Our vars
	var metaBoxesStandard = $( '.meta-box-for-post-standard' ), metaBoxesStandardTrigger = 	$( '#post-format-0' ),
		metaBoxesAside 	= 	$( '.meta-box-for-post-aside' ),	metaBoxesAsideTrigger = 	$( '#post-format-aside' ),
		metaBoxesGallery = 	$( '.meta-box-for-post-gallery' ),	metaBoxesGalleryTrigger = 	$( '#post-format-gallery' ),
		metaBoxesLink = 	$( '.meta-box-for-post-link' ),		metaBoxesLinkTrigger = 		$( '#post-format-link' ),
		metaBoxesImage = 	$( '.meta-box-for-post-image' ),	metaBoxesImageTrigger = 	$( '#post-format-image' ),
		metaBoxesQuote = 	$( '.meta-box-for-post-quote' ),	metaBoxesQuoteTrigger = 	$( '#post-format-quote' ),
		metaBoxesStatus = 	$( '.meta-box-for-post-status' ),	metaBoxesStatusTrigger = 	$( '#post-format-status' ),
		metaBoxesVideo = 	$( '.meta-box-for-post-video' ),	metaBoxesVideoTrigger = 	$( '#post-format-video' ),
		metaBoxesAudio = 	$( '.meta-box-for-post-audio' ),	metaBoxesAudioTrigger = 	$( '#post-format-audio' ),
		metaBoxesChat = 	$( '.meta-box-for-post-chat' ),		metaBoxesChatTrigger = 		$( '#post-format-chat' );

	// First hide all meta boxes
	function hideAllMetaBoxes() {
		metaBoxesStandard.css( 'display', 'none' );
		metaBoxesAside.css( 'display', 'none' );
		metaBoxesGallery.css( 'display', 'none' );
		metaBoxesLink.css( 'display', 'none' );
		metaBoxesImage.css( 'display', 'none' );
		metaBoxesQuote.css( 'display', 'none' );
		metaBoxesStatus.css( 'display', 'none' );
		metaBoxesVideo.css( 'display', 'none' );
		metaBoxesAudio.css( 'display', 'none' );
		metaBoxesChat.css( 'display', 'none' );
	}
	hideAllMetaBoxes();

	// Display appropriate meta boxes on radio button change
	$( '#post-formats-select input' ).change(
		function() {
			hideAllMetaBoxes();
			if ( $(this).val() == '0' )				metaBoxesStandard.css( 'display', 'block' );		
			else if ( $(this).val() == 'aside' )	metaBoxesAside.css( 'display', 'block' );
			else if ( $(this).val() == 'gallery' )	metaBoxesGallery.css( 'display', 'block' );
			else if ( $(this).val() == 'link' )		metaBoxesLink.css( 'display', 'block' );
			else if ( $(this).val() == 'image' )	metaBoxesImage.css( 'display', 'block' );
			else if ( $(this).val() == 'quote' )	metaBoxesQuote.css( 'display', 'block' );
			else if ( $(this).val() == 'status' )	metaBoxesStatus.css( 'display', 'block' );
			else if ( $(this).val() == 'video' )	metaBoxesVideo.css( 'display', 'block' );
			else if ( $(this).val() == 'audio' )	metaBoxesAudio.css( 'display', 'block' );
			else if ( $(this).val() == 'chat' )		metaBoxesChat.css( 'display', 'block' );
		}
	);
	if ( metaBoxesStandardTrigger.is( ':checked' ) )	metaBoxesStandard.css( 'display', 'block' );
	if ( metaBoxesAsideTrigger.is( ':checked' ) )		metaBoxesAside.css( 'display', 'block' );
	if ( metaBoxesGalleryTrigger.is( ':checked' ) )		metaBoxesGallery.css( 'display', 'block' );
	if ( metaBoxesLinkTrigger.is( ':checked' ) )		metaBoxesLink.css( 'display', 'block' );
	if ( metaBoxesImageTrigger.is( ':checked' ) )		metaBoxesImage.css( 'display', 'block' );
	if ( metaBoxesQuoteTrigger.is( ':checked' ) )		metaBoxesQuote.css( 'display', 'block' );
	if ( metaBoxesStatusTrigger.is( ':checked' ) )		metaBoxesStatus.css( 'display', 'block' );
	if ( metaBoxesVideoTrigger.is( ':checked' ) )		metaBoxesVideo.css( 'display', 'block' );
	if ( metaBoxesAudioTrigger.is( ':checked' ) )		metaBoxesAudio.css( 'display', 'block' );
	if ( metaBoxesChatTrigger.is( ':checked' ) )		metaBoxesChat.css( 'display', 'block' );
});