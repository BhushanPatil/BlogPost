jQuery(function($){

	var ajaxUrl = loadMoreVars.ajaxUrl,
		loadMoreString = loadMoreVars.loadMoreString,
		loadingString = loadMoreVars.loadingString,
		noResultsString = loadMoreVars.noResultsString,
		loadMore = $('#load-more:not(.loading,.loaded)'),
		loading = false,
		loaded = false;

	var loadMoreContent = function() {

		if ( loading == true || loaded == true ) return false;

		loading = true;
		loadMore.addClass('loading').html(loadingString); // Update the message, Show that we're working.

		var page = 		parseInt( $('#page').val() ) + 1, // The number of the next page to load (/page/x/)
			maxPage = 	$('#max-page').val(), // The maximum number of pages the current query can return
			data = {
				action: 				'load_more_query',
				loadMore_page: 			page,
				loadMore_post_type: 	$('#post-type').val(),
				loadMore_post_format: 	$('#post-format').val(),
				loadMore_archive_type: 	$('#archive-type').val(),
				loadMore_archive_id: 	$('#archive-id').val(),
				loadMore_archive_month: $('#archive-month').val(),
				loadMore_archive_year: 	$('#archive-year').val(),
				loadMore_archive_m: 	$('#archive-m').val(),
				loadMore_author: 		$('#author').val(),
				loadMore_s: 			$('#s').val(),
				loadMore_nonce: 		$('#nonce').val()
			};

		$.post( ajaxUrl, data, function( response ) {
			content = $(response);
			content.addClass('animated fadeIn');
			$('#post-entries').append(content);

			// Are there more pages to load?
			if ( page < maxPage ) {
				$('#page').val(page);
				loadMore.removeClass('loading').html(loadMoreString);
				loading = false;
			}
			else {
				loadMore.addClass('loaded').html(noResultsString);
				loaded = true;
			}
		});
	}

	$(document).ready( function() {
		loadMore.click( function(e) {
			e.preventDefault();
			loadMoreContent();
		});
	});

	$(window).load( function() {
		if ( loadMore.hasClass('scroll-auto-load') ) {
			$(window).scroll(function() {
				if ( ( $('#site-header').height() + $('#primary-content').height() ) < ( $(window).height() + $(window).scrollTop() ) )
					loadMoreContent();
			});
		}
	});

});