jQuery(function($){

	$(window).load( function() {

		var ajaxUrl = loadMoreVars.ajaxUrl,
			loadMoreString = loadMoreVars.loadMoreString,
			loadingString = loadMoreVars.loadingString,
			noResultsString = loadMoreVars.noResultsString,
			loadMore = $('#load-more:not(.loading,.loaded)'),
			anchor = loadMore.children('a'),
			loading = false,
			loaded = false;

		anchor.click( function(e) { e.preventDefault(); } );

		loadMore.click( function() {

			if ( loading == true || loaded == true ) return false;

			loading = true;
			loadMore.addClass('loading');
			anchor.html(loadingString); // Update the message, Show that we're working.

			var page = 		parseInt( $('#page').val() ) + 1, // The number of the next page to load (/page/x/)
				maxPage = 	$('#max-page').val(), // The maximum number of pages the current query can return
				data = {
					action: 				'load_more_query',
					loadMore_page: 			page,
					loadMore_post_type: 	$('#post-type').val(),
					loadMore_post_format: 	$('#post-format').val(),
					loadMore_archive_type: 	$('#archive-type').val(),
					loadMore_archive_id: 	$('#archive-id').val(),
					loadMore_archive_month: $('#archive-month').val(),
					loadMore_archive_year: 	$('#archive-year').val(),
					loadMore_archive_m: 	$('#archive-m').val(),
					loadMore_author: 		$('#author').val(),
					loadMore_s: 			$('#s').val(),
					loadMore_nonce: 		$('#nonce').val()
				};

			$.post( ajaxUrl, data, function( response ) {
				content = $(response);
				content.addClass('animated fadeIn');
				$('#post-entries').append(content);

				// Are there more posts to load?
				if ( page < maxPage ) {
					$('#page').val(page);
					anchor.html(loadMoreString);
					loadMore.removeClass('loading');
					loading = false;
				}
				else {
					anchor.html(noResultsString);
					loadMore.addClass('loaded');
					loaded = true;
				}
			});
			
		});
		
		if ( loadMore.hasClass('scroll-auto-load') ) {
			$(window).scroll(function() {
				if ( ( $('#site-header').height() + $('#primary-content').height() ) < ( $(window).height() + $(window).scrollTop() ) )
					loadMore.trigger('click');
			});
		}

	});
});