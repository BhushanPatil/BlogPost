var ww = document.body.clientWidth,
	$menu = $("#main-menu"),
	$mobileNavToggle = $("#toggle-nav a"),
	$taglineSocialLinks = $("#tagline-social-links"),
	$siteTagline = $("#tagline"),
	$socialLinks = $("#social-links"),
	$searchBox = $("#navbar .search-form"),
	$sidebar = $("#sidebar"),
	$backToTopLink = $("#back-to-top-link");

$(window).bind('resize orientationchange', function() {
	ww = document.body.clientWidth;
	adjustLayout();
});

$(document).ready(function() {
	// Adjust layout for different size devices
	adjustLayout();

	// Add Menu Icons
	$("#main-menu .menu-item.fa").each(function() {
		var $menuItem = $(this),
			classList = $menuItem.attr('class').split(/\s+/);
		$.each( classList, function( index, item ) {
			if ( item.match(/^fa-([a-z0-9_-]+)$/) )
				$menuItem.removeClass('fa '+ item).children('a').prepend('<i class="fa '+ item +'"></i> ');
		});
	});

	// Add Sub Menu Indicator Icons 
	$("#main-menu .menu-item-has-children > a").each(function() {
		$(this).after('<span class="toggle-sub-menu fa fa-angle-down"></span>');
	});

	// Mobile nav toggle
	$mobileNavToggle.click(function(e) {
		e.preventDefault();
		$(this).toggleClass("active").siblings().removeClass('active');
		adjustMobileNav();
	});

	// Scroll Back to Top Functions
	$(window).scroll(function () {
		if ( $(this).scrollTop() > 500 ) $backToTopLink.fadeIn(200);
		else $backToTopLink.fadeOut(200);
	});
	$backToTopLink.click(function(e) {
		e.preventDefault();
		$("body,html").animate({ scrollTop: 0 }, 800);
		return false;
	});

});

var adjustLayout = function() {
	if (ww >= 768) {
		$menu.addClass('desktop').removeClass('mobile');
		$("#main-menu .menu-item:has(.sub-menu)").doubleTapToGo();
		$taglineSocialLinks.insertAfter("#logo").removeAttr("style");
		$("#main-menu,#navbar .search-form,#main-menu .menu-item-has-children > .sub-menu,#tagline,#social-links").removeAttr("style");
		$(".toggle-sub-menu").removeClass("active");
		if ( $sidebar.hasClass("moved") ) {
			if ( $("body").hasClass("right-sidebar") ) $sidebar.insertAfter("#primary-content");
			else if ( $("body").hasClass("left-sidebar") ) $sidebar.insertBefore("#primary-content");
		}
	}
	else if (ww < 768) {
		$menu.addClass('mobile').removeClass('desktop');
		$taglineSocialLinks.insertAfter("#navbar .search-form").removeAttr("style");
		$sidebar.insertAfter("#primary-content").addClass('moved');
		adjustMobileNav();
	}
}

var adjustMobileNav = function() {
	if ($mobileNavToggle.hasClass("toggle-menu active")) {
		$menu.slideDown();
		$searchBox.slideUp();
		$taglineSocialLinks.slideUp();
		$("#main-menu.mobile .menu-item-has-children > .toggle-sub-menu").unbind('click').bind('click', function(e) {
			$(this).next("ul").slideToggle().parent("li.menu-item-has-children").siblings("li.menu-item-has-children").children("ul").slideUp();
			$(this).toggleClass("active").parent("li.menu-item-has-children").siblings("li.menu-item-has-children").children(".toggle-sub-menu").removeClass("active");
		});
	}
	else if ($mobileNavToggle.hasClass("toggle-search active")) {
		$menu.slideUp();
		$searchBox.slideDown();
		$taglineSocialLinks.slideUp();
	}
	else {
		$menu.slideUp();
		$searchBox.slideUp();
		$taglineSocialLinks.slideDown();
	}
}