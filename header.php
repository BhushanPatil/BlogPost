<?php
/**
 * The Header for our theme.
 * Displays all of the <head> section and everything up till <div id="main">
 */
?>
<!DOCTYPE html>
<!--[if lt IE 7 ]><html class="ie ie6" <?php language_attributes(); ?>><![endif]-->
<!--[if IE 7 ]><html class="ie ie7" <?php language_attributes(); ?>><![endif]-->
<!--[if IE 8 ]><html class="ie ie8" <?php language_attributes(); ?>><![endif]-->
<!--[if (gte IE 9)|!(IE)]><!--><html <?php language_attributes(); ?>><!--<![endif]-->
<head>

	<!-- Basic Page Needs ================================== -->
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<title><?php wp_title( '-', TRUE, 'right' ); ?></title>
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">

	<!-- Mobile Specific Metas ============================= -->

	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<?php $favicon = get_theme_mod( 'site_favicon' ); if ( $favicon != '' ) { ?>
	<!-- Favicon =========================================== -->
	<link rel="shortcut icon" href="<?php echo $favicon; ?>" />
	<?php } ?>

	<!-- HTML5 for older browsers ========================== -->
	<!--[if lt IE 9]><script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
	
	<!-- WP Head ============================================ -->
	<?php wp_head(); ?>

</head>

<body <?php body_class(); ?>>

	<!--#################### Site Header ####################-->
	<header id="site-header" role="banner">

		<div id="topbar">
			<div class="grid-container">
				<div class="col-12">
					<?php if ( get_theme_mod( 'site_logo' ) ) : ?>
					<div id="logo" class="image">
						<h1>
							<a href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" rel="home">
								<img src='<?php echo esc_url( get_theme_mod( 'site_logo' ) ); ?>' alt='<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>'/>
							</a>
						</h1>
					</div><!--END #logo -->
					<?php else : ?>

					<div id="logo" class="text">
						<h1>
							<a href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" rel="home"><?php bloginfo( 'name', 'display' ); ?></a>
						</h1>
					</div><!--END #logo -->
					<?php endif; ?>

					<div id="tagline-social-links">
						<?php if( get_theme_mod( 'show_tagline', TRUE ) == TRUE && !empty( get_bloginfo( 'description' ) ) ) : ?>
						<div id="tagline"><h2><?php bloginfo( 'description' ); ?></h2></div><!--END .tagline -->
						<?php endif; ?>
						<?php BP_social_links(); ?>
					</div><!--END #tagline-social-links -->

				</div><!--END .col-12 -->
			</div><!--END .grid-container -->
		</div><!--END #topbar -->
		
		<nav id="navbar">
			<div class="grid-container">

				<div class="col-9">
					<div id="main-menu" class="desktop">
						<?php wp_nav_menu( array(
							'theme_location' 	=> 'main',
							'container' 		=> false,
							'fallback_cb' 		=> 'BP_page_menu'
						)); ?>

					</div><!--END .main-menu -->

					<div id="toggle-nav">
						<a class="toggle-search" href="#"><i class="fa fa-search"></i></a>&nbsp;<a class="toggle-menu" href="#"><i class="fa fa-bars"></i></a>
					</div><!--END #toggle-nav -->

				</div><!--END .col-9 -->

				<div class="col-3">
					<?php get_search_form(); ?>

				</div><!--END .col-3 -->

			</div><!--END .grid-container -->
		</nav><!--END .navbar -->
		
	</header><!--END #site-header -->


	<!--#################### Site Main conetent ####################-->
	<main id="site-content" role="main">
		<?php
		
		// Homepage Slider & Carousel
        // if ( is_home() )
        	// get_template_part( 'content', 'homepage-slider' );
       	if ( is_author() )
        	get_template_part( 'author-bio-header' );
        else
        	BP_breadcrumbs();
        
        ?>