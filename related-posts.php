<?php
/**
 * Relatest Posts from the same Category
 */

$categories = get_the_category( $post->ID );
$first_cat = $categories[0]->cat_ID;
$post_number = 4;
$args = array(
	'category__in' => array( $first_cat ),
	'post__not_in' => array( $post->ID ),
	'posts_per_page' => $post_number
);
$related_posts = get_posts( $args );
if( $related_posts ) {
?>
<div id="related-posts" class="post-col-<?php echo $post_number; ?>">
	<div class="title-text"><h3>You may also like</h3></div>
	<ul>
		<?php foreach( $related_posts as $post ): setup_postdata( $post ); ?>
		<li>
			<article <?php post_class(); ?>>
				<?php BP_entry_thumbnail( array( 'size'=>array(180,130), 'no_image'=>TRUE, 'single'=>FALSE ) ); ?>
				<div class="entry-info">
					<?php BP_entry_title(  array( 'heading'=>'h4', 'show_id'=>TRUE, 'single'=>FALSE ) ); ?>
					<?php BP_entry_meta( array( 'meta'=>'%date%', 'date_format'=>'M j, Y', 'show_icon'=>FALSE ) ); ?>
				</div>
			</article>
		</li>
		<?php endforeach; ?>

	</ul>
</div>
<?php
}
wp_reset_postdata();
?>