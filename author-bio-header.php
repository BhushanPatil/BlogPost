<header id="content-header">
	<div class="grid-container">
		<div class="col-8">
			<div class="author-bio">

				<div class="author-avatar">
					<?php
					if ( !empty( get_the_author_meta('user_url') ) )
						echo '<a class="url fn n" href="'. get_author_posts_url( get_the_author_meta( 'ID' ) ) .'" rel="author">'. get_avatar( get_the_author_meta( 'user_email' ), NULL, NULL, sprintf( __( '%s\'s Avatar', THEME_TEXTDOMAIN ), get_the_author() ) ) .'</a>';
					else
						echo get_avatar( get_the_author_meta( 'user_email' ), NULL, NULL, sprintf( __( '%s\'s Avatar', THEME_TEXTDOMAIN ), get_the_author() ) );
					?>
				</div><!-- .author-avatar -->

				<div class="author-info">

					<div class="author-name">
						<h1><?php the_author(); ?></h1>
					</div><!-- .author-name -->

					<?php if ( !empty( get_the_author_meta('description') ) ) : ?>
					<div class="author-bio-text">
						<?php the_author_meta( 'description' ); ?>
					</div><!-- .author-bio -->
					<?php endif; ?>

					<div class="author-meta">

						<span class="author-post-count">
							<?php echo '<i class="fa fa-file-text"></i> '. sprintf( _n( '1 Post', '%s Posts', $wp_query->found_posts, THEME_TEXTDOMAIN ), $wp_query->found_posts ); ?>
						</span><!-- .author-post-count -->

						<?php if ( !empty( get_the_author_meta('user_url') ) ) : ?>
						<span class="author-website">
							<?php printf(
								'<i class="fa fa-external-link-square"></i> '. __( 'Website: %s', THEME_TEXTDOMAIN ),
								'<a href="'. get_the_author_meta('user_url') .'" title="'. sprintf( __( 'Visit %s\'s Website', THEME_TEXTDOMAIN ), get_the_author() ) .'" target="_blank">'. get_the_author_meta('user_url') .'</a>'
							); ?>
						</span><!-- .author-website -->
						<?php endif; ?>
						
					</div><!-- .author-meta -->

					<?php BP_author_social_links(); ?>
					
				</div><!-- .author-info -->
				
			</div><!-- .author-bio -->
		</div><!-- .col-8 -->
	</div><!-- .grid-container -->
</header><!-- #content-header -->