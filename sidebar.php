<?php
/**
 * The sidebar containing the secondary widget area, displays on posts and pages.
 * If no active widgets in this sidebar, it will be hidden completely.
 */
?>

<?php if ( is_active_sidebar( 'sidebar-1' ) ) : ?>

	<div id="sidebar" class="col-4" role="complementary">

		<?php dynamic_sidebar( 'sidebar-1' ); ?>

	</div><!--END .sidebar -->

<?php endif; ?>