<?php


/**
 * Register widget
 */
add_action( 'widgets_init', create_function( '', 'return register_widget("BP_Widget_Post_Slider");' ) );


/**
 * Widget class
 */
class BP_Widget_Post_Slider extends WP_Widget {

	/**
	 * Sets up the widgets name etc
	 */
	public function __construct() {
		parent::__construct(
			'post_slider', // Base ID
			__( 'Post Slider', THEME_TEXTDOMAIN ), // Widget Name
			array( 'description' => __( 'Shows post slider.', THEME_TEXTDOMAIN ), ) // Widget description on admin
		);
	}

	/**
	 * Outputs the content of the widget
	 *
	 * @param array $args
	 * @param array $instance
	 */
	public function widget( $args, $instance ) {
		extract($args);
	  	$title 			= apply_filters( 'widget_title', $instance['title'] );
	  	$category 		= isset( $instance['category'] ) ? $instance['category'] : 'all-cats';
	  	$order 			= isset( $instance['order'] ) ? $instance['order'] : 'none';
	  	$number 		= isset( $instance['number'] ) ? $instance['number'] : 5;
	  	$offset 		= isset( $instance['offset'] ) ? $instance['offset'] : 0;
	  	$show_thumb 	= isset( $instance['show_thumb'] ) ? $instance['show_thumb'] : TRUE;
	  	$show_excerpt 	= isset( $instance['show_excerpt'] ) ? $instance['show_excerpt'] : TRUE;
	  	$show_date 		= isset( $instance['show_date'] ) ? $instance['show_date'] : TRUE;
	  	$slideshow 		= $instance['slideshow'] ? 'true' : 'false';
	  	$animation 		= isset( $instance['animation'] ) ? $instance['animation'] : 'fade';

	  	$r = new WP_Query( apply_filters(
	  		'widget_posts_args',
	  		array(
	  			'post_type' =>'post',
	  			'offset' => $offset,
	  			'posts_per_page' => $number,
	  			'no_found_rows' => true,
	  			'post_status' => 'publish',
	  			'ignore_sticky_posts' => true,
	  			'orderby' => $order,
	  			'cat' => $category,
	  			'meta_query' => array(array('key' => '_thumbnail_id')),
	  			'post__not_in' => array( get_the_ID() ) 
	  		)
	  	) );

	  	if ( $r->have_posts() ) {
	  		$output = '';
	  		$output .= $args['before_widget'];
	  		if ( $title ) $output .= $args['before_title'] . $title . $args['after_title'];

	  		$class = 'slider';
	  		if ( $show_date ) $class .= ' with-date';
	  		if ( $show_thumb ) $class .= ' with-thumbnail';
	  		$output .= '<div class="'. $class .'">';
	  		$output .= '<ul class="slides">';
	  		while ( $r->have_posts() ) : $r->the_post();
	  			$output .= '<li class="entry">';
	  			if ( $show_thumb ) $output .= BP_entry_thumbnail( array( 'size'=>array(320,180), 'single'=>FALSE, 'echo'=>FALSE ) );
	  			$output .= BP_entry_title( array( 'single'=>FALSE, 'echo'=>FALSE ) );
	  			if ( $show_excerpt ) $output .= BP_entry_content( array( 'excerpt'=>TRUE, 'echo'=>FALSE ) );
	  			if ( $show_date ) $output .= BP_entry_meta( array( 'meta'=>'%date%', 'date_format'=>'M j, Y', 'echo'=>FALSE ) );
	  			$output .= '</li>';
	  		endwhile;
	  		$output .= '</ul>';
	  		$output .= '</div>';

	  		// wp_enqueue_script('flexslider');
	  		$output .= '<script type="text/javascript">
	  		jQuery(document).ready(function($){
	  			$("#'. $this->id .' .slider").flexslider({
					namespace: "",
					selector: ".slides > li",
					animation: "'. $animation .'",
					smoothHeight: true,
					slideshow: '. $slideshow .',
					controlNav: false,
					directionNav: true,
					prevText: "<i class=\"fa fa-angle-left\"></i>",
					nextText: "<i class=\"fa fa-angle-right\"></i>"
				});
			});</script>';

	  		/* Reset the global $the_post as this query will have stomped on it */
	  		wp_reset_postdata();

	  		$output .= $args['after_widget'];

	  		echo $output;
		}
	}

	/**
	 * Ouputs the options form on admin
	 *
	 * @param array $instance The widget options
	 */
	public function form( $instance ) {
		$title 			= isset( $instance['title'] ) ? esc_attr( $instance['title'] ) : '';
		$category 		= isset( $instance['category'] ) ? $instance['category'] : 'all-cats';
		$order 			= isset( $instance['order'] ) ? $instance['order'] : 'none';
		$number 		= isset( $instance['number'] ) ? $instance['number'] : 5;
		$offset 		= isset( $instance['offset'] ) ? $instance['offset'] : 0;
		$show_thumb 	= isset( $instance['show_thumb'] ) ? $instance['show_thumb'] : TRUE;
		$show_excerpt 	= isset( $instance['show_excerpt'] ) ? $instance['show_excerpt'] : TRUE;
		$show_date 		= isset( $instance['show_date'] ) ? $instance['show_date'] : TRUE;
		$slideshow 		= isset( $instance['slideshow'] ) ? $instance['slideshow'] : FALSE;
		$animation 		= isset( $instance['animation'] ) ? $instance['animation'] : 'fade';
		?><p>
			<label for="<?php echo $this->get_field_id( 'title'); ?>"><?php _e( 'Title:', THEME_TEXTDOMAIN ); ?></label>
			<input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo $title; ?>" />
		</p><p>
			<label for="<?php echo $this->get_field_id('category'); ?>"><?php _e( 'Category:', THEME_TEXTDOMAIN ) ?></label>
			<select class="widefat" id="<?php echo $this->get_field_id('category'); ?>" name="<?php echo $this->get_field_name('category'); ?>">
				<option value="all-cats" <?php selected( 'all-cats', $category ) ?>><?php _e( 'All', THEME_TEXTDOMAIN ); ?></option>
				<?php $cat_terms = get_terms( 'category', array( 'hide_empty' => '1' ) );
				foreach ( $cat_terms as $cat ) echo '<option value="'. $cat->term_id .'" '. selected( $cat->term_id, $category ) .'>'. $cat->name .'</option>'; ?>
			</select>
		</p><p>
			<label for="<?php echo $this->get_field_id('order'); ?>"><?php _e( 'Order by:', THEME_TEXTDOMAIN ) ?></label>
			<select class="widefat" id="<?php echo $this->get_field_id('order'); ?>" name="<?php echo $this->get_field_name('order'); ?>">
				<option value="none" <?php selected('none', $order) ?>><?php _e( 'None', THEME_TEXTDOMAIN ) ?></option>
				<option value="rand" <?php selected('rand', $order) ?>><?php _e( 'Random', THEME_TEXTDOMAIN ) ?></option>
				<option value="ID" <?php selected('ID', $order) ?>><?php _e( 'Post id', THEME_TEXTDOMAIN ) ?></option>
				<option value="author" <?php selected('author', $order) ?>><?php _e( 'Author', THEME_TEXTDOMAIN ) ?></option>
				<option value="title" <?php selected('title', $order) ?>><?php _e( 'Title', THEME_TEXTDOMAIN ) ?></option>
				<option value="name" <?php selected('name', $order) ?>><?php _e( 'Post name (post slug)', THEME_TEXTDOMAIN ) ?></option>
				<option value="date" <?php selected('date', $order) ?>><?php _e( 'Date', THEME_TEXTDOMAIN ) ?></option>
				<option value="modified" <?php selected('modified', $order) ?>><?php _e( 'Modified date', THEME_TEXTDOMAIN ) ?></option>
				<option value="parent" <?php selected('parent', $order) ?>><?php _e( 'Post parent id', THEME_TEXTDOMAIN ) ?></option>
				<option value="comment_count" <?php selected('comment_count', $order) ?>><?php _e( 'Number of comments', THEME_TEXTDOMAIN ) ?></option>
			</select>
		</p><p>
			<label for="<?php echo $this->get_field_id( 'number' ); ?>"><?php _e( 'Number of posts to show:', THEME_TEXTDOMAIN ); ?></label>
			<input id="<?php echo $this->get_field_id( 'number' ); ?>" name="<?php echo $this->get_field_name( 'number' ); ?>" type="text" value="<?php echo $number; ?>" size="3" />
		</p><p>
			<label for="<?php echo $this->get_field_id( 'offset' ); ?>"><?php _e( 'Offset, number of posts to skip:', THEME_TEXTDOMAIN ); ?></label>
			<input id="<?php echo $this->get_field_id( 'offset' ); ?>" name="<?php echo $this->get_field_name( 'offset' ); ?>" type="text" value="<?php echo $offset; ?>" size="3" />
		</p><p>
			<input class="checkbox" type="checkbox" <?php checked( $show_thumb ); ?> id="<?php echo $this->get_field_id( 'show_thumb' ); ?>" name="<?php echo $this->get_field_name( 'show_thumb' ); ?>" />
			<label for="<?php echo $this->get_field_id( 'show_thumb' ); ?>"><?php _e( 'Show Thumbnail', THEME_TEXTDOMAIN ); ?></label>
		</p><p>
			<input class="checkbox" type="checkbox" <?php checked( $show_excerpt ); ?> id="<?php echo $this->get_field_id( 'show_excerpt' ); ?>" name="<?php echo $this->get_field_name( 'show_excerpt' ); ?>" />
			<label for="<?php echo $this->get_field_id( 'show_excerpt' ); ?>"><?php _e( 'Show post excerpt', THEME_TEXTDOMAIN ); ?></label>
		</p><p>
			<input class="checkbox" type="checkbox" <?php checked( $show_date ); ?> id="<?php echo $this->get_field_id( 'show_date' ); ?>" name="<?php echo $this->get_field_name( 'show_date' ); ?>" />
			<label for="<?php echo $this->get_field_id( 'show_date' ); ?>"><?php _e( 'Show Post Date', THEME_TEXTDOMAIN ); ?></label>
		</p><p>
			<input class="checkbox" type="checkbox" <?php checked( $slideshow ); ?> id="<?php echo $this->get_field_id( 'slideshow' ); ?>" name="<?php echo $this->get_field_name( 'slideshow' ); ?>" />
			<label for="<?php echo $this->get_field_id( 'slideshow' ); ?>"><?php _e( 'Slideshow', THEME_TEXTDOMAIN ); ?></label>
		</p><p>
			<label for="<?php echo $this->get_field_id('animation'); ?>"><?php _e( 'Animation:', THEME_TEXTDOMAIN ) ?></label>
			<select class="widefat" id="<?php echo $this->get_field_id('animation'); ?>" name="<?php echo $this->get_field_name('animation'); ?>">
				<option value="fade" <?php selected('fade', $animation) ?>><?php _e( 'Fade', THEME_TEXTDOMAIN ) ?></option>
				<option value="slide" <?php selected('slide', $animation) ?>><?php _e( 'Slide', THEME_TEXTDOMAIN ) ?></option>
			</select>
		</p><?php
	}

	/**
	 * Processing and Sanitize widget form values as they are saved
	 *
	 * @param array $new_instance The new options
	 * @param array $old_instance The previous options
	 */
	public function update( $new_instance, $old_instance ) {
		$instance = $old_instance;

		$instance['title'] = strip_tags( $new_instance['title'] );

		$instance['category'] = $new_instance['category'];

		$instance['order'] = $new_instance['order'];

		$instance['number'] = absint( $new_instance['number'] );
		if ( $instance['number'] < 1 ) $instance['number'] = 5;

		$instance['offset'] = absint( $new_instance['offset'] );

		$instance['show_thumb'] = (bool) $new_instance['show_thumb'];
		$instance['show_excerpt'] = (bool) $new_instance['show_excerpt'];
		$instance['show_date'] = (bool) $new_instance['show_date'];
		$instance['slideshow'] = (bool) $new_instance['slideshow'];

		$instance['animation'] = $new_instance['animation'];

		return $instance;
	}
	
}


?>