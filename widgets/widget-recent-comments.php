<?php


/**
 * Un-register WordPress default widget
 */
add_action( 'widgets_init', create_function( '', 'unregister_widget("WP_Widget_Recent_Comments");' ) );


/**
 * Register widget
 */
add_action( 'widgets_init', create_function( '', 'return register_widget("BP_Widget_Recent_Comments");' ) );


/**
 * Widget class
 */
class BP_Widget_Recent_Comments extends WP_Widget {


	/**
	 * Sets up the widgets name etc
	 */
	public function __construct() {
		parent::__construct(
			'recent-comments', // Base ID
			__( 'Recent Comments' ), // Widget Name
			array( 'classname' => 'widget_recent_comments', 'description' => __( 'Your site&#8217;s most recent comments.' ), ) // Widget description on admin
		);

		add_action( 'comment_post', array($this, 'flush_widget_cache') );
		add_action( 'edit_comment', array($this, 'flush_widget_cache') );
		add_action( 'transition_comment_status', array($this, 'flush_widget_cache') );
	}

	/**
	 * Outputs the content of the widget
	 *
	 * @param array $args
	 * @param array $instance
	 */
	public function widget( $args, $instance ) {
		global $comments, $comment;

		$cache = wp_cache_get('widget_recent_comments', 'widget');

		if ( !is_array( $cache ) ) $cache = array();
		if ( !isset( $args['widget_id'] ) ) $args['widget_id'] = $this->id;
		if ( isset( $cache[ $args['widget_id'] ] ) ) {
			echo $cache[ $args['widget_id'] ];
			return;
		}


		extract($args);
		$output = '';

	  	$title 				= apply_filters( 'widget_title', $instance['title'] );
	  	$description 		= apply_filters( 'widget_text', empty( $instance['description'] ) ? '' : $instance['description'], $instance );
	  	$number 			= isset( $instance['number'] ) ? absint( $instance['number'] ) : 5;
	  	$show_date 			= isset( $instance['show_date'] ) ? (bool) $instance['show_date'] : TRUE;
	  	$show_author_name 	= isset( $instance['show_author_name'] ) ? (bool) $instance['show_author_name'] : TRUE;
	  	$show_thumb 		= isset( $instance['show_thumb'] ) ? (bool) $instance['show_thumb'] : TRUE;

	  	$comments = get_comments( apply_filters( 'widget_comments_args', array( 'number' => $number, 'status' => 'approve', 'post_status' => 'publish' ) ) );
	  	$output .= $args['before_widget'];
	  	if ( $title ) $output .= $args['before_title'] . $title . $args['after_title'];
	  	if ( !empty($description) ) $output .= '<p class="description">'. $description .'</p>';

	  	if ( $comments ) {

	  		$classes = '';
	  		if ( $show_date ) $classes .= ' with-date';
	  		if ( $show_author_name ) $classes .= ' with-author-name';
	  		if ( $show_thumb ) $classes .= ' with-thumbnail';
	  		$output .= '<ul class="recent-comments'. $classes .'">';

	  		// Prime cache for associated posts. (Prime post term cache if we need it for permalinks.)
	  		$post_ids = array_unique( wp_list_pluck( $comments, 'comment_post_ID' ) );
	  		_prime_post_caches( $post_ids, strpos( get_option( 'permalink_structure' ), '%category%' ), FALSE );

	  		foreach ( (array) $comments as $comment) {
	  			$output .= '<li class="comment">';
	  			if ( $show_thumb ) $output .= '<figure class="comment-author-avatar"><a href="'. esc_url( get_comment_link( $comment->comment_ID ) ) .'">'. get_avatar( $comment->comment_author_email, 70 ) .'</a></figure>';
	  			$output .= '<p class="comment-content">';
	  			if ( $show_author_name ) $output .= '<span class="comment-author"><a href="'. esc_url( get_comment_link( $comment->comment_ID ) ) .'"><cite>'. get_comment_author() .'</cite></a>:&nbsp;</span>';
	  			$output .= trim( mb_substr( strip_tags( $comment->comment_content ), 0, 70 ) );
	  			if ( $show_date ) $output .= '<span class="comment-date"><time class="datetime" datetime="'. date( 'c', strtotime( $comment->comment_date ) ) .'">'. date( 'M j, Y', strtotime( $comment->comment_date ) ) .'</time></span>';
	  			$output .= '</p>';
	  			$output .= '</li>';
	  		}

	  		$output .= '</ul>';
	  	}

	  	$output .= $args['after_widget'];

	  	echo $output;

	  	$cache[$args['widget_id']] = $output;
	  	wp_cache_set('widget_recent_comments', $cache, 'widget');
	}

	/**
	 * Ouputs the options form on admin
	 *
	 * @param array $instance The widget options
	 */
	public function form( $instance ) {
		$title 				= isset( $instance['title'] ) ? esc_attr( $instance['title'] ) : '';
		$description 		= esc_textarea( $instance['description'] );
		$number 			= isset( $instance['number'] ) ? absint( $instance['number'] ) : 5;
		$show_date 			= isset( $instance['show_date'] ) ? (bool) $instance['show_date'] : TRUE;
		$show_author_name 	= isset( $instance['show_author_name'] ) ? (bool) $instance['show_author_name'] : TRUE;
		$show_thumb 		= isset( $instance['show_thumb'] ) ? (bool) $instance['show_thumb'] : TRUE;
		?><p>
			<label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Title:', THEME_TEXTDOMAIN ); ?></label>
			<input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo $title; ?>" />
		</p>
		<p>
			<label for="<?php echo $this->get_field_id( 'description' ); ?>"><?php _e( 'Description:', THEME_TEXTDOMAIN ); ?></label>
			<textarea class="widefat" rows="3" cols="20" id="<?php echo $this->get_field_id( 'description' ); ?>" name="<?php echo $this->get_field_name( 'description' ); ?>"><?php echo $description; ?></textarea>
		</p>
		<p>
			<label for="<?php echo $this->get_field_id( 'number' ); ?>"><?php _e( 'Number of posts to show:', THEME_TEXTDOMAIN ); ?></label>
			<input id="<?php echo $this->get_field_id( 'number' ); ?>" name="<?php echo $this->get_field_name( 'number' ); ?>" type="text" value="<?php echo $number; ?>" size="3" />
		</p>
		<p>
			<input class="checkbox" type="checkbox" <?php checked( $show_date ); ?> id="<?php echo $this->get_field_id( 'show_date' ); ?>" name="<?php echo $this->get_field_name( 'show_date' ); ?>" />
			<label for="<?php echo $this->get_field_id( 'show_date' ); ?>"><?php _e( 'Show Comment Date', THEME_TEXTDOMAIN ); ?></label>
		</p>
		<p>
			<input class="checkbox" type="checkbox" <?php checked( $show_author_name ); ?> id="<?php echo $this->get_field_id( 'show_author_name' ); ?>" name="<?php echo $this->get_field_name( 'show_author_name' ); ?>" />
			<label for="<?php echo $this->get_field_id( 'show_author_name' ); ?>"><?php _e( 'Show Author Name', THEME_TEXTDOMAIN ); ?></label>
		</p>
		<p>
			<input class="checkbox" type="checkbox" <?php checked( $show_thumb ); ?> id="<?php echo $this->get_field_id( 'show_thumb' ); ?>" name="<?php echo $this->get_field_name( 'show_thumb' ); ?>" />
			<label for="<?php echo $this->get_field_id( 'show_thumb' ); ?>"><?php _e( 'Show Thumbnail', THEME_TEXTDOMAIN ); ?></label>
		</p><?php
	}

	/**
	 * Processing and Sanitize widget form values as they are saved
	 *
	 * @param array $new_instance The new options
	 * @param array $old_instance The previous options
	 */
	public function update( $new_instance, $old_instance ) {
		$instance = $old_instance;
		
		$instance['title'] = strip_tags( $new_instance['title'] );
		
		if ( current_user_can( 'unfiltered_html' ) ) $instance['description'] =  $new_instance['description'];
		else $instance['description'] = stripslashes( wp_filter_post_kses( addslashes($new_instance['description']) ) ); // wp_filter_post_kses() expects slashed

		$instance['number'] = absint($new_instance['number']);
		if ( $instance['number'] < 1 ) $instance['number'] = 8;
		else if ( $instance['number'] > 10 ) $instance['number'] = 10;

		$instance['show_date'] = (bool) $new_instance['show_date'];
		$instance['show_author_name'] = (bool) $new_instance['show_author_name'];
		$instance['show_thumb'] = (bool) $new_instance['show_thumb'];

		$this->flush_widget_cache();
		$alloptions = wp_cache_get( 'alloptions', 'options' );
		if ( isset($alloptions['widget_recent_entries']) )
			delete_option('widget_recent_entries');
		
		return $instance;
	}

	/**
	 * Disambiguates widget cache
	 */
	function flush_widget_cache() {
		wp_cache_delete( 'widget_recent_comments', 'widget' );
	}
}


?>