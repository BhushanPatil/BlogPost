<?php


/**
 * Register widget
 */
add_action( 'widgets_init', create_function( '', 'return register_widget("BP_Widget_Author_Bio");' ) );


/**
 * Widget class
 */
class BP_Widget_Author_Bio extends WP_Widget {

	/**
	 * Sets up the widgets name etc
	 */
	public function __construct() {
		parent::__construct(
			'author_bio', // Base ID
			__( 'Author Bio' ), // Widget Name
			array( 'classname' => 'widget_author_bio', 'description' => __( 'Shows author bio if a single post is being displayed.' ), ) // Widget description on admin
		);
	}

	/**
	 * Outputs the content of the widget
	 *
	 * @param array $args
	 * @param array $instance
	 */
	public function widget( $args, $instance ) {
		if ( is_single() && !is_page() && !is_attachment() ) {
			extract($args);
			$title 				= apply_filters( 'widget_title', $instance['title'] );
			$show_avatar 		= isset( $instance['show_avatar'] ) ? (bool) $instance['show_avatar'] : true;
			$collapsed_bio 		= isset( $instance['collapsed_bio'] ) ? (bool) $instance['collapsed_bio'] : true;
			$author_bio_text 	= get_the_author_meta('description');
			if ( strlen( $author_bio_text ) < 210 ) $collapsed_bio = false;
			$classes = "";
			if ( $show_avatar === true ) $classes .= " with-avatar";
			if ( $collapsed_bio === true ) $classes .= " collapsed";
			echo $before_widget;
			if ( !empty( $title ) ) { echo $before_title . $title . $after_title; } ?>
			<div class="author-bio<?php echo $classes ?>">
				<?php if ( $show_avatar === true ) echo get_avatar( get_the_author_email(), '70' ) ?>
			    <h3 class="author-title"><?php _e( 'About', THEME_TEXTDOMAIN ); echo ' '; the_author_posts_link(); ?></h3>
			    <p class="author-bio-text"><?php echo $author_bio_text ?></p>
			    <?php if ( $collapsed_bio === true ) : ?>
			    <a class="expand-toggle expand-link" href="javascript:;">+ Expand Bio</a>
			    <a class="expand-toggle collapse-link" href="javascript:;">- Collapse Bio</a>
			    <?php endif; ?>
			</div>
			<?php if ( $collapsed_bio === true ) : ?>
			<script type="text/javascript">
			jQuery( document ).ready( function( $ ) {
				$( "#<?php echo $this->id; ?> .expand-toggle" ).unbind( 'click' ).bind( 'click', function( e ) {
					$( "#<?php echo $this->id; ?> .author-bio" ).toggleClass( "collapsed" );
				});
			});
			</script>
			<?php endif;
			echo $after_widget;
		}
	}

	/**
	 * Ouputs the options form on admin
	 *
	 * @param array $instance The widget options
	 */
	public function form( $instance ) {
		$title 			= isset( $instance['title'] ) ? esc_attr( $instance['title'] ) : '';
		$show_avatar 	= isset( $instance['show_avatar'] ) ? (bool) $instance['show_avatar'] : true;
		$collapsed_bio 	= isset( $instance['collapsed_bio'] ) ? (bool) $instance['collapsed_bio'] : true;
		?><p>
			<label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Title:', THEME_TEXTDOMAIN ); ?></label>
			<input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo $title; ?>" />
		</p>
		<p>
			<input class="checkbox" type="checkbox" <?php checked( $show_avatar ); ?> id="<?php echo $this->get_field_id( 'show_avatar' ); ?>" name="<?php echo $this->get_field_name( 'show_avatar' ); ?>" />
			<label for="<?php echo $this->get_field_id( 'show_avatar' ); ?>"><?php _e( 'Show Avatar', THEME_TEXTDOMAIN ); ?></label>
		</p>
		<p>
			<input class="checkbox" type="checkbox" <?php checked( $collapsed_bio ); ?> id="<?php echo $this->get_field_id( 'collapsed_bio' ); ?>" name="<?php echo $this->get_field_name( 'collapsed_bio' ); ?>" />
			<label for="<?php echo $this->get_field_id( 'collapsed_bio' ); ?>"><?php _e( 'Collapsed Bio', THEME_TEXTDOMAIN ); ?></label>
		</p><?php
	}

	/**
	 * Processing and Sanitize widget form values as they are saved
	 *
	 * @param array $new_instance The new options
	 * @param array $old_instance The previous options
	 */
	public function update( $new_instance, $old_instance ) {
		$instance = $old_instance;
		$instance['title'] = strip_tags( $new_instance['title'] );
		$instance['show_avatar'] = (bool) $new_instance['show_avatar'];
		$instance['collapsed_bio'] = (bool) $new_instance['collapsed_bio'];
		return $instance;
	}
}


?>