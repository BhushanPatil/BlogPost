<?php


/**
 * Register widget
 */
add_action( 'widgets_init', create_function( '', 'return register_widget("BP_Widget_Dribbble");' ) );


/**
 * Widget class
 */
class BP_Widget_Dribbble extends WP_Widget {

	/**
	 * Sets up the widgets name etc
	 */
	public function __construct() {
		parent::__construct(
			'dribbble', // Base ID
			__( 'Dribbble' ), // Widget Name
			array( 'description' => __( 'Shows latest Dribbble shots.' ), ) // Widget description on admin
		);
	}

	/**
	 * Outputs the content of the widget
	 *
	 * @param array $args
	 * @param array $instance
	 */
	public function widget( $args, $instance ) {
		extract($args);
	  	$title 			= apply_filters( 'widget_title', $instance['title'] );
	  	$description 	= apply_filters( 'widget_text', empty( $instance['description'] ) ? '' : $instance['description'], $instance );
	  	$dribbble_user 	= strip_tags($instance['dribbble_user']);
	  	$shots 			= $instance['shots'];
	  	$columns 		= $instance['columns'];
	  	echo $args['before_widget'];
	  	if ( $title ) echo $args['before_title'] . $title . $args['after_title'];
	  	if ( !empty($description) ) echo '<p class="description">'. $description .'</p>';
	  	if ( !empty($dribbble_user) ) BP_dribbble_shots( $dribbble_user, $shots, $columns );
	  	else echo '<em>'. __( 'Error, No user specified' ) .'</em>';
	  	echo $args['after_widget'];
	}

	/**
	 * Ouputs the options form on admin
	 *
	 * @param array $instance The widget options
	 */
	public function form( $instance ) {
		$title 			= isset( $instance['title'] ) ? esc_attr( $instance['title'] ) : __( 'Dribbble Shots' );
		$description 	= esc_textarea( $instance['description'] );
		$dribbble_user 	= $instance['dribbble_user'];
		$shots 			= isset( $instance['shots'] ) ? absint( $instance['shots'] ) : 8;
		$columns 		= isset( $instance['columns'] ) ? absint( $instance['columns'] ) : 4;
		?><p>
			<label for="<?php echo $this->get_field_id( 'title' ); ?>">Title:<?php _e( 'Title:', THEME_TEXTDOMAIN ); ?></label>
			<input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo $title; ?>" />
		</p>
		<p>
			<label for="<?php echo $this->get_field_id( 'description' ); ?>"><?php _e( 'Description:', THEME_TEXTDOMAIN ); ?></label>
			<textarea class="widefat" rows="3" cols="20" id="<?php echo $this->get_field_id( 'description' ); ?>" name="<?php echo $this->get_field_name( 'description' ); ?>"><?php echo $description; ?></textarea>
		</p>
		<p>
			<label for="<?php echo $this->get_field_id( 'dribbble_user' ); ?>"><?php _e( 'Dribbble User:', THEME_TEXTDOMAIN ); ?></label>
			<input class="widefat" id="<?php echo $this->get_field_id( 'dribbble_user' ); ?>" name="<?php echo $this->get_field_name( 'dribbble_user' ); ?>" type="text" value="<?php echo $dribbble_user; ?>" />
		</p>
		<p>
			<label for="<?php echo $this->get_field_id( 'shots' ); ?>"><?php _e( 'Number of shots to show:', THEME_TEXTDOMAIN ); ?></label>
			<input id="<?php echo $this->get_field_id( 'shots' ); ?>" name="<?php echo $this->get_field_name( 'shots' ); ?>" type="text" value="<?php echo $shots; ?>" size="3" />
		</p>
		<p>
			<label for="<?php echo $this->get_field_id( 'columns' ); ?>"><?php _e( 'Shots per row:', THEME_TEXTDOMAIN ); ?></label>
			<input id="<?php echo $this->get_field_id( 'columns' ); ?>" name="<?php echo $this->get_field_name( 'columns' ); ?>" type="text" value="<?php echo $columns; ?>" size="3" />
		</p><?php
	}

	/**
	 * Processing and Sanitize widget form values as they are saved
	 *
	 * @param array $new_instance The new options
	 * @param array $old_instance The previous options
	 */
	public function update( $new_instance, $old_instance ) {
		$instance = $old_instance;
		$instance['title'] = strip_tags( $new_instance['title'] );
		if ( current_user_can( 'unfiltered_html' ) ) $instance['description'] =  $new_instance['description'];
		else $instance['description'] = stripslashes( wp_filter_post_kses( addslashes($new_instance['description']) ) ); // wp_filter_post_kses() expects slashed
		$instance['dribbble_user'] = trim( $new_instance['dribbble_user'] );
		$instance['shots'] = absint($new_instance['shots']);
		if ( $instance['shots'] < 1 ) $instance['shots'] = 8;
		else if ( $instance['shots'] > 15 ) $instance['shots'] = 15;
		$instance['columns'] = absint($new_instance['columns']);
		if ( $instance['columns'] < 1 ) $instance['columns'] = 4;
		else if ( $instance['columns'] > 5 ) $instance['columns'] = 5;
		return $instance;
	}

} // class BP_dribbble


function BP_dribbble_shots( $player, $shots = 8, $columns = 4 ) {
	$key = 'dribbble_'. $player;
	$cache = get_transient( $key );

	if ( $cache === false ) {
		$url = 'http://api.dribbble.com/players/'. $player .'/shots';
		$response = wp_remote_get( $url );
		if( is_wp_error( $response ) ) return;
		$xml = wp_remote_retrieve_body( $response );
		if( is_wp_error( $xml ) ) return;
		if( $response['headers']['status'] == 200 ) {
			$json = json_decode( $xml );
			$dribbble_shots = $json->shots;
			set_transient( $key, $dribbble_shots, 60*5 );
		}
	}
	else $dribbble_shots = $cache;

	if ( $dribbble_shots ) {
		$i = 0;

		if ( $columns == 4 ) $columns = 'shots-col-4';
		else if ( $columns == 5 ) $columns = 'shots-col-5';
		else if ( $columns == 3 ) $columns = 'shots-col-3';
		else if ( $columns == 2 ) $columns = 'shots-col-2';
		else if ( $columns == 1 ) $columns = 'shots-col-1';
		echo '<ul class="dribbble-shots '. $columns .'">';
		foreach( $dribbble_shots as $dribbble_shot ) {
			if ( $i == $shots ) break;
			echo '<li><a href="'. $dribbble_shot->url .'">';
			echo '<img height="'. $dribbble_shot->height .'" width="'. $dribbble_shots[$i]->width .'" src="'. $dribbble_shot->image_url .'" alt="'. $dribbble_shot->title .'" />';
			echo '</a></li>';
			$i++;
		}
		echo '</ul>';
	}
	else echo '<em>'. __( 'Error retrieving Dribbble shots' ) .'</em>';
}


?>