<?php


/**
 * Register widget
 */
add_action('widgets_init', create_function('', 'return register_widget("BP_Widget_Facebook_Like_Box");') );


/**
 * Widget class
 */
class BP_Widget_Facebook_Like_Box extends WP_Widget {

	/**
	 * Sets up the widgets name etc
	 */
	public function __construct() {
		parent::__construct(
			'facebook_like_box', // Base ID
			__( 'Facebook Like Box', THEME_TEXTDOMAIN ), // Widget Name
			array( 'description' => __( 'Shows latest Facebook Like Box', THEME_TEXTDOMAIN ), ) // Widget description on admin
		);
	}

	/**
	 * Outputs the content of the widget
	 *
	 * @param array $args
	 * @param array $instance
	 */
	public function widget( $args, $instance ) {
		extract($args);
	  	$title 			= apply_filters( 'widget_title', $instance['title'] );
	  	$description 	= apply_filters( 'widget_text', empty( $instance['description'] ) ? '' : $instance['description'], $instance );
	  	$facebook_page 	= $instance['facebook_page'];
	  	$background 	= empty( $instance['background'] ) ? 'transparent' : $instance['background'];
	  	$color_scheme 	= $instance['color_scheme'];
	  	$show_faces 	= $instance['show_faces'] ? 'true' : 'false';
	  	$show_header 	= $instance['show_header'] ? 'true' : 'false';
	  	$show_posts 	= $instance['show_posts'] ? 'true' : 'false';
	  	$show_border 	= $instance['show_border'] ? 'true' : 'false';

	  	if ( $background === 'transparent' ) {
	  		if ( $show_faces === 'false' && $show_header === 'false'  && $show_posts === 'false'  && $show_border === 'false' )
	  			$height	= '59px';
	  		else if ( $show_faces === 'true' && $show_header === 'false'  && $show_posts === 'false'  && $show_border === 'false' )
	  			$height	= '230px';
	  		else if ( $show_faces === 'false' && $show_header === 'true'  && $show_posts === 'false'  && $show_border === 'false' )
	  			$height	= '59px';
	  		else if ( $show_faces === 'false' && $show_header === 'false'  && $show_posts === 'true'  && $show_border === 'false' )
	  			$height	= '388px';
	  		else if ( $show_faces === 'false' && $show_header === 'false'  && $show_posts === 'false'  && $show_border === 'true' )
	  			$height	= '59px';
	  		else if ( $show_faces === 'true' && $show_header === 'true'  && $show_posts === 'false'  && $show_border === 'false' )
	  			$height	= '255px';
	  		else if ( $show_faces === 'true' && $show_header === 'false'  && $show_posts === 'true'  && $show_border === 'false' )
	  			$height	= '531px';
	  		else if ( $show_faces === 'true' && $show_header === 'false'  && $show_posts === 'false'  && $show_border === 'true' )
	  			$height	= '241px';
	  		else if ( $show_faces === 'false' && $show_header === 'true'  && $show_posts === 'true'  && $show_border === 'false' )
	  			$height	= '413px';
	  		else if ( $show_faces === 'false' && $show_header === 'true'  && $show_posts === 'false'  && $show_border === 'true' )
	  			$height	= '59px';
	  		else if ( $show_faces === 'false' && $show_header === 'false'  && $show_posts === 'true'  && $show_border === 'true' )
	  			$height	= '395px';
	  		else if ( $show_faces === 'true' && $show_header === 'true'  && $show_posts === 'true'  && $show_border === 'false' )
	  			$height	= '569px';
	  		else if ( $show_faces === 'false' && $show_header === 'true'  && $show_posts === 'true'  && $show_border === 'true' )
	  			$height	= '425px';
	  		else if ( $show_faces === 'true' && $show_header === 'false'  && $show_posts === 'true'  && $show_border === 'true' )
	  			$height	= '541px';
	  		else if ( $show_faces === 'true' && $show_header === 'true'  && $show_posts === 'false'  && $show_border === 'true' )
	  			$height	= '271px';
	  		else
	  			$height	= '571px';
	  	}
	  	else {
	  		if ( $show_faces === 'false' && $show_header === 'false'  && $show_posts === 'false'  && $show_border === 'false' )
	  			$height	= '68px';
	  		else if ( $show_faces === 'true' && $show_header === 'false'  && $show_posts === 'false'  && $show_border === 'false' )
	  			$height	= '239px';
	  		else if ( $show_faces === 'false' && $show_header === 'true'  && $show_posts === 'false'  && $show_border === 'false' )
	  			$height	= '68px';
	  		else if ( $show_faces === 'false' && $show_header === 'false'  && $show_posts === 'true'  && $show_border === 'false' )
	  			$height	= '398px';
	  		else if ( $show_faces === 'false' && $show_header === 'false'  && $show_posts === 'false'  && $show_border === 'true' )
	  			$height	= '68px';
	  		else if ( $show_faces === 'true' && $show_header === 'true'  && $show_posts === 'false'  && $show_border === 'false' )
	  			$height	= '264px';
	  		else if ( $show_faces === 'true' && $show_header === 'false'  && $show_posts === 'true'  && $show_border === 'false' )
	  			$height	= '539px';
	  		else if ( $show_faces === 'true' && $show_header === 'false'  && $show_posts === 'false'  && $show_border === 'true' )
	  			$height	= '241px';
	  		else if ( $show_faces === 'false' && $show_header === 'true'  && $show_posts === 'true'  && $show_border === 'false' )
	  			$height	= '423px';
	  		else if ( $show_faces === 'false' && $show_header === 'true'  && $show_posts === 'false'  && $show_border === 'true' )
	  			$height	= '68px';
	  		else if ( $show_faces === 'false' && $show_header === 'false'  && $show_posts === 'true'  && $show_border === 'true' )
	  			$height	= '395px';
	  		else if ( $show_faces === 'true' && $show_header === 'true'  && $show_posts === 'true'  && $show_border === 'false' )
	  			$height	= '569px';
	  		else if ( $show_faces === 'false' && $show_header === 'true'  && $show_posts === 'true'  && $show_border === 'true' )
	  			$height	= '425px';
	  		else if ( $show_faces === 'true' && $show_header === 'false'  && $show_posts === 'true'  && $show_border === 'true' )
	  			$height	= '541px';
	  		else if ( $show_faces === 'true' && $show_header === 'true'  && $show_posts === 'false'  && $show_border === 'true' )
	  			$height	= '271px';
	  		else
	  			$height	= '571px';
	  	}
	  	
	  	echo $args['before_widget'];
	  	if ( $title ) echo $args['before_title'] . $title . $args['after_title'];
	  	if ( !empty($description) ) echo '<p class="description">'. $description .'</p>';
	  	if ( !empty($facebook_page) ) { ?>
	  	<div class="facebook-like-box" style="background:<?php echo $background; ?>; overflow:hidden; width:100%; height:<?php echo $height; ?>;">
	  		<iframe src="http://www.facebook.com/plugins/likebox.php?href=http%3A%2F%2Fwww.facebook.com%2F<?php echo $facebook_page; ?>&amp;width&amp;height&amp;colorscheme=<?php echo $color_scheme; ?>&amp;show_faces=<?php echo $show_faces; ?>&amp;header=<?php echo $show_header; ?>&amp;stream=<?php echo $show_posts; ?>&amp;show_border=<?php echo $show_border; ?>" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:100%; height:<?php echo $height; ?>;" allowTransparency="true"></iframe>
	  	</div>
	  	<?php
	  	}
	  	else echo '<em>'. __( 'Error, No facebook page specified' ) .'</em>';
	  	echo $args['after_widget'];
	}

	/**
	 * Ouputs the options form on admin
	 *
	 * @param array $instance The widget options
	 */
	public function form( $instance ) {
		$title 			= isset( $instance['title'] ) ? esc_attr( $instance['title'] ) : __( 'Find us on Facebook'  );
		$description 	= esc_textarea( $instance['description'] );
		$facebook_page 	= $instance['facebook_page'];
		$color_scheme 	= isset( $instance['color_scheme'] ) ? $instance['color_scheme'] : 'light';
		$background 	= isset( $instance['background'] ) ? $instance['background'] : '';
		$show_faces 	= isset( $instance['show_faces'] ) ? (bool) $instance['show_faces'] : true;
		$show_header 	= isset( $instance['show_header'] ) ? (bool) $instance['show_header'] : false;
		$show_posts 	= isset( $instance['show_posts'] ) ? (bool) $instance['show_posts'] : false;
		$show_border 	= isset( $instance['show_border'] ) ? (bool) $instance['show_border'] : false;
		?><p>
			<label for="<?php echo $this->get_field_id('title'); ?>"><?php _e( 'Title:', THEME_TEXTDOMAIN ); ?></label>
			<input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo $title; ?>" />
		</p>
		<p>
			<label for="<?php echo $this->get_field_id('description'); ?>"><?php _e( 'Description:', THEME_TEXTDOMAIN ); ?></label>
			<textarea class="widefat" rows="3" cols="20" id="<?php echo $this->get_field_id('description'); ?>" name="<?php echo $this->get_field_name('description'); ?>"><?php echo $description; ?></textarea>
		</p>
		<p>
			<label for="<?php echo $this->get_field_id('facebook_page'); ?>"><?php _e( 'Facebook Page:', THEME_TEXTDOMAIN ); ?></label>
			<input class="widefat" id="<?php echo $this->get_field_id('facebook_page'); ?>" name="<?php echo $this->get_field_name('facebook_page'); ?>" type="text" value="<?php echo $facebook_page; ?>" />
		</p>
		<p>
			<label for="<?php echo $this->get_field_id('color_scheme'); ?>"><?php _e( 'Color scheme:', THEME_TEXTDOMAIN ) ?></label>
			<select class="widefat" id="<?php echo $this->get_field_id('color_scheme'); ?>" name="<?php echo $this->get_field_name('color_scheme'); ?>">
				<option value="light" <?php selected('light', $color_scheme) ?>><?php _e( 'Light', THEME_TEXTDOMAIN ) ?></option>
				<option value="dark" <?php selected('dark', $color_scheme) ?>><?php _e( 'Dark', THEME_TEXTDOMAIN ) ?></option>
			</select>
		</p>
		<p>
			<label for="<?php echo $this->get_field_id('background'); ?>"><?php _e( 'Background:', THEME_TEXTDOMAIN ); ?></label>
			<input class="widefat" id="<?php echo $this->get_field_id('background'); ?>" name="<?php echo $this->get_field_name('background'); ?>" type="text" value="<?php echo $background; ?>" />
		</p>
		<p>
			<input id="<?php echo $this->get_field_id('show_faces'); ?>" name="<?php echo $this->get_field_name('show_faces'); ?>" type="checkbox" <?php checked( $show_faces ); ?> />
			<label for="<?php echo $this->get_field_id('show_faces'); ?>"><?php _e( 'Show Friends\' Faces', THEME_TEXTDOMAIN ); ?></label>
		</p>
		<p>
			<input id="<?php echo $this->get_field_id('show_header'); ?>" name="<?php echo $this->get_field_name('show_header'); ?>" type="checkbox" <?php checked( $show_header ); ?> />
			<label for="<?php echo $this->get_field_id('show_header'); ?>"><?php _e( 'Show Header', THEME_TEXTDOMAIN ); ?></label>
		</p>
		<p>
			<input id="<?php echo $this->get_field_id('show_posts'); ?>" name="<?php echo $this->get_field_name('show_posts'); ?>" type="checkbox" <?php checked( $show_posts ); ?> />
			<label for="<?php echo $this->get_field_id('show_posts'); ?>"><?php _e( 'Show Posts', THEME_TEXTDOMAIN ); ?></label>
		</p>
		<p>
			<input id="<?php echo $this->get_field_id('show_border'); ?>" name="<?php echo $this->get_field_name('show_border'); ?>" type="checkbox" <?php checked( $show_border ); ?> />
			<label for="<?php echo $this->get_field_id('show_border'); ?>"><?php _e( 'Show Border', THEME_TEXTDOMAIN ); ?></label>
		</p><?php
	}

	/**
	 * Processing and Sanitize widget form values as they are saved
	 *
	 * @param array $new_instance The new options
	 * @param array $old_instance The previous options
	 */
	public function update( $new_instance, $old_instance ) {
		$instance = $old_instance;
		$instance['title'] = strip_tags( $new_instance['title'] );
		if ( current_user_can('unfiltered_html') ) $instance['description'] =  $new_instance['description'];
		else $instance['description'] = stripslashes( wp_filter_post_kses( addslashes($new_instance['description']) ) ); // wp_filter_post_kses() expects slashed
		$instance['facebook_page'] = trim( $new_instance['facebook_page'] );
		$instance['color_scheme'] = stripslashes( $new_instance['color_scheme'] );
		$instance['background'] = trim( $new_instance['background'] );
		$instance['show_faces'] = (bool) $new_instance['show_faces'];
		$instance['show_header'] = (bool) $new_instance['show_header'];
		$instance['show_posts'] = (bool) $new_instance['show_posts'];
		$instance['show_border'] = (bool) $new_instance['show_border'];
		return $instance;
	}
}


?>