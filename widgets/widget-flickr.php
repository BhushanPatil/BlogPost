<?php


/**
 * Register widget
 */
add_action( 'widgets_init', create_function( '', 'return register_widget("BP_Widget_Flickr");' ) );


/**
 * Widget class
 */
class BP_Widget_Flickr extends WP_Widget {

	/**
	 * Sets up the widgets name etc
	 */
	public function __construct() {
		parent::__construct(
			'flickr', // Base ID
			__( 'Flickr' ), // Widget Name
			array( 'description' => __( 'Shows latest Flickr photos' ), ) // Widget description on admin
		);
	}

	/**
	 * Outputs the content of the widget
	 *
	 * @param array $args
	 * @param array $instance
	 */
	public function widget( $args, $instance ) {
		extract($args);
	  	$title 			= apply_filters( 'widget_title', $instance['title'] );
	  	$description 	= apply_filters( 'widget_text', empty( $instance['description'] ) ? '' : $instance['description'], $instance );
	  	$flickr_id 		= $instance['flickr_id'];
	  	$source_type 	= $instance['source_type'];
	  	$order 			= $instance['order'];
	  	$number 		= $instance['number'];
	  	$columns 		= $instance['columns'];
	  	echo $args['before_widget'];
	  	if ( $title ) echo $args['before_title'] . $title . $args['after_title'];
	  	if ( !empty($description) ) echo '<p class="description">'. $description .'</p>';
	  	if ( !empty($flickr_id) ) BP_flickr_photos($flickr_id, $number, $columns, $order, $source_type);
	  	else echo '<em>'. __( 'Error, No user specified' ) .'</em>';
	  	echo $args['after_widget'];
	}

	/**
	 * Ouputs the options form on admin
	 *
	 * @param array $instance The widget options
	 */
	public function form( $instance ) {
		$title 			= isset( $instance['title'] ) ? esc_attr( $instance['title'] ) : __( 'Flickr Photos' );
		$description 	= esc_textarea( $instance['description'] );
		$flickr_id 		= $instance['flickr_id'];
		$source_type 	= isset( $instance['source_type'] ) ? $instance['source_type'] : 'user';
		$order 			= isset( $instance['order'] ) ? $instance['order'] : 'latest';
		$number 		= isset( $instance['number'] ) ? absint( $instance['number'] ) : 8;
		$columns 		= isset( $instance['columns'] ) ? absint( $instance['columns'] ) : 4;
		?><p>
			<label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Title:', THEME_TEXTDOMAIN ); ?></label>
			<input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo $title; ?>" />
		</p>
		<p>
			<label for="<?php echo $this->get_field_id( 'description' ); ?>"><?php _e( 'Description:', THEME_TEXTDOMAIN ); ?></label>
			<textarea class="widefat" rows="3" cols="20" id="<?php echo $this->get_field_id( 'description' ); ?>" name="<?php echo $this->get_field_name( 'description' ); ?>"><?php echo $description; ?></textarea>
		</p>
		<p>
			<label for="<?php echo $this->get_field_id( 'flickr_id' ); ?>"><?php _e( 'Flickr ID:', THEME_TEXTDOMAIN ); ?></label>
			<input class="widefat" id="<?php echo $this->get_field_id( 'flickr_id' ); ?>" name="<?php echo $this->get_field_name( 'flickr_id' ); ?>" type="text" value="<?php echo $flickr_id; ?>" />
		</p>
		<p>
			<label for="<?php echo $this->get_field_id( 'source_type' ); ?>"><?php _e( 'Source Type:', THEME_TEXTDOMAIN ) ?></label>
			<select class="widefat" id="<?php echo $this->get_field_id( 'source_type' ); ?>" name="<?php echo $this->get_field_name( 'source_type' ); ?>">
				<option value="user" <?php selected( 'user', $source_type) ?>><?php _e( 'User', THEME_TEXTDOMAIN ) ?></option>
				<option value="group" <?php selected( 'group', $source_type) ?>><?php _e( 'Group', THEME_TEXTDOMAIN ) ?></option>
				<option value="user_set" <?php selected( 'user_set', $source_type) ?>><?php _e( 'User Set', THEME_TEXTDOMAIN ) ?></option>
			</select>
		</p>
		<p>
			<label for="<?php echo $this->get_field_id( 'order' ); ?>"><?php _e( 'Order:', THEME_TEXTDOMAIN ) ?></label>
			<select class="widefat" id="<?php echo $this->get_field_id( 'order' ); ?>" name="<?php echo $this->get_field_name( 'order' ); ?>">
				<option value="latest" <?php selected( 'latest', $order) ?>><?php _e( 'Latest', THEME_TEXTDOMAIN ) ?></option>
				<option value="random" <?php selected( 'random', $order) ?>><?php _e( 'Random', THEME_TEXTDOMAIN ) ?></option>
			</select>
		</p>
		<p>
			<label for="<?php echo $this->get_field_id( 'number' ); ?>"><?php _e( 'Number of photos to show:', THEME_TEXTDOMAIN ); ?></label>
			<input id="<?php echo $this->get_field_id( 'number' ); ?>" name="<?php echo $this->get_field_name( 'number', THEME_TEXTDOMAIN ); ?>" type="text" value="<?php echo $number; ?>" size="3" />
		</p>
		<p>
			<label for="<?php echo $this->get_field_id( 'columns' ); ?>"><?php _e( 'Photos per row:', THEME_TEXTDOMAIN ); ?></label>
			<input id="<?php echo $this->get_field_id( 'columns' ); ?>" name="<?php echo $this->get_field_name( 'columns' ); ?>" type="text" value="<?php echo $columns; ?>" size="3" />
		</p><?php
	}

	/**
	 * Processing and Sanitize widget form values as they are saved
	 *
	 * @param array $new_instance The new options
	 * @param array $old_instance The previous options
	 */
	public function update( $new_instance, $old_instance ) {

		$instance = $old_instance;

		$instance['title'] = strip_tags( $new_instance['title'] );

		if ( current_user_can( 'unfiltered_html' ) ) $instance['description'] =  $new_instance['description'];
		else $instance['description'] = stripslashes( wp_filter_post_kses( addslashes($new_instance['description']) ) ); // wp_filter_post_kses() expects slashed

		$instance['flickr_id'] = trim( $new_instance['flickr_id'] );

		$instance['source_type'] = stripslashes($new_instance['source_type']);

		$instance['order'] = stripslashes($new_instance['order']);

		$instance['number'] = absint($new_instance['number']);
		if ( $instance['number'] < 1 ) $instance['number'] = 8;
		else if ( $instance['number'] > 10 ) $instance['number'] = 10;

		$instance['columns'] = absint($new_instance['columns']);
		if ( $instance['columns'] < 1 ) $instance['columns'] = 4;
		else if ( $instance['columns'] > 5 ) $instance['columns'] = 5;

		return $instance;
	}
}


function BP_flickr_photos( $flickr_id, $number = 8, $columns = 4, $order = 'latest', $source_type = 'user' ) {
	$url = 'http://www.flickr.com/badge_code_v2.gne?count='. $number .'&amp;size=s&amp;layout=x&amp;display='. $order;
	if ( $source_type == 'group' ) $url .= '&amp;source=group&amp;group='. $flickr_id;
	else if ( $source_type == 'user_set' ) $url .= '&amp;source=user_set&amp;set='. $flickr_id;
	else  $url .= '&amp;source=user&amp;user='. $flickr_id;
	if ( $columns == 4 ) $columns = 'photos-col-4';
	else if ( $columns == 5 ) $columns = 'photos-col-5';
	else if ( $columns == 3 ) $columns = 'photos-col-3';
	else if ( $columns == 2 ) $columns = 'photos-col-2';
	else if ( $columns == 1 ) $columns = 'photos-col-1';
	echo '<div class="flickr-photos '. $columns .'"><script type="text/javascript" src="'. $url .'"></script></div>';
}


?>