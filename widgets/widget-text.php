<?php


/**
 * Un-register WordPress default widget
 */
add_action( 'widgets_init', create_function( '', 'unregister_widget("WP_Widget_Text");' ) );


/**
 * Register widget
 */
add_action( 'widgets_init', create_function( '', 'return register_widget("BP_Widget_Text");' ) );


/**
 * Widget class
 */
class BP_Widget_Text extends WP_Widget {

	/**
	 * Sets up the widgets name etc
	 */
	public function __construct() {
		parent::__construct(
			'text', // Base ID
			__( 'Text' ), // Widget Name
			array( 'classname' => 'widget_text', 'description' => __( 'Arbitrary text or HTML.' ), ) // Widget description on admin
		);
	}

	/**
	 * Outputs the content of the widget
	 *
	 * @param array $args
	 * @param array $instance
	 */
	public function widget( $args, $instance ) {
		extract($args);
		$title = apply_filters( 'widget_title', empty( $instance['title'] ) ? '' : $instance['title'], $instance, $this->id_base );
		$text = apply_filters( 'widget_text', empty( $instance['text'] ) ? '' : $instance['text'], $instance );
		$style = $instance['style'];
		$style_class = '';

		if ( !empty($style) ) $style_class = ' style-'. $style;
		$before_widget = str_replace( 'widget_text', 'widget_text'. $style_class, $before_widget );

		echo $before_widget;
		if ( !empty( $title ) ) { echo $before_title . $title . $after_title; }
		?><div class="textwidget"><?php echo !empty( $instance['filter'] ) ? wpautop( $text ) : $text; ?></div><?php
		echo $after_widget;
	}



	/**
	 * Ouputs the options form on admin
	 *
	 * @param array $instance The widget options
	 */
	public function form( $instance ) {
		$instance = wp_parse_args( (array) $instance, array( 'title' => '', 'text' => '' ) );
		$title = strip_tags($instance['title']);
		$text = esc_textarea($instance['text']);
		$style = isset( $instance['style'] ) ? $instance['style'] : 'none';
		?><p>
			<label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Title:', THEME_TEXTDOMAIN ); ?></label>
			<input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo esc_attr($title); ?>" />
		</p>
		<textarea class="widefat" rows="16" cols="20" id="<?php echo $this->get_field_id( 'text' ); ?>" name="<?php echo $this->get_field_name( 'text' ); ?>"><?php echo $text; ?></textarea>
		<p>
			<input id="<?php echo $this->get_field_id( 'filter' ); ?>" name="<?php echo $this->get_field_name( 'filter' ); ?>" type="checkbox" <?php checked(isset($instance['filter']) ? $instance['filter'] : 0); ?> />&nbsp;<label for="<?php echo $this->get_field_id( 'filter' ); ?>"><?php _e( 'Automatically add paragraphs', THEME_TEXTDOMAIN ); ?></label>
		</p>
		<p>
			<label for="<?php echo $this->get_field_id( 'style' ); ?>"><?php _e( 'Style:', THEME_TEXTDOMAIN ) ?></label>
			<select class="widefat" id="<?php echo $this->get_field_id( 'style' ); ?>" name="<?php echo $this->get_field_name( 'style' ); ?>">
				<option value="none" <?php selected( 'none', $style) ?>><?php _e( 'None', THEME_TEXTDOMAIN ) ?></option>
				<option value="box" <?php selected( 'box', $style) ?>><?php _e( 'Box', THEME_TEXTDOMAIN ) ?></option>
				<option value="notepad" <?php selected( 'notepad', $style) ?>><?php _e( 'Notepad', THEME_TEXTDOMAIN ) ?></option>
				<option value="notepaper" <?php selected( 'notepaper', $style) ?>><?php _e( 'Notepaper', THEME_TEXTDOMAIN ) ?></option>
				<option value="notepaper-blue" <?php selected( 'notepaper-blue', $style) ?>><?php _e( 'Notepaper Blue', THEME_TEXTDOMAIN ) ?></option>
				<option value="notepaper-yellow" <?php selected( 'notepaper-yellow', $style) ?>><?php _e( 'Notepaper Yellow', THEME_TEXTDOMAIN ) ?></option>
			</select>
		</p><?php
	}

	/**
	 * Processing and Sanitize widget form values as they are saved
	 *
	 * @param array $new_instance The new options
	 * @param array $old_instance The previous options
	 */
	public function update( $new_instance, $old_instance ) {
		$instance = $old_instance;
		$instance['title'] = strip_tags($new_instance['title']);
		if ( current_user_can( 'unfiltered_html' ) ) $instance['text'] =  $new_instance['text'];
		else $instance['text'] = stripslashes( wp_filter_post_kses( addslashes($new_instance['text']) ) ); // wp_filter_post_kses() expects slashed
		$instance['filter'] = isset($new_instance['filter']);
		$instance['style'] = stripslashes( $new_instance['style'] );
		return $instance;
	}
}


?>