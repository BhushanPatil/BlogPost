<?php


/**
 * Register widget
 */
add_action( 'widgets_init', create_function( '', 'return register_widget("BP_Widget_Twitter");' ) );


function BP_latest_tweets( $consumer_key, $consumer_secret, $access_token, $access_token_secret, $user = null, $number = 3, $button = false, $show_photo = true, $show_time = true, $transient = null, $expiration = 60 ) {
	if ( $transient === null ) $transient = 'tweets_'. $user;
	$cache = get_transient( $transient );

	if ( $cache === false ) {
		include_once 'widget-twitter-twitteroauth.php';
		$connect = new TwitterOAuth( $consumer_key, $consumer_secret, $access_token, $access_token_secret );
		$tweets = $connect->get( 'statuses/user_timeline', array( 'screen_name' => $user, 'count' => $number ) );
		set_transient( $transient, $tweets, $expiration );
	}
	else $tweets = $cache;

	if ( $tweets ) {
		if ( $show_photo && $tweet_text ) echo '<ul class="latest-tweets with-photo with-date">';
		else if ( $show_photo ) echo '<ul class="latest-tweets with-photo">';
		else if (  $tweet_text ) echo '<ul class="latest-tweets with-date">';
		else echo '<ul class="latest-tweets">';
		foreach ( $tweets as $tweet ) {
			$tweet_text = $tweet->text;
			$tweet_text = preg_replace( '#http://[a-z0-9._/-]+#i', '<a target="_blank" href="$0">$0</a>', $tweet_text ); 
			$tweet_text = preg_replace( '#@([a-z0-9_]+)#i', '@<a target="_blank" href="http://twitter.com/$1">$1</a>', $tweet_text ); 
			$tweet_text = preg_replace( '# \#([a-z0-9_-]+)#i', ' #<a target="_blank" href="http://twitter.com/search?q=%23$1">$1</a>', $tweet_text ); 
			$tweet_text = preg_replace( '#https://[a-z0-9._/-]+#i', '<a target="_blank" href="$0">$0</a>', $tweet_text );
			echo '<li class="tweet">';
			if ( $show_photo ) echo '<figure class="tweet-user-photo"><a target="_blank" href="https://twitter.com/'. $tweet->user->screen_name .'"><img class="photo" src="'. $tweet->user->profile_image_url .'" alt="'. $tweet->user->screen_name .'" /></a></figure>';
			else echo '<span class="twitter-icon"><i class="fa fa-twitter"></i></span>';
			echo '<p class="tweet-text">';
			echo $tweet_text;
			if ( $show_time ) echo '<a class="tweet-timestamp" target="_blank" href="http://twitter.com/'. $tweet->user->screen_name .'/status/'. $tweet->id_str .'"><time class="datetime" datetime="'. date( 'c', strtotime( $tweet->created_at ) ) .'">'. human_time_diff( date( 'U', strtotime( $tweet->created_at ) ), current_time('timestamp') ) . ' ago</time></a>';
			echo '</p>';
			echo '</li>';
		}
		echo '</ul>';
		if ( $button && !empty( $button ) ) echo '<p class="follow-button"><a class="button smaller" target="_blank" href="https://twitter.com/intent/user?screen_name='. $tweet->user->screen_name .'">'. $button .'</a></p>';
	}
	else echo '<em>'. __( 'Error retrieving tweets' ) .'</em>';
}


/**
 * Widget class
 */
class BP_Widget_Twitter extends WP_Widget {

	/**
	 * Sets up the widgets name etc
	 */
	public function __construct() {
		parent::__construct(
			'twitter', // Base ID
			__( 'Tweets' ), // Widget Name
			array( 'description' => __( 'Shows latest tweets'  ), ) // Widget description on admin
		);
	}

	/**
	 * Outputs the content of the widget
	 *
	 * @param array $args
	 * @param array $instance
	 */
	public function widget( $args, $instance ) {
		extract($args);

	  	$title 				= apply_filters( 'widget_title', $instance['title'] );
	  	$description 		= apply_filters( 'widget_text', empty( $instance['description'] ) ? '' : $instance['description'], $instance );
	  	$user 				= $instance['user'];
	  	$number 			= $instance['number'];
	  	$cachetime 			= $instance['cachetime'];
	  	$show_button 		= isset( $instance['show_button'] ) ? (bool) $instance['show_button'] : true;
	  	$show_user_photo 	= isset( $instance['show_user_photo'] ) ? (bool) $instance['show_user_photo'] : true;
	  	$show_time 			= isset( $instance['show_time'] ) ? (bool) $instance['show_time'] : true;

	  	if ( $show_button ) $button = $instance['button_text'];
	  	else $button = false;

	  	echo $args['before_widget'];
	  	if ( $title ) echo $args['before_title'] . $title . $args['after_title'];
	  	if ( !empty( $description ) ) echo '<p class="description">'. $description .'</p>';
	  	BP_latest_tweets( $instance['consumer_key'], $instance['consumer_secret'], $instance['access_token'], $instance['access_token_secret'], $user, $number, $button, $show_user_photo, $show_time, $instance['transient'], $cachetime );
	  	echo $args['after_widget'];
	}

	/**
	 * Ouputs the options form on admin
	 *
	 * @param array $instance The widget options
	 */
	public function form( $instance ) {
		/* Set up some default widget settings. */
		$title 					= isset( $instance['title'] ) ? esc_attr( $instance['title'] ) : __( 'Latest Tweets' );
		$description 			= esc_textarea( $instance['description'] );
		$user 					= $instance['user'];
		$number 				= isset( $instance['number'] ) ? absint( $instance['number'] ) : 3;
		$button_text 			= isset( $instance['button_text'] ) ? $instance['button_text'] : __( 'Follow on Twitter' );
		$cachetime 				= isset( $instance['cachetime'] ) ? absint( $instance['cachetime'] ) : 3600;
		$show_button 			= isset( $instance['show_button'] ) ? (bool) $instance['show_button'] : true;
		$show_user_photo 		= isset( $instance['show_user_photo'] ) ? (bool) $instance['show_user_photo'] : true;
		$show_time 				= isset( $instance['show_time'] ) ? (bool) $instance['show_time'] : true;
		?><p>
			<label for="<?php echo $this->get_field_id( 'title' ) ?>"><?php _e( 'Title:', THEME_TEXTDOMAIN ) ?></label>
			<input class="widefat" id="<?php echo $this->get_field_id( 'title' ) ?>" name="<?php echo $this->get_field_name( 'title' ) ?>" type="text" value="<?php echo $title ?>" />
		</p>
		<p>
			<label for="<?php echo $this->get_field_id( 'description' ) ?>"><?php _e( 'Description:', THEME_TEXTDOMAIN ) ?></label>
			<textarea class="widefat" rows="2" cols="20" id="<?php echo $this->get_field_id( 'description' ) ?>" name="<?php echo $this->get_field_name( 'description' ) ?>"><?php echo $description ?></textarea>
		</p>
		<p>
			<label for="<?php echo $this->get_field_id( 'user' ) ?>"><?php _e( 'Username (without @):', THEME_TEXTDOMAIN ) ?></label>
			<input type="text" class="widefat" id="<?php echo $this->get_field_id( 'user' ) ?>" name="<?php echo $this->get_field_name( 'user' ) ?>" value="<?php echo $user ?>" />
		</p>
		<p>
			<label for="<?php echo $this->get_field_id( 'number' ) ?>"><?php _e( 'Number of tweets (max 20):', THEME_TEXTDOMAIN ) ?></label>
			<input type="text" class="widefat" id="<?php echo $this->get_field_id( 'number' ) ?>" name="<?php echo $this->get_field_name( 'number' ) ?>" value="<?php echo $number ?>" />
		</p>
		<p>
			<label for="<?php echo $this->get_field_id( 'button_text' ) ?>"><?php _e( 'Follow Button Text', THEME_TEXTDOMAIN ) ?></label>
			<input type="text" class="widefat" id="<?php echo $this->get_field_id( 'button_text' ) ?>" name="<?php echo $this->get_field_name( 'button_text' ) ?>" value="<?php echo $button_text ?>" />
		</p>
		<p>
			<label for="<?php echo $this->get_field_id( 'cachetime' ) ?>"><?php _e( 'Cache Time (in second):', THEME_TEXTDOMAIN ) ?></label>
			<input type="text" class="widefat" id="<?php echo $this->get_field_id( 'cachetime' ) ?>" name="<?php echo $this->get_field_name( 'cachetime' ) ?>" value="<?php echo $cachetime ?>" />
		</p>
		<p>
			<input class="checkbox" type="checkbox" <?php checked( $show_button ) ?> id="<?php echo $this->get_field_id( 'show_button' ) ?>" name="<?php echo $this->get_field_name( 'show_button' ) ?>" />
			<label for="<?php echo $this->get_field_id( 'show_button' ) ?>"><?php _e( 'Show Follow Button', THEME_TEXTDOMAIN ) ?></label>
		</p>
		<p>
			<input class="checkbox" type="checkbox" <?php checked( $show_user_photo ) ?> id="<?php echo $this->get_field_id( 'show_user_photo' ) ?>" name="<?php echo $this->get_field_name( 'show_user_photo' ) ?>" />
			<label for="<?php echo $this->get_field_id( 'show_user_photo' ) ?>"><?php _e( 'Show User Photo', THEME_TEXTDOMAIN ) ?></label>
		</p>
		<p>
			<input class="checkbox" type="checkbox" <?php checked( $show_time ) ?> id="<?php echo $this->get_field_id( 'show_time' ) ?>" name="<?php echo $this->get_field_name( 'show_time' ) ?>" />
			<label for="<?php echo $this->get_field_id( 'show_time' ) ?>"><?php _e( 'Show Time', THEME_TEXTDOMAIN ) ?></label>
		</p>
		<hr/>
		<h4 style="margin-bottom: 0;"><?php _e( 'API Settings:', THEME_TEXTDOMAIN ) ?></h4>
		<small><?php _e( 'You need to create an application on <a href="https://dev.twitter.com" target="_blank">Twitter for Developer</a> to have an access to these information.', THEME_TEXTDOMAIN ) ?></small><br />
		<p>
			<label for="<?php echo $this->get_field_id( 'consumer_key', THEME_TEXTDOMAIN ) ?>"><?php _e( 'Consumer Key:' ) ?></label>
			<input type="text" class="widefat" id="<?php echo $this->get_field_id( 'consumer_key' ) ?>" name="<?php echo $this->get_field_name( 'consumer_key' ) ?>" value="<?php echo $instance['consumer_key'] ?>" />
		</p>
		<p>
			<label for="<?php echo $this->get_field_id( 'consumer_secret' ) ?>"><?php _e( 'Consumer Secret:', THEME_TEXTDOMAIN ) ?></label>
			<input type="text" class="widefat" id="<?php echo $this->get_field_id( 'consumer_secret' ) ?>" name="<?php echo $this->get_field_name( 'consumer_secret' ) ?>" value="<?php echo $instance['consumer_secret'] ?>" />
		</p>
		<p>
			<label for="<?php echo $this->get_field_id( 'access_token' ) ?>"><?php _e( 'Access Token:', THEME_TEXTDOMAIN ) ?></label>
			<input type="text" class="widefat" id="<?php echo $this->get_field_id( 'access_token' ) ?>" name="<?php echo $this->get_field_name( 'access_token' ) ?>" value="<?php echo $instance['access_token'] ?>" />
		</p>
		<p>
			<label for="<?php echo $this->get_field_id( 'access_token_secret' ) ?>"><?php _e( 'Access Token Secret:', THEME_TEXTDOMAIN ) ?></label>
			<input type="text" class="widefat" id="<?php echo $this->get_field_id( 'access_token_secret' ) ?>" name="<?php echo $this->get_field_name( 'access_token_secret' ) ?>" value="<?php echo $instance['access_token_secret'] ?>" />
		</p><?php
	}

	/**
	 * Processing and Sanitize widget form values as they are saved
	 *
	 * @param array $new_instance The new options
	 * @param array $old_instance The previous options
	 */
	public function update( $new_instance, $old_instance ) {
		$instance = $old_instance;

		/* Strip tags for title and user to remove HTML (important for text inputs). */
		$instance['title'] 					= strip_tags( $new_instance['title'] );

		if ( current_user_can( 'unfiltered_html' ) )
			$instance['description'] 		=  $new_instance['description'];
		else
			$instance['description'] 		= stripslashes( wp_filter_post_kses( addslashes($new_instance['description']) ) ); // wp_filter_post_kses() expects slashed

		$instance['user'] 					= strip_tags( $new_instance['user'] );
		$instance['number'] 				= $new_instance['number'];
		$instance['cachetime'] 				= $new_instance['cachetime'];
		$instance['button_text'] 			= strip_tags( $new_instance['button_text'] );
		$instance['show_button'] 			= (bool) $new_instance['show_button'];
		$instance['show_user_photo'] 		= (bool) $new_instance['show_user_photo'];
		$instance['show_time'] 				= (bool) $new_instance['show_time'];
		$instance['consumer_key'] 			= $new_instance['consumer_key'];
		$instance['consumer_secret'] 		= $new_instance['consumer_secret'];
		$instance['access_token'] 			= $new_instance['access_token'];
		$instance['access_token_secret'] 	= $new_instance['access_token_secret'];

		$old_transient = $old_transient = $old_instance['user'] . $old_instance['number'] . $old_instance['cachetime'];
		$new_transient = $new_instance['user'] . $new_instance['number'] . $new_instance['cachetime'];
		if ( $new_transient !== $old_transient ) {
			delete_transient( $old_transient );
			$instance['transient'] 	= $new_transient;
		}

		return $instance;
	}
}


?>