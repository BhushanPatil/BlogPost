<?php


/**
 * Register widget
 */
add_action( 'widgets_init', create_function( '', 'return register_widget("BP_Widget_Video");' ) );


/**
 * Widget class
 */
class BP_Widget_Video extends WP_Widget {

	/**
	 * Sets up the widgets name etc
	 */
	public function __construct() {
		parent::__construct(
			'video', // Base ID
			__( 'Video' ), // Widget Name
			array( 'description' => __( 'Shows embeded video' ), ) // Widget description on admin
		);
	}

	/**
	 * Outputs the content of the widget
	 *
	 * @param array $args
	 * @param array $instance
	 */
	public function widget( $args, $instance ) {
		extract($args);
	  	$title 			= apply_filters( 'widget_title', $instance['title'] );
	  	$description 	= apply_filters( 'widget_text', empty( $instance['description'] ) ? '' : $instance['description'], $instance );
	  	$video 			= $instance['video'];
	  	echo $args['before_widget'];
	  	if ( $title ) echo $args['before_title'] . $title . $args['after_title'];
	  	if ( !empty($description) ) echo '<p class="description">'. $description .'</p>';
	  	echo '<div class="video-viewport fluid-video">'. $video .'</div>';
	  	echo $args['after_widget'];
	}

	/**
	 * Ouputs the options form on admin
	 *
	 * @param array $instance The widget options
	 */
	public function form( $instance ) {
		$title 			= isset( $instance['title'] ) ? esc_attr( $instance['title'] ) : __( 'Video' );
		$description 	= esc_textarea( $instance['description'] );
		$video 			= $instance['video']; ?>
		<p>
			<label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Title:', THEME_TEXTDOMAIN ); ?></label>
			<input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo $title; ?>" />
		</p>
		<p>
			<label for="<?php echo $this->get_field_id( 'description' ); ?>"><?php _e( 'Description:', THEME_TEXTDOMAIN ); ?></label>
			<textarea class="widefat" rows="3" cols="20" id="<?php echo $this->get_field_id( 'description' ); ?>" name="<?php echo $this->get_field_name( 'description' ); ?>"><?php echo $description; ?></textarea>
		</p>
		<p>
			<label for="<?php echo $this->get_field_id( 'video' ); ?>"><?php _e( 'Video embed code:', THEME_TEXTDOMAIN ); ?></label>
			<textarea class="widefat" rows="3" cols="20" id="<?php echo $this->get_field_id( 'video' ); ?>" name="<?php echo $this->get_field_name( 'video' ); ?>"><?php echo $video; ?></textarea>
		</p><?php
	}

	/**
	 * Processing and Sanitize widget form values as they are saved
	 *
	 * @param array $new_instance The new options
	 * @param array $old_instance The previous options
	 */
	public function update( $new_instance, $old_instance ) {
		$instance = $old_instance;
		$instance['title'] = strip_tags( $new_instance['title'] );
		if ( current_user_can( 'unfiltered_html' ) ) $instance['description'] =  $new_instance['description'];
		else $instance['description'] = stripslashes( wp_filter_post_kses( addslashes($new_instance['description']) ) ); // wp_filter_post_kses() expects slashed
		$instance['video'] = trim( $new_instance['video'] );
		return $instance;
	}
}


?>