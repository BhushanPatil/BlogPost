<?php


/**
 * Un-register WordPress default widget
 */
add_action( 'widgets_init', create_function( '', 'unregister_widget("WP_Widget_Recent_Posts");' ) );


/**
 * Register widget
 */
add_action( 'widgets_init', create_function( '', 'return register_widget("BP_Widget_Recent_Posts");' ) );


/**
 * Widget class
 */
class BP_Widget_Recent_Posts extends WP_Widget {


	/**
	 * Sets up the widgets name etc
	 */
	public function __construct() {
		parent::__construct(
			'recent_posts', // Base ID
			__( 'Recent Posts', THEME_TEXTDOMAIN ), // Widget Name
			array(
				'classname' => 'widget_recent_entries', // Widget class name
				'description' => __( 'Your site&#8217;s most recent Posts.', THEME_TEXTDOMAIN ), // Widget description on admin
			) );

		add_action( 'save_post', array($this, 'flush_widget_cache') );
		add_action( 'deleted_post', array($this, 'flush_widget_cache') );
		add_action( 'switch_theme', array($this, 'flush_widget_cache') );
	}

	/**
	 * Outputs the content of the widget
	 *
	 * @param array $args
	 * @param array $instance
	 */
	public function widget( $args, $instance ) {
		$cache = wp_cache_get('widget_recent_posts', 'widget');

		if ( !is_array($cache) ) $cache = array();
		if ( !isset( $args['widget_id'] ) ) $args['widget_id'] = $this->id;
		if ( isset( $cache[ $args['widget_id'] ] ) ) {
			echo $cache[ $args['widget_id'] ];
			return;
		}

		extract($args);
		$output = '';

	  	$title 			= apply_filters( 'widget_title', $instance['title'] );
	  	$description 	= apply_filters( 'widget_text', empty( $instance['description'] ) ? '' : $instance['description'], $instance );
	  	$number 		= $instance['number'];
	  	$style 			= $instance['style'];
	  	$show_date 		= isset( $instance['show_date'] ) ? (bool) $instance['show_date'] : true;
	  	$show_thumb 	= isset( $instance['show_thumb'] ) ? (bool) $instance['show_thumb'] : true;
	  	$show_icon 		= isset( $instance['show_icon'] ) ? (bool) $instance['show_icon'] : true;
	  	$list_type 		= $instance['list_type'];
	  	$show_border 	= isset( $instance['show_border'] ) ? (bool) $instance['show_border'] : false;
	  	$background 	= $instance['background'];
	  	$style_class 	= '';

	  	$style_class .= ' style-'. $style;
	  	if ( $show_date ) $style_class .= ' with-date';
	  	if ( $show_thumb ) $style_class .= ' with-thumbnail';
	  	if ( $show_border ) $style_class .= ' with-borders';
	  	if ( $style === "box-extended" ) $style_class .= ' recent-entry-box';

	  	$output .= "<style>.widget_recent_entries.recent-entry-box,.recent-entry-box{background:". $background .";}</style>";

	  	$output .= str_replace( 'widget_recent_entries', 'widget_recent_entries'. $style_class, $args['before_widget'] );
	  	if ( $title ) $output .= $args['before_title'] . $title . $args['after_title'];
	  	if ( !empty($description) ) $output .= '<p class="description">'. $description .'</p>';

	  	$r = new WP_Query( apply_filters( 'widget_posts_args', array( 'posts_per_page' => $number, 'no_found_rows' => true, 'post_status' => 'publish', 'ignore_sticky_posts' => true ) ) );
	  	if ( $r->have_posts() ) :

	  		if ( $style === "box" ) $output .= '<div class="recent-entry-box recent-entry-container">';
	  		else $output .= '<div class="recent-entry-container">';

	  		if ( $list_type === "default" ) $output .= '<ul>';
	  		else if ( $list_type === "none" ) $output .= '<ul style="list-style-type:none;padding-left:0;">';
	  		else $output .= '<ul class="with-list-style" style="list-style-type:'. $list_type .';">';

	  		while ( $r->have_posts() ) : $r->the_post();

	  			if ( $style === "list-2" ) $output .= '<li class="recent-entry recent-entry-box">';
	  			else $output .= '<li class="recent-entry">';

	  			if ( $show_thumb ) :
	  				$output .= '<figure class="recent-entry-thumbnail">';
	  				$output .= '<a href="'. get_permalink() .'">';
	  				if ( has_post_thumbnail() ) $output .= get_the_post_thumbnail( NULL, array(150,150), array( 'class' => 'thumbnail' ) );
	  				else $output .= '<img src="'. THEME_URI .'/images/no-image.png" class="thumbnail wp-post-image" alt="No thumbnail" />';
	  				$output .= '</a>';
	  				$output .= '</figure>';
	  			endif;

	  			if ( get_the_title() ) $title = get_the_title();
	  			else $title = get_the_ID();

	  			$output .= '<div class="recent-entry-content">';
	  			if ( $show_icon )
	  				$output .= '<h4 class="recent-entry-title"><a href="'. get_permalink() .'"><i class="fa fa-file-text-o"></i> '. $title .'</a></h4>';
	  			else
	  				$output .= '<h4 class="recent-entry-title"><a href="'. get_permalink() .'">'. $title .'</a></h4>';

	  			if ( $show_date ) $output .= sprintf( '<time class="recent-entry-date" datetime="%1$s">%2$s</time>', esc_attr( get_the_date( 'c' ) ), esc_html( sprintf( '%2$s', get_post_format_string( get_post_format() ), get_the_date( 'M j, Y' ) ) ) );
	  			$output .= '</div>';

	  			$output .= '</li>';
	  		endwhile;
	  		$output .= '</ul>';

	  		$output .= '</div>';

	  		/* Reset the global $the_post as this query will have stomped on it */
	  		wp_reset_postdata();

		endif;
	  	$output .= $args['after_widget'];

	  	echo $output;

	  	$cache[$args['widget_id']] = $output;
	  	wp_cache_set('widget_recent_posts', $cache, 'widget');
	}

	/**
	 * Ouputs the options form on admin
	 *
	 * @param array $instance The widget options
	 */
	public function form( $instance ) {
		$title 			= isset( $instance['title'] ) ? esc_attr( $instance['title'] ) : __( 'Recent Posts', THEME_TEXTDOMAIN );
		$description 	= esc_textarea( $instance['description'] );
		$number 		= isset( $instance['number'] ) ? absint( $instance['number'] ) : 5;
		$show_date 		= isset( $instance['show_date'] ) ? (bool) $instance['show_date'] : true;
		$show_thumb 	= isset( $instance['show_thumb'] ) ? (bool) $instance['show_thumb'] : true;
		$show_icon 		= isset( $instance['show_icon'] ) ? (bool) $instance['show_icon'] : false;
		$style 			= isset( $instance['style'] ) ? $instance['style'] : 'list';
		$list_type 		= isset( $instance['list_type'] ) ? $instance['list_type'] : 'default';
		$show_border 	= isset( $instance['show_border'] ) ? (bool) $instance['show_border'] : false;
		$background 		= isset( $instance['background'] ) ? $instance['background'] : '#FDFDFD';
		
		$js_func_name 	= str_replace( '-', '_', $this->get_field_id( 'style' ) ) ."()";

		?><p>
			<label for="<?php echo $this->get_field_id( 'title'); ?>"><?php _e( 'Title:', THEME_TEXTDOMAIN ); ?></label>
			<input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo $title; ?>" />
		</p>
		<p>
			<label for="<?php echo $this->get_field_id( 'description' ); ?>"><?php _e( 'Description:', THEME_TEXTDOMAIN ); ?></label>
			<textarea class="widefat" rows="3" cols="20" id="<?php echo $this->get_field_id( 'description' ); ?>" name="<?php echo $this->get_field_name( 'description' ); ?>"><?php echo $description; ?></textarea>
		</p>
		<p>
			<label for="<?php echo $this->get_field_id( 'number' ); ?>"><?php _e( 'Number of posts to show:', THEME_TEXTDOMAIN ); ?></label>
			<input id="<?php echo $this->get_field_id( 'number' ); ?>" name="<?php echo $this->get_field_name( 'number' ); ?>" type="text" value="<?php echo $number; ?>" size="3" />
		</p>
		<p>
			<label for="<?php echo $this->get_field_id( 'style' ); ?>"><?php _e( 'Style:', THEME_TEXTDOMAIN ); ?></label>
			<select class="widefat" id="<?php echo $this->get_field_id( 'style' ); ?>" name="<?php echo $this->get_field_name( 'style' ); ?>"  onchange="<?php echo $js_func_name; ?>">
				<option value="list" <?php selected( 'list', $style ); ?>><?php _e( 'List', THEME_TEXTDOMAIN ); ?></option>
				<option value="list-2" <?php selected( 'list-2', $style ); ?>><?php _e( 'List 2', THEME_TEXTDOMAIN ); ?></option>
				<option value="box" <?php selected( 'box', $style ); ?>><?php _e( 'Box', THEME_TEXTDOMAIN ); ?></option>
				<option value="box-extended" <?php selected( 'box-extended', $style ); ?>><?php _e( 'Box Extended', THEME_TEXTDOMAIN ); ?></option>
			</select>
		</p>
		<p>
			<input class="checkbox" type="checkbox" <?php checked( $show_date ); ?> id="<?php echo $this->get_field_id( 'show_date' ); ?>" name="<?php echo $this->get_field_name( 'show_date' ); ?>" />
			<label for="<?php echo $this->get_field_id( 'show_date' ); ?>"><?php _e( 'Show Post Date', THEME_TEXTDOMAIN ); ?></label>
		</p>
		<p>
			<input class="checkbox" type="checkbox" <?php checked( $show_thumb ); ?> id="<?php echo $this->get_field_id( 'show_thumb' ); ?>" name="<?php echo $this->get_field_name( 'show_thumb' ); ?>" />
			<label for="<?php echo $this->get_field_id( 'show_thumb' ); ?>"><?php _e( 'Show Thumbnail', THEME_TEXTDOMAIN ); ?></label>
		</p>
		<p>
			<input class="checkbox" type="checkbox" <?php checked( $show_icon ); ?> id="<?php echo $this->get_field_id( 'show_icon' ); ?>" name="<?php echo $this->get_field_name( 'show_icon' ); ?>" />
			<label for="<?php echo $this->get_field_id( 'show_icon' ); ?>"><?php _e( 'Show Post Icon', THEME_TEXTDOMAIN ); ?></label>
		</p>
		<p class="option-for-<?php echo $this->get_field_id( 'style' ); ?> list box box-extended">
			<input class="checkbox" type="checkbox" <?php checked( $show_border ); ?> id="<?php echo $this->get_field_id( 'show_border' ); ?>" name="<?php echo $this->get_field_name( 'show_border' ); ?>" />
			<label for="<?php echo $this->get_field_id( 'show_border' ); ?>"><?php _e( 'Show Border', THEME_TEXTDOMAIN ); ?></label>
		</p>
		<p class="option-for-<?php echo $this->get_field_id( 'style' ); ?> list box box-extended">
			<label for="<?php echo $this->get_field_id( 'list_type' ); ?>"><?php _e( 'List style type:', THEME_TEXTDOMAIN ); ?></label>
			<select class="widefat" id="<?php echo $this->get_field_id( 'list_type' ); ?>" name="<?php echo $this->get_field_name( 'list_type' ); ?>">
				<option value="default" <?php selected( 'default', $list_type ); ?>><?php _e( 'Theme Default', THEME_TEXTDOMAIN ); ?></option>
				<option value="none" <?php selected( 'none', $list_type ); ?>><?php _e( 'None', THEME_TEXTDOMAIN ); ?></option>
				<option value="disc" <?php selected( 'disc', $list_type ); ?>><?php _e( 'Disc', THEME_TEXTDOMAIN ); ?></option>
				<option value="circle" <?php selected( 'circle', $list_type ); ?>><?php _e( 'Circle', THEME_TEXTDOMAIN ); ?></option>
				<option value="square" <?php selected( 'square', $list_type ); ?>><?php _e( 'Square', THEME_TEXTDOMAIN ); ?></option>
				<option value="decimal" <?php selected( 'decimal', $list_type ); ?>><?php _e( 'Decimal', THEME_TEXTDOMAIN ); ?></option>
				<option value="decimal-leading-zero" <?php selected( 'decimal-leading-zero', $list_type ); ?>><?php _e( 'Decimal Leading Zero', THEME_TEXTDOMAIN ); ?></option>
				<option value="armenian" <?php selected( 'armenian', $list_type ); ?>><?php _e( 'Armenian', THEME_TEXTDOMAIN ); ?></option>
				<option value="cjk-ideographic" <?php selected( 'cjk-ideographic', $list_type ); ?>><?php _e( 'CJK Ideographic', THEME_TEXTDOMAIN ); ?></option>
				<option value="georgian" <?php selected( 'georgian', $list_type ); ?>><?php _e( 'Georgian', THEME_TEXTDOMAIN ); ?></option>
				<option value="hebrew" <?php selected( 'hebrew', $list_type ); ?>><?php _e( 'Hebrew', THEME_TEXTDOMAIN ); ?></option>
				<option value="hiragana" <?php selected( 'hiragana', $list_type ); ?>><?php _e( 'Hiragana', THEME_TEXTDOMAIN ); ?></option>
				<option value="hiragana-iroha" <?php selected( 'hiragana-iroha', $list_type ); ?>><?php _e( 'Hiragana Iroha', THEME_TEXTDOMAIN ); ?></option>
				<option value="katakana" <?php selected( 'katakana', $list_type ); ?>><?php _e( 'Katakana', THEME_TEXTDOMAIN ); ?></option>
				<option value="katakana-iroha" <?php selected( 'katakana-iroha', $list_type ); ?>><?php _e( 'Katakana Iroha', THEME_TEXTDOMAIN ); ?></option>
				<option value="lower-alpha" <?php selected( 'lower-alpha', $list_type ); ?>><?php _e( 'Lower Alpha', THEME_TEXTDOMAIN ); ?></option>
				<option value="lower-greek" <?php selected( 'lower-greek', $list_type ); ?>><?php _e( 'Lower Greek', THEME_TEXTDOMAIN ); ?></option>
				<option value="lower-latin" <?php selected( 'lower-latin', $list_type ); ?>><?php _e( 'Lower Latin', THEME_TEXTDOMAIN ); ?></option>
				<option value="lower-roman" <?php selected( 'lower-roman', $list_type ); ?>><?php _e( 'Lower Roman', THEME_TEXTDOMAIN ); ?></option>
				<option value="upper-alpha" <?php selected( 'upper-alpha', $list_type ); ?>><?php _e( 'Upper Alpha', THEME_TEXTDOMAIN ); ?></option>
				<option value="upper-latin" <?php selected( 'upper-latin', $list_type ); ?>><?php _e( 'Upper Latin', THEME_TEXTDOMAIN ); ?></option>
				<option value="upper-roman" <?php selected( 'upper-roman', $list_type ); ?>><?php _e( 'Upper Roman', THEME_TEXTDOMAIN ); ?></option>
			</select>
		</p>
		<p class="option-for-<?php echo $this->get_field_id( 'style' ); ?> list-2 box box-extended">
			<label for="<?php echo $this->get_field_id( 'background'); ?>"><?php _e( 'background:', THEME_TEXTDOMAIN ); ?></label>
			<input class="widefat" id="<?php echo $this->get_field_id( 'background' ); ?>" name="<?php echo $this->get_field_name( 'background' ); ?>" type="text" value="<?php echo $background; ?>" />
		</p>
		<script type="text/javascript">
		jQuery(document).ready(function($){
			var $select = $( '#<?php echo $this->get_field_id( 'style' ); ?>' );
			$( '.option-for-<?php echo $this->get_field_id( 'style' ); ?>' ).css( 'display', 'none' );
			$( '.option-for-<?php echo $this->get_field_id( 'style' ); ?>.' + $select.val() ).css( 'display', 'block' );
			$select.change(function() {
				$( '.option-for-<?php echo $this->get_field_id( 'style' ); ?>' ).css( 'display', 'none' );
				$( '.option-for-<?php echo $this->get_field_id( 'style' ); ?>.' + $(this).val() ).css( 'display', 'block' );
			});
		});</script>
		<?php
	}

	/**
	 * Processing and Sanitize widget form values as they are saved
	 *
	 * @param array $new_instance The new options
	 * @param array $old_instance The previous options
	 */
	public function update( $new_instance, $old_instance ) {
		$instance = $old_instance;

		$instance['title'] = strip_tags( $new_instance['title'] );

		if ( current_user_can('unfiltered_html') ) $instance['description'] =  $new_instance['description'];
		else $instance['description'] = stripslashes( wp_filter_post_kses( addslashes($new_instance['description']) ) ); // wp_filter_post_kses() expects slashed

		$instance['number'] = absint($new_instance['number']);
		if ( $instance['number'] < 1 ) $instance['number'] = 5;
		// else if ( $instance['number'] > 10 ) $instance['number'] = 10;

		$instance['style'] = stripslashes( $new_instance['style'] );

		$instance['show_date'] = (bool) $new_instance['show_date'];
		$instance['show_thumb'] = (bool) $new_instance['show_thumb'];
		$instance['show_icon'] = (bool) $new_instance['show_icon'];

		$this->flush_widget_cache();
		$alloptions = wp_cache_get( 'alloptions', 'options' );
		if ( isset($alloptions['widget_recent_entries']) )
			delete_option('widget_recent_entries');

		$instance['list_type'] = stripslashes( $new_instance['list_type'] );

		$instance['show_border'] = (bool) $new_instance['show_border'];

		$instance['background'] = strip_tags( $new_instance['background'] );

		return $instance;
	}

	/**
	 * Disambiguates widget cache
	 */
	function flush_widget_cache() {
		wp_cache_delete('widget_recent_posts', 'widget');
	}
}


?>