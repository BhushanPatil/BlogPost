<?php
/**
 * The template for displaying 404 pages (Not Found).
 */
?>

<?php get_header(); ?>

		<div class="grid-container">
        
            <?php $sidebar_position = BP_get_sidebar_position(); ?>
			<?php if ( $sidebar_position == 'left-sidebar' ) get_sidebar(); ?>
            
			<?php if ( $sidebar_position == 'no-sidebar' ) : ?><div id="primary-content" class="col-12">
			<?php else : ?><div id="primary-content" class="col-8"><?php endif; ?>

				<div class="error">
					<div>
						<h1><?php _e( 'Oops! 404', THEME_TEXTDOMAIN ); ?></h1>
						<h2><?php _e( 'Page or file not found', THEME_TEXTDOMAIN ); ?></h2>
						<h3>Now what? Go <i class="fa fa-long-arrow-right "></i> <a href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" rel="home">Home</a></h3>
					</div>
				</div>
				
			</div><!--END #primary-content -->

			<?php if ( $sidebar_position == 'right-sidebar' ) get_sidebar(); ?>

		</div>

<?php get_footer(); ?>