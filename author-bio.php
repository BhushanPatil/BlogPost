<div class="author-bio">

	<div class="author-avatar">
		<?php
		echo '<a class="url fn n" href="'. get_author_posts_url( get_the_author_meta( 'ID' ) ) .'" rel="author">'. get_avatar( get_the_author_meta( 'user_email' ), NULL, NULL, sprintf( __( '%s\'s Avatar', THEME_TEXTDOMAIN ), get_the_author() ) ) .'</a>';
		?>
	</div><!-- .author-avatar -->

	<div class="author-info">

		<div class="author-name">
			<h2><?php printf( __( 'Posted by %s', THEME_TEXTDOMAIN ), get_the_author() ); ?></h2>
		</div><!-- .author-name -->

		<?php if ( !empty( get_the_author_meta('description') ) ) : ?>
		<div class="author-bio-text"><?php the_author_meta( 'description' ); ?></div><!-- .author-bio -->
		<?php endif; ?>

		<div class="author-meta">
			<span class="author-posts">
				<?php printf(
					'<i class="fa fa-file-text"></i> '. __( 'Read other Posts by %s', THEME_TEXTDOMAIN ),
					'<a class="url fn n" href="'. get_author_posts_url( get_the_author_meta( 'ID' ) ) .'" title="'. sprintf( __( 'View Posts written by %s', THEME_TEXTDOMAIN ), get_the_author() ) .'" rel="author">'. get_the_author() .'</a>'
				); ?>
			</span><!-- .author-posts -->
			<?php if ( !empty( get_the_author_meta('user_url') ) ) : ?>
			<span class="author-website">
				<?php printf(
					'<i class="fa fa-external-link-square"></i> '. __( 'Website: %s', THEME_TEXTDOMAIN ),
					'<a href="'. get_the_author_meta('user_url') .'" title="'. sprintf( __( 'Visit %s\'s Website', THEME_TEXTDOMAIN ), get_the_author() ) .'" target="_blank">'. get_the_author_meta('user_url') .'</a>'
				); ?>
			</span><!-- .author-website -->
			<?php endif; ?>
		</div><!-- .author-meta -->

		<?php BP_author_social_links(); ?>
		
	</div><!-- .author-info -->

</div><!-- .author-bio -->