<?php
/**
 * The template for displaying posts in the Link post format
 */
?>
<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

	<?php BP_entry_thumbnail(); ?>

	<?php $link = BP_get_links(); ?>
	<?php if( !empty($link) ) : ?>
	<div class="entry-content">
		<p class="link-title"><?php echo $link[html]; ?></p>
		<p class="link-url"><?php echo $link[url]; ?></p>
		<?php if ( !empty($link[desciption]) ) echo '<p class="link-desciption">'. $link[desciption] .'</p>'; ?>
	</div><!--END .entry-content -->
	<?php endif; ?>

	<footer class="entry-footer">
		<?php BP_entry_meta(); ?>
	</footer><!--END .entry-footer -->

</article><!--END #post-<?php the_ID(); ?> -->