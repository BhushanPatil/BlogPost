<?php
/**
 * The default template for displaying content.
 * Used for both single and index/archive/search.
 */
?>
<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

	<?php BP_entry_thumbnail(); ?>

	<header class="entry-header">
		<?php BP_entry_title(); ?>
		<?php BP_entry_meta(); ?>
	</header><!--END .entry-header -->
	
	<?php BP_entry_content(); ?>
	
</article><!--END #post-<?php the_ID(); ?> -->

<?php if ( is_single() ) : ?>
<?php BP_entry_social_media_share(); ?>
<?php endif; ?>